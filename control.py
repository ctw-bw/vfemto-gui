# coding: utf-8

"""
provides interfaces to the Scanlab RTC card and Aerotech A3200 Axes

"""

# noinspection PyUnresolvedReferences
from exceptions import InputError
# from rtc import *
import rtc4
import rtc5
import rtc4v
# import A3200
import WattPilot

import logging

__author__ = "Luigi Capuano"


def setup_loggers(file_level="DEBUG", console_level="INFO", filename=None):
    # set up logging to a file
    # from https://docs.python.org/2/howto/logging-cookbook.html#logging-to-multiple-destinations
    if filename is None:
        filename = "logfile.log"
    file_level = getattr(logging, file_level.upper(), logging.DEBUG)
    console_level = getattr(logging, console_level.upper(), logging.INFO)

    logging.basicConfig(
        level=file_level,
        format="%(asctime)s %(levelname)-8s %(name)-20s  %(message)s",
        filename=filename,
        filemode="w",
    )
    # define a Handler which writes INFO messages or higher to sys.stderr
    console = logging.StreamHandler()
    console.setLevel(console_level)
    formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
    console.setFormatter(formatter)
    logging.getLogger(__name__).addHandler(console)


class Workstation:
    """This class provides an interface to both the rtc and the Aerotech A3200
    It is done in such away that this class is used for all the common initialisation and system wide settings.
    """

    def __init__(self, rtc=None):
        """This initializes the workstation instance not the setup it self.
        The RTC defaults to a "virtual" RTC4 card
        And the stages to None.
        :param rtc: requires one of the following "rtc4", "rtc5", "rtc4v", None or a valid rtc.RtcDll sub-class instance
        :(not anymore) param stages: requires either "A3200", None or a valid sublclass instance of A3200.A3200Sys
        :raise (InputError, "No stage selected"):
        """
        if rtc is None:
            self.rtc = rtc4v.vRtc4()
        elif rtc == "rtc4":
            self.rtc = rtc4.Rtc4()
        elif rtc == "rtc5":
            self.rtc = rtc5.Rtc5()
        elif rtc == "rtc4v":
            self.rtc = rtc4v.vRtc4()
        elif issubclass(type(rtc), rtc.RtcDll):
            self.rtc = rtc
        else:
            raise (InputError, "No rtc type selected")

        """
        if stages is None:
            self.stages = None
        elif issubclass(type(stages), A3200.A3200Sys):
            self.stages = stages
            self.stages.num_axes = 3
        elif stages == "A3200":
            self.stages = A3200.A3200Sys()
            self.stages.num_axes = 3
        else:
            raise(InputError, "No stage selected")

        self.axes_dict = {0: "xaxis", 1: "yaxis", 2: "zaxis", 3: "raxis"}
        """

        self.scanner = None
        self.list_1 = None
        self.list_2 = None

        self.laser_mode = 1

        # TODO: Needs to be here? or maybe a module variable or even in a config file
        # list of lenses and their setting
            # self.lenses = [{"name": "green 103 mm",
            #     "correction_file": "C:\Documents and Settings\WA USER\My Documents\RTC4\Correction Files\D2_248.ctb",
            #     "factor_K": 945}]

        self.lenses = [
            {
                "name": "IR 1064 mm",
                "correction_file": "C:\RTC4\Correction Files\D2_818.ctb",  # Copy of D2_818_2.ctb",
                "factor_K": 902,
            }
        ]

        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    '''    
    def init_stages(self, num_axes=None, axes_dict=None, home=True, wait=True):
        """ This initializes the stages and gives them several useful binds.
        :param num_axes: the amount of axes in the system
        :param axes_dict: names for the axes as a dict for an example look at the uninitialized axes_dict
        :return:
        """
        if not self.stages is None:
            if num_axes is None:
                num_axes = self.stages.num_axes
            else:
                self.stages.num_axes = num_axes
            if axes_dict is None:
                axes_dict = self.axes_dict
            else:
                self.axes_dict = axes_dict

            self.stages.init()
            for i in range(num_axes):
                self.stages.axes[i].init(home=True)
                setattr(self, axes_dict[i], self.stages.axes[i])
                if home:
                    self.stages.axes[i].home(wait=wait)
    '''

    def init_scanner(self, lens=None, laser_source="femto"):
        """This initializes the RTC card and set the settings for the lens
        it creates the scanner, list_1 and list 2 binds to the appropriate parts of the rtc instance
        :param lens: is either a lens dict or None (in which case a default lens is used)
        :return:
        """
        # TODO: make sure this works with the RTC5 cards
        if lens is None:
            lens = self.lenses[0]
        self.rtc.init()

        self.scanner = self.rtc.cards[0]
        self.scanner.set_laser_source(laser_source)
        self.scanner.correction_file = lens["correction_file"]
        print("control correction file: ", lens["correction_file"])
        self.scanner_parameters = self.scanner.load_parameter_file()
        self.scanner.initialize_card()
        self.scanner.head_parameters["factor_K"] = lens["factor_K"]
        self.list_1 = self.scanner.lists[0]
        self.list_1.set_laser_source(laser_source)
        self.list_2 = self.scanner.lists[1]
        self.list_1.set_laser_source(laser_source)
        self.scanner.set_laser_mode(self.laser_mode)
        # self.list_1.laser_on()

    def init_powermodulator(self):
        self.p_mod = WattPilot.WattPilot()
        self.cur_pos = self.p_mod.initialize_Watt_Pilot()
        self.p_mod.set_power(0)  # move watt pilot to lowest value

    def set_power(self, power):
        self.p_mod.set_power(power)  # move watt pilot to lowest value

    def init(self, laser_source="femto", home=True):
        """Initialize both the scanner and the stages with their default values (you probably only want to use this
        with the virtual setup and no stages)
        :return:
        """
        self.init_powermodulator()
        self.init_scanner(laser_source=laser_source)
        # self.init_stages(home = home)

    '''
    def positions(self):
        """ prints a list of the position
        :return:
        """
        print("axis\t\tposition")
        if not self.stages is None:
            for i in range(self.stages.num_axes):
                print("stage {}\t{:07.3f} [mm]".format(self.axes_dict[i], self.stages.axes[i].position()))
        print("scanner 0\t{:07.3f} [mm]".format(self.scanner.position()[0]))
        print("scanner 1\t{:07.3f} [mm]".format(self.scanner.position()[1]))
    
    def stop(self):
        """ stop the stages and the execution of the currently running program.
        :return:
        """
        self.stages.stop()
        self.scanner.stop_execution()
    '''

    def enable_laser(self):
        """enable the laser signals on the rtc card
        :return:
        """
        self.scanner._enable_laser()

    def disable_laser(self):
        """disable the laser signals on the rtc card
        :return:
        """
        self.scanner._disable_laser()
