"""
provides interfaces to the Scanlab RTC card and Aerotech A3200 Axes

"""


from control import Workstation
import time


class wrp_RTC4:
    """
    This class provides interfaces to the Scanlab RTC4 card

    It has 'private' members for the DLL fucntions and accessable functions for use in other programs and classes

    Attributes:
        dll_path = the path to the RTC4DLL.dll file
        dll_initialized = True when the DLL is initialized otherwise False
    """

    def __init__(self):

        return

    def InitCard(self):
        """
        Initalizes the class and set all python variables to their defaults
        """

        self.ws = Workstation("rtc4")
        self.ws.init_scanner()
        self.ws.init_powermodulator()
        self.UpdateLaserPower(1)

        with self.ws.list_1:
            self.ws.list_1.set_laser_delays(laseron=100, laseroff=0)
            self.ws.list_1.set_laser_timing(100, 50, 50, 0)

    def LaserOn(self):
        with self.ws.list_1:
            self.ws.list_1.set_start_list(1)
            self.ws.list_1.sh_on()
            self.ws.list_1.set_end_of_list()
            self.ws.list_1.execute_list(1)
        return

    def LaserOff(self):
        with self.ws.list_1:
            self.ws.list_1.sh_off()
        return

    def Wait(self, deltat):
        DELTAT = deltat

        if deltat < 0.5:
            with self.ws.list_1:
                self.ws.list_1.wait(deltat)

        if deltat > 0.5:
            while DELTAT > 0.5:
                with self.ws.list_1:
                    self.ws.list_1.wait(0.5)
                DELTAT = DELTAT - 0.5
            with self.ws.list_1:
                self.ws.list_1.wait(DELTAT)

        return

    def UpdateLaserPower(self, power_percentage):
        self.ws.set_power(power_percentage)
        return

    def ReleaseSinglePulse(self, TimeWindow):
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.laser_on(TimeWindow, True)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def ReleaseMultiplePulses(self, NumberOfPulses, TimeWindow):
        bits = int(NumberOfPulses * TimeWindow)  # *1e5)
        DELTAT = bits

        print("Printing %d pulses" % NumberOfPulses)
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.sh_on()

        if bits < 65500:
            self.ws.list_1.wait(bits, True)

        if bits > 65500:
            while DELTAT > 65500:
                self.ws.list_1.wait(65500)
                DELTAT = DELTAT - 65500
            self.ws.list_1.wait(DELTAT, True)

        self.ws.list_1.sh_off()
        self.ws.list_1.set_end_of_list()  # lc13082019
        self.ws.list_1.execute_list(1)
        return

    def multipulse_pause(self, NumberOfPulses, TimeWindow):

        bits = int(NumberOfPulses * TimeWindow * 1e5)
        DELTAT_ON = bits
        DELTAT_OFF = bits

        self.ws.list_1.set_start_list(1)

        with self.ws.list_1:

            if NumberOfPulses > 0:

                # print (NumberOfPulses)

                self.ws.list_1.sh_on()

                if bits < 65500:
                    self.ws.list_1.wait(bits, True)

                if bits > 65500:
                    while DELTAT_ON > 65500:
                        self.ws.list_1.wait(65500)
                        DELTAT_ON = DELTAT_ON - 65500
                    self.ws.list_1.wait(DELTAT_ON, True)

                self.ws.list_1.sh_off()
                # self.ws.list_1.set_end_of_list()
                # self.ws.list_1.execute_list(1)
            elif NumberOfPulses < 0:
                bits = -bits
                # print (NumberOfPulses)

                # self.ws.list_1.sh_off()

                if bits < 65500:
                    self.ws.list_1.wait(bits - 2 * 6000, True)

                if bits > 65500:
                    while DELTAT_OFF > 65500:
                        self.ws.list_1.wait(65500)
                        DELTAT_OFF = DELTAT_OFF - 65500
                    self.ws.list_1.wait(DELTAT_OFF - 2 * 6000, True)

                # self.ws.list_1.sh_off()
                # self.ws.list_1.set_end_of_list()
                # self.ws.list_1.execute_list(1)
        return

    def execute_line(self):
        with self.ws.list_1:
            self.ws.list_1.set_end_of_list()
            self.ws.list_1.execute_list(1)

    ####### Galvo scanner #########

    def jump_speed(self, speed):
        with self.ws.list_1:
            self.ws.list_1.set_jump_speed(speed, True)
            self.ws.list_1.execute_list(1)

    def mark_speed(self, speed):
        with self.ws.list_1:
            self.ws.list_1.set_mark_speed(speed, True)
            self.ws.list_1.execute_list(1)

    def mark(self, x, y, rel):
        with self.ws.list_1:
            self.ws.list_1.set_start_list(1)
            self.ws.list_1.mark(x, y, rel, False)
            self.ws.list_1.set_end_of_list()
            self.ws.list_1.execute_list(1)

        return

    def jump(self, x, y, rel):
        with self.ws.list_1:
            self.ws.list_1.set_start_list(1)
            self.ws.list_1.jump(x, y, rel, False)
            self.ws.list_1.set_end_of_list()
            self.ws.list_1.execute_list(1)

        return
