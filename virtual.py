"""
provides interfaces to a virtual rtc card

currently only one head per card is used.
"""

import ctypes
import logging

# noinspection PyUnresolvedReferences
import exceptions
import shapely
import shapely.geometry
import shapely.affinity

from matplotlib import pyplot
import math


__author__ = "Gerald Ebberink"
__license__ = "GPL v2"
__version__ = "0.0.1"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@utwente.nl"
__status__ = "Prototype"


# noinspection PyUnresolvedReferences
class vRtcDll:
    """
    This class provides interfaces to the Scanlab RTC4 card

    It has 'private' memebers for the DLL fucntions and accessable functions for use in other programs and classes

    Attributes:
        dll_path = the path to the RTCDLL.dll file
        dll_initialized = True when the DLL is initialized otherwise False
    """

    def __init__(self):
        """
        Initalizes the class and set all python variables to their defaults
        :return:
        """
        self.rtc_path = "C:\\Rtc"
        self.programfile_path = self.rtc_path
        self.dll_path = self.programfile_path + "\\RTCDLL.dll"
        self.headerfile = "\\RTCImport\\RTCexpl.h"
        self.dll_initialized = False
        self.default_rtc = 1
        self.option_3d = False
        self.rtc_dll = None
        self.cards = []

        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

        self._fig_handle = None

    # The __enter__ and __exit__ functions can be used in by the with clause
    def __enter__(self):
        """function used for the contextmanager (the with statement)
        :return:
        """
        self.logger.debug("entering virtual rtc context")
        self.init()
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, type, value, traceback):
        """function used for the contextmanager (the with statement)
        :return:
        """
        self.logger.debug("exiting rtc context")
        self._terminate_dll()

    def _load_function(
        self, name, argtypes=None, restype=None, doc=None, private=True, other=None
    ):
        """loads function with "name" from the dll and set the argtypes, restype and doc

        :param name: Name of the function
        :param argtypes: the arguments of the function
        :param restype: the return type of the function
        :param doc: the docstring for the function
        :param private: if set to True (default) an _ is added to the beginning of the function name in python
        :param other: is the Card instance into which the function should be loaded.
        :return:
        """
        if not isinstance(other, RtcCard):
            other = self

        func = getattr(self.rtc_dll, name)
        if argtypes is not None:
            func.argtypes = argtypes
        if restype is not None:
            func.restype = restype
        if doc is not None:
            func.__doc__ = doc
        if private:
            name = "_" + name
        setattr(other, name, func)
        self.logger.debug(
            "function {} loaded into {}: argtypes = {}, restype = {}, private = {}".format(
                name, other, argtypes, restype, private
            )
        )

    def _init_dll(self, dll=None):
        """
        Initializes the dll and checks for errors
        :param dll: path to the dll. If None is given (as default) the value of self.dll_path is used
        :return: self.dll_initialized is set to True and True is returned
        """
        self.logger.debug("entering the _init_dll function")
        fig = pyplot.figure()
        pyplot.axis("equal")
        ax = fig.gca()
        ax.autoscale()
        self._fig_handle = fig.number
        self.dll_initialized = True

    def _init_cards(self):
        """
        Initialize the cards.

        :return:
        """
        self.logger.debug("initialising cards")
        self.card_count = 1
        self.cards = []
        for card in range(self.card_count):
            self.logger.debug("card no. {} added".format(card + 1))
            self.cards.append(self.rtc_card_class(self, card + 1))
            self.cards[card].initialize_card()
        self.logger.debug("all cards added")

    def _terminate_dll(self):
        """
        Stops all the running processes
        terminates the connection to the RTC5 card.
        :return:
        """
        pass

    def init(self):
        self.logger.debug("init")
        self._init_dll()
        self._init_cards()


# noinspection PyProtectedMember,PyUnresolvedReferences
class vRtcCard:
    """ " To communicate with RTC cards

    Most of the functions can be run concurrent for several cards.
    Only the initialize function is depended on the select_rtc and has to be run by itself
    """

    def __init__(self, dll=None, cardno=None):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)
        self.logger.debug("initializing RTC Card instance")
        self.lists = []
        self._dll = dll
        self.cardno = cardno
        self.correction_file = dll.rtc_path + "\\Cor_1to1.ct5"
        self.initialized = False
        self.program_file_path = None
        self.out_version = None
        self.rbf_version = None
        self.option_3d = False
        self.serial_number = None
        self.lists = []
        self.delays = {
            "laseron": 5,
            "laseroff": 5,
            "jump": 50,
            "mark": 30,
            "polygon": 30,
            "varpoly": 0,
            "direct3d": 0,
            "edgelevel": 0xFFFFFFFF,
            "minjumpdelay": 1,
            "jumplength": 0,
        }
        self.head_parameters = {"factor_K": 1}
        self.logger.debug("Instance is initialized for cardno: {}".format(self.cardno))

        # specific for virtual driver
        self._position = [0, 0]
        self._power = 0
        self._jump_speed = 0
        self._mark_speed = 0
        self._fig_handle = dll._fig_handle
        self._max_speed = 5
        self._colors = {"mark": "#ff3355", "jump": "#6699cc"}
        self._matrix = [[0, 1], [1, 0]]
        self._time = 0

    def initialize_card(self):
        """This initializes the card

         Internally it uses _select_rtc so it cannot be run concurrent or multi-threaded

        :return:
        """
        card = self.cardno
        self._time = 0
        self.lists = []
        for listno in range(2):
            listno += 1
            self.logger.debug("card {}: init of list {}".format(card, listno))
            self.lists.append(self.rtc_list_class(self, listno))

    def stop_execution(self):
        pass

    def get_list_status(self):
        return 0

    def set_delay_mode(self, delays=None):
        """set the delay mode
        see RTC manual
        If any of the values is none, the value is taken from self.delays
        :param delays:  a dict with the delays needing to be changed or
                        if None self.delays is used
        :return:
        """
        if isinstance(delays, dict):
            new_delay = self.delays
            new_delay.update(delays)
            self.logger.debug(
                "card {}: updated delays with {}".format(self.cardno, delays)
            )
        elif delays is None:
            new_delay = self.delays
            self.logger.debug(
                "card {}: delays used from self.delays {}".format(
                    self.cardno, self.delays
                )
            )
        else:
            self.logger.error(
                "card {}: No valid delays given using self.delays {}".format(
                    self.cardno, self.delays
                )
            )
            new_delay = self.delays

        self.logger.debug("card {}: delays set:".format(self.cardno))
        self.logger.debug(
            "card {}: varpoly is {}".format(self.cardno, new_delay["varpoly"])
        )
        self.logger.debug(
            "card {}: direct3d is {}".format(self.cardno, new_delay["direct3d"])
        )
        self.logger.debug(
            "card {}: edgelevel is {}".format(self.cardno, new_delay["edgelevel"])
        )
        self.logger.debug(
            "card {}: minjumpdelay is {}".format(self.cardno, new_delay["minjumpdelay"])
        )
        self.logger.debug(
            "card {}: jumplength is {}".format(self.cardno, new_delay["jumplength"])
        )

        self.delays = new_delay

    def load_varpolydelay(self, filename=None, number=None):
        pass

    def load_correction_file(self, filename=None, table=None):
        pass

    def select_cor_table(self, head_a=None, head_b=None):
        pass

    def set_laser_mode(self, mode=None):
        pass

    def write_da(self, da=None, value=None, bits=False):
        """Write a value to the given d/a converter

        :param da: 1 and 2 are valid if none then 1 is used
        :param value: between 0 and 10 if bits is False otherwise a value between 0x000 and 0xFFF
        :param bits: when True value is given in bits
        :return:
        """
        if da is None:
            da = 1
        if value is None:
            value = 0

        if not bits:
            value = (value / 10) * 0x3FF  # (2 ** 12) -1
            value = int(value)

        if value > 0x3FF:
            self.logger.info(
                "card {}: DA channel {} value to high ({})clipped to 0x3FF".format(
                    self.cardno, da, value
                )
            )
            value = 0x3FF
        if value < 0:
            self.logger.info(
                "card {}: DA channel {} value to low ({})clipped to 0x0".format(
                    self.cardno, da, value
                )
            )
            value = 0x0

        self.logger.debug(
            "card {}: DA channel {} set to {} Volt or {} in bits".format(
                self.cardno, da, (value / 0x3FF) * 10, value
            )
        )
        self._power = (value / 0x3FF) * 100

    def position(self, bits=False):
        """returns the position as a (x,y) tuple
        :param bits: If set to True returns the bit value
        :return: tuple with (x,y) position"""
        return tuple(self._position)

    def get_time(self):
        """get the time
        :return:
        """
        return self._time

    def reset_time(self):
        self._time = 0
        return self._time


# noinspection PyProtectedMember,PyUnresolvedReferences
class vRtcList:
    def __init__(self, card=None, listno=None):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)
        if not isinstance(card, vRtcCard):
            self.logger.error("No RTC given.")
            raise (ValueError, "No RtcCard given")
        if listno is None:
            listno = 1
        self._card = card
        self.cardno = self._card.cardno
        self.listno = listno
        self.delays = card.delays
        self.logger.debug(
            "card {}: list {}: instance initialized".format(self.cardno, self.listno)
        )
        self._fig_handle = card._fig_handle
        self._mark_time = None

    def __enter__(self):
        self.load_list()
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, type, value, traceback):
        self.set_end_of_list()
        self.execute_list()

    def mark(self, x, y, relative=True, bits=False):
        """Marks a vector
        :param x: the x coordinate in mm of the position in absolute mode or the dx in relative mode
        :param y: the y coordinate in mm of the position in absolute mode or the dy in relative mode
        :param relative: if False absolute mode is used
        :return:
        """
        rtc = self._card
        pyplot.figure(self._fig_handle)
        width = 8 * rtc._power / 100
        alpha = 0.2 + 0.8 * rtc._mark_speed / rtc._max_speed

        if bits:
            x /= rtc.head_parameters["factor_K"]
            y /= rtc.head_parameters["factor_K"]

        # now apply the matrix
        x = x * rtc._matrix[0][0] + x * rtc._matrix[0][1]
        y = y * rtc._matrix[1][0] + y * rtc._matrix[1][1]

        if relative:
            x += rtc._position[0]
            y += rtc._position[1]
        pyplot.plot(
            [rtc._position[0], x],
            [rtc._position[1], y],
            linewidth=width,
            alpha=alpha,
            color=rtc._colors["mark"],
        )
        distance = math.sqrt((rtc._position[0] - x) ** 2 + (rtc._position[1] - y) ** 2)
        rtc._time += distance / rtc._mark_speed
        rtc._time += self.delays["mark"] * 10 * 10e-6
        rtc._position[0] = x
        rtc._position[1] = y

    def jump(self, x, y, relative=True, bits=False):
        """jumps to position
        :param x: the x coordinate in mm of the position in absolute mode or the dx in relative mode
        :param y: the y coordinate in mm of the position in absolute mode or the dy in relative mode
        :param relative: if False absolute mode is used
        :return:
        """
        rtc = self._card
        pyplot.figure(self._fig_handle)
        width = 4 * rtc._power / 100
        alpha = 0.2 + 0.8 * rtc._jump_speed / rtc._max_speed

        if bits:
            x /= rtc.head_parameters["factor_K"]
            y /= rtc.head_parameters["factor_K"]

        # now apply the matrix
        x = x * rtc._matrix[0][0] + x * rtc._matrix[0][1]
        y = y * rtc._matrix[1][0] + y * rtc._matrix[1][1]

        if relative:
            x += rtc._position[0]
            y += rtc._position[1]
        pyplot.plot(
            [rtc._position[0], x],
            [rtc._position[1], y],
            linewidth=width,
            alpha=alpha,
            color=rtc._colors["jump"],
        )
        distance = math.sqrt((rtc._position[0] - x) ** 2 + (rtc._position[1] - y) ** 2)
        rtc._time += distance / rtc._jump_speed
        rtc._time += self.delays["jump"] * 10 * 10e-6
        rtc._position[0] = x
        rtc._position[1] = y

    def arc(self, x, y, angle, relative=True, bits=False):
        """draw an circular arc from the current position
        The center of this circle is located at the x, y position.
        :param x: the x coordinate of the center in absolute mode or the dx in relative mode
        :param y: the y coordinate of the center in absolute mode or the dy in relative mode
        :param angle: the angle to be marked
        :param relative: if False absolute mode is used
        :return:
        """
        raise (NotImplementedError)

    def ellipse(self, a, b, phi0, phi, x, y, alpha, relative=True, bits=False):
        """Draw an elliptical arc
        The center of this is located at the x, y position.
        :param a: length of long half-axis
        :param b: length of the short half-axis
        :param phi0: beginning phase angle (distance from the end point of a
        :param phi: angle to be marked (defines the length of the mark)
        :param x: center of the ellipse in either absolute x or relative dx
        :param y: center of the ellipse in either absolute y or relative dy
        :param alpha: angle between the X-axis and the a axis of the ellipse
        :param relative: if False absolute mode is used
        :return:
        """
        raise (NotImplementedError)

    def execute_list(self, position=None):
        """start executing a list with previous loaded commands
        :param position: the position at which to start executing the list (0 if None)
        :return: the number of times the 10 us clock is over run
        """
        pass

    def load_list(self, position=None):
        """Load this list
        :param position: Start at this position if None postion 0 is used.
        :return: True is succesfull
        """
        self._mark_time = 0
        pass

    def set_end_of_list(self):
        """sets the end of the list os processing will stop here
        :return:
        """
        pass

    def set_laser_delays(self, laseron=None, laseroff=None):
        """Sets the laser on and laser off delays.
        Time in microseconds
        if None is given the appropriate value from self.delays is used.
        :param laseron: LaserOnDelay in us
        :param laseroff: LaserOffDelay in us
        :return:
        """
        pass

    def set_scanner_delays(self, jump=None, mark=None, polygon=None):
        """Sets the jump, mark and polygon delays
        Time in microseconds
        if None is given the appropriate value from self.delays is used.
        :param jump: jump delay in us
        :param mark: mark delay in us
        :param polygon: polygon delay in us
        :return:
        """
        rtc = self._card
        if not jump is None:
            rtc.delays["jump"] = jump
        if not mark is None:
            rtc.delays["mark"] = mark
        pass

    def set_delay_mode(self, delays=None):
        """set the delay mode
        see RTC5 manual
        If any of the values is none, the value is taken from self.delays
        :param delays:  a dict with the delays needing to be changed or
                        if None self.delays is used
        :return:
        """
        pass

    def wait(self, time=None, bits=False):
        rtc = self._card
        rtc._time += time
        pass

    def set_mark_speed(self, speed=None, bits=False):
        rtc = self._card
        if speed is None:
            speed = 2.0  # m/s
        if bits:
            speed /= rtc.head_parameters["factor_K"]
        rtc._mark_speed = speed

    def set_jump_speed(self, speed=None, bits=False):
        rtc = self._card
        if speed is None:
            speed = 2.0  # m/s
        if bits:
            speed /= rtc.head_parameters["factor_K"]
        rtc._jump_speed = speed

    def write_da(self, da=None, value=None, bits=False):
        """Write a value to the given d/a converter

        :param da: 1 and 2 are valid if none then 1 is used
        :param value: between 0 and 10 if bits is False otherwise a value between 0x000 and 0xFFF
        :param bits: when True value is given in bits
        :return:
        """
        rtc = self._card
        if da is None:
            da = 1
        if value is None:
            value = 0

        if not bits:
            value = (value / 10) * 0x3FF  # (2 ** 12) -1
            value = int(value)

        if value > 0x3FF:
            self.logger.info(
                "card {}: list {}: DA channel {} value to high ({}) clipped to 0x3FF".format(
                    self.cardno, self.listno, da, hex(value)
                )
            )
            value = 0x3FF
        if value < 0:
            self.logger.info(
                "card {}: list {}: DA channel {} value to low ({}) clipped to 0x0".format(
                    self.cardno, self.listno, da, hex(value)
                )
            )
            value = 0x0
        self.logger.debug(
            "card {}: list {}: DA channel {} set to {} Volt or {} in bits".format(
                self.cardno, self.listno, da, (value / 0x3FF) * 10, hex(value)
            )
        )
        rtc._power = (value / 0x3FF) * 10

    def laser_on(self, period=None, bits=False):
        """turn the laser on for 'period' amount of seconds (for bits is False)
        if bits is True then the amount is given in multiples of 10 us
        :param period: the amount of seconds the laser needs to be on (if None zero is used)
        :param bits: if False period is in seconds otherwise in 10 us
        """
        rtc = self._card
        pyplot.figure(self._fig_handle)
        pyplot.plot(rtc._position[0], rtc._position[1], ".", color=rtc._colors["mark"])
        pass

    def check_busy(self):
        """check if the scanner is busy
        :return: True if the scanner is busy
        """
        return False

    def position(self, bits=False):
        rtc = self._card
        return rtc._position

    def mark_linestring(self, line=None, relative=True, bits=False):
        if not issubclass(type(line), shapely.geometry.LineString):
            raise (TypeError, "No subclass of or shapely.geometry.LineString given")
        else:
            initial = True
            prev_point = shapely.geometry.Point((0, 0))
            for point in line.coords:
                point = shapely.geometry.Point(point)
                if initial:
                    if not relative:
                        self.jump(x=point.x, y=point.y, relative=False, bits=bits)
                        prev_point = shapely.geometry.Point(point)
                    initial = False
                else:
                    track = shapely.geometry.Point(
                        point.x - prev_point.x, point.y - prev_point.y
                    )
                    self.mark(track.x, track.y, relative=True, bits=bits)
                prev_point = shapely.geometry.Point(point)

    def mark_multilinestring(self, multiline=None, relative=True, bits=False):
        if issubclass(type(multiline), shapely.geometry.MultiLineString):
            firstline = list(multiline)[0]
            if not relative:
                self.jump(
                    firstline.coords[0][0],
                    firstline.coords[0][1],
                    relative=False,
                    bits=bits,
                )
            last_point = shapely.geometry.Point((0, 0))
            for line in list(multiline):
                next_point = shapely.geometry.Point(line.coords[0])
                trace = shapely.geometry.Point(
                    next_point.x - last_point.x, next_point.y - last_point.y
                )
                self.jump(trace.x, trace.y, relative=True, bits=bits)
                self.mark_linestring(line=line, relative=True, bits=bits)
                last_point = shapely.geometry.Point(line.coords[-1])

    def rectangle(self, width, height, relative=True, bits=False):
        self.logger.debug(
            "card {}: list {}: mark a rectangle with witdh {} and height {}".format(
                self.cardno, self.listno, width, height
            )
        )
        if relative:
            self.logger.debug(
                "card {}: list {}: at the current position".format(
                    self.cardno, self.listno
                )
            )
            self.mark(x=width, y=0, relative=True, bits=bits)
            self.mark(x=0, y=height, relative=True, bits=bits)
            self.mark(x=width * -1, y=0, relative=True, bits=bits)
            self.mark(x=0, y=height * -1, relative=True, bits=bits)
        else:
            self.logger.debug(
                "card {}: list{}: around the origin".format(self.cardno, self.listno)
            )
            width /= 2
            height /= 2
            self.jump(x=width * -1, y=height * -1, relative=False, bits=bits)
            # nice way to do this
            self.rectangle(width=width, height=height, relative=True, bits=bits)

    def hatch_rectangle(
        self,
        width,
        height,
        distance,
        angle,
        radians=False,
        meander=True,
        relative=True,
        bits=False,
    ):
        vecs = []
        self.logger.error("test {}".format(len(vecs)))
        inv = False
        if not radians:
            angle = math.radians(angle)
        # hatches larger then 90 degrees do not work
        if angle > 0.5 * math.pi:
            angle = 0.5 * math.pi
        # hatches smaller then 0 do not work.
        # zero does not work either (div / 0)
        if angle < 0.0001:
            angle = 0.0001
        slope = math.tan(angle)
        y_change = distance * math.sqrt(1 + slope ** 2)
        n = 1
        y1 = 0
        self.logger.debug("card {}: list {}: hatching with:")
        self.logger.debug(
            "card {}: list {}: width, height: {}, {}".format(
                self.cardno, self.listno, width, height
            )
        )
        self.logger.debug(
            "card {}: list {}: distance, angle: {}, {}".format(
                self.cardno, self.listno, distance, angle
            )
        )
        # let us calculate the hatch lines.
        while y1 < height:
            x1 = 0
            y1 = n * y_change
            x2 = (height - y1) / slope
            if x2 < width:
                y2 = height
            elif x2 > width:
                x2 = width
                y2 = x2 * slope + y1
            else:
                x2 = width
                y2 = height
            n += 1
            if y1 < height:
                line = RtcLine([x1, y1], [x2, y2])
                if inv and meander:
                    line.invert()
                inv = not inv
                vecs.append(line)

        n = 0
        x1 = 0
        while x1 < width:
            y = n * y_change
            y1 = 0
            x1 = (y * -1) / slope

            y2 = (slope * width) + y
            if y2 < height:
                x2 = width
            elif y2 > height:
                y2 = height
                x2 = (height - y) / slope
            else:
                y2 = heigth
                x2 = width
            n -= 1
            if x1 < width:
                line = RtcLine([x1, y1], [x2, y2])
                if inv and meander:
                    line.invert()
                inv = not inv
                vecs.append(line)

        if not relative:
            self.jump(x=vecs[0].node1[0], y=vecs[0].node1[1], relative=False, bits=bits)

        for i in range(len(vecs)):
            # except for the first one jump to beginning of line
            if i != 0:
                rel_x = vecs[i].node1[0] - vecs[i - 1].node2[0]
                rel_y = vecs[i].node1[1] - vecs[i - 1].node2[1]
                self.jump(x=rel_x, y=rel_y, relative=True, bits=bits)

            # write line
            rel_x = vecs[i].node2[0] - vecs[i].node1[0]
            rel_y = vecs[i].node2[1] - vecs[i].node1[1]
            self.mark(x=rel_x, y=rel_y, relative=True, bits=bits)

    def square(self, size, relative=True, bits=False):
        self.logger.debug(
            "card {}: list {}: mark a square with size {}".format(
                self.cardno, self.listno, size
            )
        )
        self.rectangle(width=size, height=size, relative=relative, bits=bits)

    def hatch_square(
        self,
        size,
        distance,
        angle,
        radians=False,
        meander=True,
        relative=True,
        bits=False,
    ):
        self.hatch_rectangle(
            width=size,
            height=size,
            angle=angle,
            distance=distance,
            radians=radians,
            meander=meander,
            relative=relative,
            bits=bits,
        )

    def mark_linestring(self, line=None, relative=True, bits=False):
        card = self._card

        if not issubclass(type(line), shapely.geometry.LineString):
            raise (TypeError, "No subclass of or shapely.geometry.LineString given")
        else:
            initial = True
            prev_point = shapely.geometry.Point((0, 0))
            for point in line.coords:
                point = shapely.geometry.Point(point)
                if initial:
                    if not relative:
                        self.jump(x=point.x, y=point.y, relative=False, bits=bits)
                        prev_point = shapely.geometry.Point(point)
                    initial = False
                else:
                    track = shapely.geometry.Point(
                        point.x - prev_point.x, point.y - prev_point.y
                    )
                    self.mark(track.x, track.y, relative=True, bits=bits)
                prev_point = shapely.geometry.Point(point)

    def mark_multilinestring(self, multiline=None, relative=True, bits=False):
        card = self._card
        if issubclass(type(multiline), shapely.geometry.MultiLineString):
            firstline = list(multiline)[0]
            if not relative:
                self.jump(
                    firstline.coords[0][0],
                    firstline.coords[0][1],
                    relative=False,
                    bits=bits,
                )
            last_point = shapely.geometry.Point((0, 0))
            for line in list(multiline):
                next_point = shapely.geometry.Point(line.coords[0])
                trace = shapely.geometry.Point(
                    next_point.x - last_point.x, next_point.y - last_point.y
                )
                self.jump(trace.x, trace.y, relative=True, bits=bits)
                self.mark_linestring(line=line, relative=True, bits=bits)
                last_point = shapely.geometry.Point(line.coords[-1])

    def mark_shape(self, shape=None, relative=True, bits=False):
        if issubclass(type(shape), shapely.geometry.MultiLineString):
            self.mark_multilinestring(multiline=shape, relative=relative, bits=bits)
        elif issubclass(type(shape), shapely.geometry.LineString):
            self.mark_linestring(line=shape, relative=relative, bits=bits)
        else:
            raise (TypeError, "Not a recognised shape")


# Bits and bytes
class RtcVersionBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("firmware_version", ctypes.c_uint8, 8),
        ("on_the_fly", ctypes.c_uint8, 1),
        ("second_control", ctypes.c_uint8, 1),
        ("3d_enabled", ctypes.c_uint8, 1),
        ("reserved", ctypes.c_uint8, 5),
        ("DSP_version", ctypes.c_uint8, 8),
        ("firmware_subversion", ctypes.c_uint8, 8),
    ]


class RtcVersion(ctypes.Union):
    _fields_ = [("bits", RtcVersionBits), ("ulData", ctypes.c_uint32)]


class RtcListStatusBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("load1", ctypes.c_uint8, 1),
        ("load2", ctypes.c_uint8, 1),
        ("ready1", ctypes.c_uint8, 1),
        ("ready2", ctypes.c_uint8, 1),
        ("busy1", ctypes.c_uint8, 1),
        ("busy2", ctypes.c_uint8, 1),
        ("used1", ctypes.c_uint8, 1),
        ("used2", ctypes.c_uint8, 1),
    ]


class RtcListStatus(ctypes.Union):
    _fields_ = [("bits", RtcListStatusBits), ("ulData", ctypes.c_uint32)]


class RtcLaserControlBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("pulse_switch", ctypes.c_uint8, 1),
        ("phase_shift", ctypes.c_uint8, 1),
        ("enable", ctypes.c_uint8, 1),
        ("laser_on_signal_level", ctypes.c_uint8, 1),
        ("laser_signal_level", ctypes.c_uint8, 1),
        ("ext_flank", ctypes.c_uint8, 1),
        ("synchronize", ctypes.c_uint8, 1),
        ("constant_pulse", ctypes.c_uint8, 1),
        ("reserved", ctypes.c_uint8, 8),
        ("AX_power", ctypes.c_uint8, 1),
        ("AX_temp", ctypes.c_uint8, 1),
        ("AX_pos", ctypes.c_uint8, 1),
        ("AY_power", ctypes.c_uint8, 1),
        ("AY_temp", ctypes.c_uint8, 1),
        ("AY_pos", ctypes.c_uint8, 1),
        ("BX_power", ctypes.c_uint8, 1),
        ("BX_temp", ctypes.c_uint8, 1),
        ("BX_pos", ctypes.c_uint8, 1),
        ("BY_power", ctypes.c_uint8, 1),
        ("BY_temp", ctypes.c_uint8, 1),
        ("BY_pos", ctypes.c_uint8, 1),
        ("auto_stop", ctypes.c_uint8, 1),
    ]


class RtcLaserCtrl(ctypes.Union):
    _fields_ = [("bits", RtcLaserControlBits), ("ulData", ctypes.c_uint32)]


# classes for complex shapes
class RtcLine:
    def __init__(self, node1=None, node2=None):
        self.node1 = node1
        self.node2 = node2

    def invert(self):
        temp = self.node1
        self.node1 = self.node2
        self.node2 = temp

    def flip_x(self):
        temp = self.node2[0]
        self.node2[0] = self.node1[0]
        self.node1[0] = temp

    def distance(self):
        distance = math.sqrt(
            (self.node1[0] - self.node2[0]) ** 2 + (self.node1[1] - self.node2[1]) ** 2
        )
        if self.node2[1] < self.node1[1]:
            distance *= -1
        return distance


class RtcOutLine:
    def __init__(self, points, contours):
        self.points = points
        self.contours = contours
