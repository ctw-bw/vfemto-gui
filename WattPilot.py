import SerialCOM
import LoadParamFile
import logging
import time

RWSerial = SerialCOM.RWSerial()
OpenFile = LoadParamFile.OpenFile()


class WattPilot:
    def __init__(self):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def initialize_Watt_Pilot(self):
        self.serial_port = RWSerial.search_serial()
        self.load_calib_file()
        self.cur_pos = self.go_to_hardware_home()
        return self.cur_pos

    def load_calib_file(self):
        """from the parameter_list the required parameters are loaded.
        :this can be done using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        The parameter that is being loaded is in text format so to convert it to a float or int use float() or int()
        the desired setting name can be chosen with any character except :, this is the seperator that seperates the
        setting name and setting value.
        the parameter_file is constructed in the following manner:
        setting name 1 : value
        setting name 2 : value
        setting name 3 : value
        """
        parameter_list = self.load_parameter_file()

        self.min_pos = int(
            OpenFile.retrieve_data_from_file(parameter_list, "PowerModulator MIN Steps")
        )
        self.max_pos = int(
            OpenFile.retrieve_data_from_file(parameter_list, "PowerModulator MAX Steps")
        )
        self.min_watt = float(
            OpenFile.retrieve_data_from_file(parameter_list, "PowerModulator MIN Watt")
        )
        self.max_watt = float(
            OpenFile.retrieve_data_from_file(parameter_list, "PowerModulator MAX Watt")
        )

    def load_parameter_file(self):
        """this code loads the entire file containing the parameters for the setup. this file is located
         in the settings-directory
        :located inside the settings directory located at the same folder-level as the current directory
         (settings\init_parameters.txt)
        :from the file the parameters are loaded into a list, called the parameter_list
        :from this list the required parameters can be retrieved using the
         OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        :this is done in the def load_parameters, found below)
        :return:
        """
        file_path, parameter_list = LoadParamFile.LoadSettings().load_settings_file()
        return parameter_list

    def set_power(self, power=0):
        """set laser power as percentage whitin calbration values. The power can never be less then 0 or
        more then 100, it can be a float value. If no exact step is available, the step-value will be rounded off
         to the nearest full step (the rounded off power percentage is returned after the movement has finished).
        power value:
        power = 0 is min available AFTER the Watt Pilot (so for example 0.3 Watt)
        power = 100 is max availeble power AFTER the laser has passed through the Watt Pilot (for example 95% of
        original laser power)
        """

        if power < 0:
            power = 0
            self.logger.debug("set laser power percentage too low, reset to 0%")

        if power > 100:
            power = 100
            self.logger.debug("set laser power percentage too high, reset to 100%")

        steps_from_min_to_max = self.max_pos - self.min_pos
        steps_per_percentage = steps_from_min_to_max / 100
        step_inc = round(steps_per_percentage * power)
        set_power = (
                step_inc / steps_per_percentage
        )  # recalculate set power, this can be different than the input power value because it has been rounded off
        set_steps = step_inc + self.min_pos

        self.abs_move(set_steps)
        return set_power

    def set_power_W(self, watt=0):
        """set laser power as absolute WATT value whitin calbration values.
        the set value can never be more or less than the calibration values.
        If it is reset to the min or max calibration value depending if the set wattage is too low or too high.
        the watt value:
        lowest value = Watt Pilot MIN power WATT parameter in parameter file
        highest value = Watt Pilot MAX power WATT parameter in parameter file
        """
        if watt < self.min_watt:
            watt = self.min_watt
            print(
                "set laser power wattage too low, reset to Watt Pilot MIN power WATT parameter in parameter file"
            )
            self.logger.debug(
                "set laser power wattage too low, reset to Watt Pilot MIN power WATT parameter in parameter file"
            )
        if watt > self.max_watt:
            watt = self.max_watt
            print(
                "set laser power wattage too low, reset to Watt Pilot MIN power WATT parameter in parameter file"
            )
            self.logger.debug(
                "set laser power wattage too high, reset to Watt Pilot MAX power WATT parameter in parameter file"
            )
        power_from_min_to_max = self.max_watt - self.min_watt
        steps_from_min_to_max = self.max_pos - self.min_pos
        steps_per_watt = steps_from_min_to_max / power_from_min_to_max
        steps_for_set_wattage = round(steps_per_watt * watt)
        set_wattage = steps_for_set_wattage / steps_per_watt

        set_steps = steps_for_set_wattage + self.min_pos

        self.abs_move(set_steps)
        return set_wattage

    def rel_move(self, x):
        """move motor by x steps,
        Parameter x:
        Integer number. Can be positive (motor turns clockwise)
        and negative (motor turns counterclockwise). Place “-“ for negative notation.
        x can be in range of 2 147 483 646..+2147483646.
        Example:
        “m 1000” to move 1000 steps clockwise and “m -1000” to
        move 1000 steps counterclockwise.
        """
        self.logger.debug("powermodulator relative move : {}".format(x))
        command = "m " + str(x)
        message = RWSerial.write_to_serial(command)
        cur_pos = self.wait_for_movement_to_finish()
        return cur_pos

    def abs_move(self, x):
        """Go to absolute coordinate.
        Parameter x:
        Integer number. Can be positive and negative. Place “-“ for negative notation.
        x can be in range of 2 147 483 646..+2147 483 646.
        Example:
        “g -400” – motor turns while internal step counter reaches 400.
        Then send “m 1000” to move 1000 steps clockwise.
        Now motor stands in 600 position
        """
        self.logger.debug("powermodulator absolute move : {}".format(x))
        command = "g " + str(x)
        message = RWSerial.write_to_serial(command)
        cur_pos = self.wait_for_movement_to_finish()
        return cur_pos

    def set_coordinate_counter(self, x):
        """Set coordinate counter to specific value.
        Parameter x:
        Integer number. Can be positive and negative. Place “-“ for negative notation. x can be
        in range of 2 147 483 646..+2147 483 646
        Example:
        “i 625” – set coordinate to 625. Now send “so” to save position to controller.
        """
        self.logger.debug("powermodulator set coordinate counter: {}".format(x))
        command = "i " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def reset_coordinate_counter(self):
        """ Resets coordinate counter to 0."""
        self.logger.debug("powermodulator reset coordinate counter")
        command = "h"
        message = RWSerial.write_to_serial(command)
        return message

    def smooth_stop(self):
        """Stop motor smoothly if it is currently running. This is preferred command to stop motor instead of “b”."""
        self.logger.debug("powermodulator smooth stop")
        command = "st"
        message = RWSerial.write_to_serial(command)
        cur_pos = self.wait_for_movement_to_finish()
        return cur_pos

    def emerg_stop(self):
        """Bake movement immediately.
        This command stops motor, but step counter accuracy can degrade using watt pilot."""

        self.logger.debug("powermodulator emergency stop")
        command = "b"
        message = RWSerial.write_to_serial(command)
        cur_pos = self.wait_for_movement_to_finish()
        return cur_pos

    def go_to_hardware_home(self):
        """set current position of stepper to 0"""
        self.logger.debug("powermodulator go to hardware home")
        command = "zp"
        message = RWSerial.write_to_serial(command)
        cur_pos = self.wait_for_movement_to_finish()
        return cur_pos

    def set_micro_stepping_res(self, x):
        """Set motor micro stepping resolution.
        Parameter x:
        1 Motor is driven in full steps mode. Waveplate holder turns once in 15600 steps for standard attenuator
         or 36000 for big aperture.
        2 Half step mode. Waveplate holder turns once in 31200 steps.
        4 Quarter step mode. Waveplate holder turns once in 62400 steps.
        8 Eight step mode. Waveplate holder turns once in 124800 steps.
        6 Sixteen step mode. Waveplate holder turns once in 249600 steps.
        Higher micro stepping levels demonstrate better position accuracy and no motor resonance.
        It is advisable to use half stepping operation mode.
        """
        self.logger.debug("powermodulator set microstepping resolution : {}".format(x))
        command = "r " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def set_idle_current(self, x):
        """Set motor current then it is idles. This removes motor heat. Some amount of current must be left in order
         to keep position accuracy.
        Parameter x:
        Integer number in range of from 0 to 255. Motor current can be calculated using such equation: I = 0.00835x (A)
        """
        self.logger.debug("powermodulator set idle current : {}".format(x))
        command = "ws " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def set_running_current(self, x):
        """Set motor current then it moves.
        Parameter x:
        Integer number in range of from 0 to 255. Motor current can be calculated using such equation: I = 0.00835x (A)
        """
        self.logger.debug("powermodulator set running current : {}".format(x))
        command = "wm " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def set_acceleration(self, x):
        """Set acceleration.
        Parameter x:
        Integer number in range of from 0 to 255. 1 is the lowest acceleration and 255 is the highest.
        0 turns off acceleration. Turning on acceleration helps to increase position repeatability.
        """
        self.logger.debug("powermodulator set acceleration : {}".format(x))
        command = "a " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def set_decelleration(self, x):
        """Set deceleration.
        Parameter x:
        Integer number in range of from 0 to 255. 1 is the lowest deceleration and 255 is the highest.
        0 turns off deceleration. Turning on deceleration helps to increase position repeatability.
        """
        self.logger.debug("powermodulator set deceleration : {}".format(x))
        command = "d " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def show_controller_settings(self):
        """Show controller settings, related to USB mode.
        Type this command only when using terminal and manual command entering.
        This is the way to see fundamental settings in “eye friendly” fashion.
        To get controller settings for software programming, use “pc” command instead.
        This command can be used to “ping” controller (to check if controller is attached to particular COM port).
        If device response to “p\r” string begins with “pUSB:“,
        it means that Watt Pilot is attached and is turned on

        see wattpilot for detailed explanation
        """
        # self.logger.debug("powermodulator show controller settings : {}".format(x))
        command = "p"
        message = RWSerial.write_to_serial(command)
        controller_settings = RWSerial.wait_for_response()
        return controller_settings

    def show_controller_settings_pc_mode(self):
        """Show all controller settings, separated by semicolon (;).
        Use this command when programming computer software to read all settings.
        Return string (finished with 0x0A and 0x0D symbols):
        """
        # self.logger.debug("powermodulator show controller settings pc mode : {}".format(x))
        command = "pc"
        message = RWSerial.write_to_serial(command)
        controller_settings_pc_mode = RWSerial.wait_for_response()
        return controller_settings_pc_mode

    def return_running_state(self):
        """Return running state of the motor and current position.
        Return string (finished with 0x0A and 0x0D symbols):
        [1];[2]

        [1] Integer 0, 1, 2 or 3. Current motor run state:
                                    0 – motor is stopped,
                                    1 – accelerating,
                                    2 – decelerating
                                    3 – running at constant speed;
        [2] Integer in range of -2147483646..+2147483646. Current motor position;
        Response example:
        3;4437
        Use this command to determine if motor has done its movement. After
        issuing any move command, poll “o” command in time intervals about
        250 ms and decode response. If [1] parameter become 0, it means that
        motor has stopped and is ready for next move command.
        """
        command = "o"
        message = RWSerial.write_to_serial(command)
        cur_pos = RWSerial.wait_for_response()
        return cur_pos

    def motor_enable(self, x):
        """Motor enable.
        Parameter x:
        1 Motor is enabled.
        0 Motor is disconnected.
        """
        self.logger.debug("powermodulator motor enable")
        command = "en " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def reset_controller(self):
        """Reset controller.
        Controller resets in 4 s after issuing this command.
        It is equivalent to power cycle.
        This command can also enter into firmware upgrade mode if firmware upgrade button is pressed.
        All changed settings and position are restored in previous state,
        if “ss” and/or “so” commands was not issued before reset.
        Then controller starts, it sends string “USB mode” if nothing is connected to 15 pin D-SUB connector in front.
        """
        self.logger.debug("powermodulator reset controller")
        command = "j"
        message = RWSerial.write_to_serial(command)
        return message

    def turn_off_acceleratation(self):
        """turn off acceleration"""
        self.logger.debug("powermodulator turn off acceleration")
        command = "a 0"
        message = RWSerial.write_to_serial(command)
        return message

    def turn_off_deceleratation(self):
        """turn off deceleration"""
        self.logger.debug("powermodulator turn off deceleration")
        command = "d 0"
        message = RWSerial.write_to_serial(command)
        return message

    def save_controller_settings(self):
        """Save settings of controller.
        Save configuration mentioned in "pc" command description (See manual of watt pilot) to controller memory.
        Configuration saved by "ss" command wil be restored on controller power on.
        """
        self.logger.debug("powermodulator save controller settings")
        command = "ss"
        message = RWSerial.write_to_serial(command)
        return message

    def write_name_to_controller(self, x):
        """Write 20 character long name to controller
        Parameter x:
        20 characters. If x is not 20 symbols, then returned name can consist of unreadable characters.
        Please space pad trailing name.
        """
        self.logger.debug("powermodulator write name to controller : {}".format(x))
        command = "sn " + str(x)
        message = RWSerial.write_to_serial(command)
        return message

    def show_controller_name(self):
        """Show name.
        Returns 20 character string, saved by “sn” command.
        Return: 20 character string
        """
        self.logger.debug("powermodulator show controller name")
        command = "n"
        message = RWSerial.write_to_serial(command)
        controller_name = RWSerial.wait_for_response()
        return controller_name

    def wait_for_movement_to_finish(self):
        """wait for the movement of the stepper motor to complete.
        this is done by asking the controller the movement status every 250ms, as recomended by the documentation.
        the return running state function is called to ask the controller moving status.
        If the answer starts with a 0 the movement has finished.
        If the movement has not finished within 1 minute an error is given.
        """
        wait_time = 60  # wait for the movement to finish for at most 60 seconds
        time_out = time.clock() + wait_time
        while time_out - time.clock() >= 0:
            # print("wait for movement to finish")
            # if (self.message != []):
            time.sleep(
                0.1
            )  # waittime before sending next command asking the position of the watt pilot
            running_state = self.return_running_state()
            # print("running state : ", running_state[1])

            if int(running_state[1]) == 0:
                # print("set power reached")
                self.logger.debug("set power reached")
                self.cur_pos = int(running_state[3::])
                return self.cur_pos
                # break

        if time_out - time.clock() <= 0:
            self.logger.debug("no message recieved from selected serial port")
            return "NO RESP"

            # print time_out - time.clock()

        return self.cur_pos
