"""
provides interfaces to the Scanlab RTC5 card

currently only one head per card is used.


"""

import os.path

# import freetype
import string
import logging

# noinspection PyUnresolvedReferences
import exceptions

# noinspection PyUnresolvedReferences
import rtc

__author__ = "Gerald Ebberink"
__license__ = "GPL v2"
__version__ = "0.0.1"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@utwente.nl"
__status__ = "Prototype"


def setup_loggers(file_level="DEBUG", console_level="INFO", filename=None):
    # set up logging to a file
    # from https://docs.python.org/2/howto/logging-cookbook.html#logging-to-multiple-destinations
    if filename is None:
        filename = "logfile.log"
    file_level = getattr(logging, file_level.upper(), logging.DEBUG)
    console_level = getattr(logging, console_level.upper(), logging.INFO)

    logging.basicConfig(
        level=file_level,
        format="%(asctime)s %(levelname)-8s %(name)-20s  %(message)s",
        filename=filename,
        filemode="w",
    )
    # define a Handler which writes INFO messages or higher to sys.stderr
    console = logging.StreamHandler()
    console.setLevel(console_level)
    formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
    console.setFormatter(formatter)
    logging.getLogger(__name__).addHandler(console)


# noinspection PyUnresolvedReferences
class Rtc5(rtc.RtcDll):
    """
    This class provides interfaces to the Scanlab RTC5 card

    It has 'private' memebers for the DLL fucntions and accessable functions for use in other programs and classes

    Attributes:
        dll_path = the path to the RTC5DLL.dll file
        dll_initialized = True when the DLL is initialized otherwise False
    """

    def __init__(self):
        """
        Initalizes the class and set all python variables to their defaults
        :return:
        """
        super().__init__()
        self.rtc_path = "C:\\RTC5"
        self.programfile_path = self.rtc_path + "\\RTC5 Files\\"
        self.dll_path = self.programfile_path + "\\RTC5DLL.dll"
        self.headerfile = "RTC5expl.h"
        self.rtc_card_class = Rtc5Card

        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def _rtc_count_cards(self):
        return self._rtc5_count_cards()

    def _init_dll(self, dll=None):
        """
        Initializes the dll and checks for errors
        :param dll: path to the dll. If None is given (as default) the value of self.dll_path is used
        :return: self.dll_initialized is set to True and True is returned
        """
        # noinspection PyProtectedMember
        super()._init_dll(dll=dll)
        if self.dll_initialized:
            self.dll_initialized = False
            errorcode = self._init_rtc5_dll()
            self.logger.debug("initial errorcode: {}".format(errorcode))

            if errorcode != 0:
                rtc5_card_count = self._rtc5_count_cards()
                self.logger.debug("number of RTC5 cards: {}".format(rtc5_card_count))
                if rtc5_card_count > 0:
                    acc_error = 0
                    for card in range(rtc5_card_count):
                        card += 1
                        error = self._n_get_last_error(card)
                        if error != 0:
                            acc_error |= error
                            self.logger.warning(
                                "Card no. {}: Error {} detected".format(card, error)
                            )
                            self._n_reset_error(card, error)
                    if acc_error != 0:
                        self._terminate_dll()
                else:
                    self.logger.error(
                        "initializing the DLL: Error {} detected".format(errorcode)
                    )
                    self._terminate_dll()
            else:
                self.logger.debug("Dll initialized")
                self.dll_initialized = True
        else:
            self.logger("DLL path error: {} does not exist.".format(dll))
            # print("Initializing the DLL: Error file not found\n")

        return self.dll_initialized


# noinspection PyProtectedMember,PyUnresolvedReferences,PyAttributeOutsideInit
class Rtc5Card(rtc.RtcCard):
    """ " To communicate with RTC5 cards

    Most of the functions can be run concurrent for several cards.
    Only the initialize function is depended on the select_rtc and has to be run by itself
    """

    def __init__(self, dll=None, cardno=None):
        super().__init__(dll=dll, cardno=cardno)
        self.correction_file = dll.rtc_path + "\\\Correction Files\\D2_818.ct5"
        # print("correction file location: ", self.correction_file)
        # self.correction_file = dll.rtc_path + '\\Cor_1to1.ct5'
        self.laser_control = rtc.RtcLaserCtrl()
        self.laser_control.ulData = 25  # (bit 1, 3 and 4 high)
        self.rtc_list_class = Rtc5List

    def initialize_card(self):
        """This initializes the card

         Internally it uses _select_rtc so it cannot be run concurrent or multi-threaded

        :return:
        """

        card = self.cardno
        dll = self._dll
        self.logger.debug("card {}: initializing".format(card))

        # TODO: only load the functions used for the card (so not the dll or list)
        self.logger.debug("card {}: loading headerfile functions".format(card))
        dll._load_functions_from_headerfile(other=self)

        if card != self._select_rtc(card):
            errorcode = self._n_get_last_error(card)
            self.logger.debug(
                "card {}: initial error found: {}".format(card, errorcode)
            )

            if errorcode & 256:
                # this means an RTC5_VERSION_MISMATCH
                self.logger.info(
                    "card {}: RTC5_VERSION_MISMATCH trying to recover by loading programfile 0".format(
                        card
                    )
                )
                errorcode = self._n_load_program_file(card, 0)
                self.logger.debug(
                    "card {}: now has errorcode {}".format(card, errorcode)
                )

            if errorcode != 0:
                self.logger.error(
                    "card {}: No access, errorcode{})".format(card, errorcode)
                )
                return errorcode
            else:
                self._select_rtc(card)

        # Now that we have determined to have a valid card without errors we can continue with init
        self._n_stop_execution(card)

        # Now load the correct program file
        if self.program_file_path is None:
            self.program_file_path = dll.programfile_path

        self.logger.debug(
            "card {}: loading programfile {}".format(card, self.program_file_path)
        )
        errorcode = self._load_program_file(self.program_file_path.encode("utf-8"))
        self.logger.debug("card {}: resulted in errorcode: {}".format(card, errorcode))

        self.out_version = dll._get_hex_version()
        self.logger.debug("card {}: hex version: {}".format(card, self.out_version))
        if self.out_version > 3000:
            self.out_version -= 1000
            self.option_3d = True
        self.out_version -= 2000

        self.rtc_version = rtc.RtcVersion()
        self.rtc_version.ulData = dll._get_rtc_version()
        self.rbf_version = self.rtc_version.bits.firmware_version + 500
        self.logger.debug("card {}: rbf version: {}".format(card, self.rbf_version))
        self.serial_number = self._n_get_serial_number(self.cardno)
        self.logger.debug("card {}: serial number: {}".format(card, self.serial_number))
        print("start load correction file in rtc initialize card: ")
        self.load_correction_file()

        # set the basic laser settings
        self.set_laser_mode()
        self.set_laser_control()
        self.get_head_paras()

        self.lists = []
        for listno in range(2):
            listno += 1
            self.logger.debug("card {}: init of list {}".format(card, listno))
            self.lists.append(Rtc5List(self, listno))
        return errorcode

    def load_char(self, charset=0, asciicode=None, outline=None):
        """loads a character into the memory of the RTC5
        :param charset: character set
        :param asciicode: ascii value of the character
        :param outline: a freetype.face.glyph.outline instance
        :return:
        """
        self.logger.debug("card {}: load character".format(self.cardno))
        if asciicode is None or asciicode < 0 or asciicode > 256:
            self.logger.error(
                "card {}: character {} is not in ASCII range".format(
                    self.cardno, asciicode
                )
            )
            raise InputError(asciicode, "expects a ascii index")
        if charset > 3 or charset < 0:
            self.logger.error(
                "card {}: illegal charset {} selected".format(self.cardno, charset)
            )
            raise ValueError(charset, "values should be between 0 and 3 inclusive")
        self.logger.debug(
            "card {}: begin loading char {} in set {}".format(
                self.cardno, asciicode, charset
            )
        )

        char_position = (charset * 256) + asciicode
        self._n_load_char(self.cardno, char_position)

        points = outline.points
        contours = outline.contours
        contours.insert(0, -1)

        i = -1
        for point in points:
            if i in contours:
                self._n_jump_abs(self.cardno, point[0], point[1])
            else:
                self._n_mark_rel(self.cardno, point[0], point[1])
            i += 1
        self._n_jump_abs(self.cardno, int(outline.get_bbox().xMax * 1.2), 0)
        self._n_list_return(self.cardno)
        self.logger.debug("card {}: loaded {} listcommands".format(self.cardno, i + 2))

    def load_ttf(self, filename, size=8, charset=0):
        """Load a true type font with a given size
        :param filename: the filename of the ttf font
        :param size: the size in pixels
        :param charset: load in which set
        :return:
        """
        self.logger.debug("card {}: load truetype font".format(self.cardno))
        face = freetype.Face(filename)
        face.set_char_size(size * 64)
        self.logger.debug(
            "card {}: TTfont file {}, size {}".format(self.cardno, filename, size)
        )
        # let us load the letters:
        for letter in string.ascii_letters:
            face.load_char(letter)
            self.load_char(
                charset=charset, asciicode=ord(letter), outline=face.glyph.outline
            )

        # now the digits
        for digit in string.digits:
            face.load_char(digit)
            self.load_char(
                charset=charset, asciicode=ord(digit), outline=face.glyph.outline
            )

        for punctuation in ".:,;'\"(!?)+-*/=":
            face.load_char(punctuation)
            self.load_char(
                charset=charset, asciicode=ord(punctuation), outline=face.glyph.outline
            )

    def save_list3_file(self, filename=None, mode=None):
        """save list3 to the disk
        :param filename: filename where the data should be stored if none list3.dat is used
        :param mode: 1 - all characters and text strings
                     2 - all subroutines
                     3 - all of the above
        :return: The number of stored commands
        """
        if filename is None:
            filename = os.path.curdir = "list3.dat"
        if mode is None:
            mode = 3
        self.logger.debug(
            "card {}: storing list 3 in {} with mode {}".format(
                self.cardno, filename, mode
            )
        )
        ret = self._n_save_disk(self.cardno, filename, mode)
        self.logger.debug("card {}: stored {} lines".format(self.cardno, ret))
        return ret

    def load_list3_file(self, filename=None, reset=False):
        """Load list 3 from the disk
        :param filename:  filename were the data should be read from, if none list3.dat is used
        :param reset: if True, the list will be initialized (all data removed) before loading.
        :return: the amount of loaded commands
        """
        mode = 1
        if filename is None:
            filename = os.path.curdir = "list3.dat"
        if reset:
            mode = 0

        self.logger.debug(
            "card {}: loading {} into list 3 using mode {}".format(
                self.cardno, filename, mode
            )
        )
        ret = self._n_load_disk(self.cardno, filename, mode)
        self.logger.debug("card {}: read {} lines".format(self.cardno, ret))

        return ret

    def load_correction_file(self, filename=None, table=None, dimensions=None):
        """load correction file from file and store it in table with dimension
        :param filename: the filename if None, the self.correction_file will be used
        :param table: the table if None 1 will be used
        :param dimensions: 2 or 3 for two or three D correction files. If None 2D will be used.
        :return:
        """
        print("correction file in rtc.py: ", self.correction_file)
        self.logger.debug("card {}: loading correctionfile".format(self.cardno))
        if filename is None:
            filename = self.correction_file.encode("UTF-8")
        if table is None:
            table = 1
        if dimensions is None:
            dimensions = 2
        error = self._n_load_correction_file(self.cardno, filename, table, dimensions)
        self.correction_file = filename
        self.logger.debug(
            "card {}: loading {}, table {}, dim {}".format(
                self.cardno, filename, table, dimensions
            )
        )
        self.logger.debug("card {}: resulted in error {}".format(self.cardno, error))
        if error == 0:
            self.select_cor_table(table, 0)
        return error

    def get_head_para(self, parameter=None, headno=None):
        """get a single parameter from the given head.
        :param parameter: parameter number or name if None equals None
        :param headno: headnumber if None equals 1
        :return:
        """
        paras = [
            "type",
            "factor_K",
            "wd",
            "stretch_X",
            "stretch_Y",
            "c_A",
            "c_B",
            "c_C",
            "correction_file",
            "F-theta",
            "source",
            "inverse",
            "angle",
            "geometry",
            "protective",
        ]
        if parameter is None:
            parameter = 1
        elif isinstance(parameter, str):
            parameter = paras.index(parameter)
        if headno is None:
            headno = 1
        ret = self._n_get_head_para(self.cardno, headno, parameter)
        print(ret)
        self.logger.debug(
            "card {}: head {}: get para {} resulted in {}".format(
                self.cardno, headno, paras[parameter], ret
            )
        )

        return ret

    def get_head_paras(self, headno=None):
        """get all the parameters for the selected head and update self.head_parameters with them
        :param headno: head number if None one is used
        :return:
        """
        paras = [
            "type",
            "factor_K",
            "wd",
            "stretch_X",
            "stretch_Y",
            "c_A",
            "c_B",
            "c_C",
            "correction_file",
            "F-theta",
            "source",
            "inverse",
            "angle",
            "geometry",
            "protective",
        ]
        print("get head parameters", len(paras))
        if headno is None:
            headno = 1
        for i in range(len(paras)):
            self.head_parameters[paras[i]] = self.get_head_para(i, headno)
        self.logger.debug(
            "card {}: head {}: get para resulted in {}".format(
                self.cardno, headno, self.head_parameters
            )
        )
        return self.head_parameters

    def set_laser_control(self, control=None):
        """set the laser control parameters as described in the manual
        :param control: when a RtcLaserCtrl instance is given this is directly used for setting the parameters
                        when a dict is given self.laser_control is updated with these values
                        when a interger is given, this is passed to the ulData of the setting

        :return:
        """
        self.logger.debug("card {}: setting laser control".format(self.cardno))
        if isinstance(control, rtc.RtcLaserCtrl):
            self.logger.debug(
                "card {}: received a RtcLaserCtrl instance {}".format(
                    self.cardno, control
                )
            )
            self.laser_control = control
        elif isinstance(control, dict):
            self.logger.debug(
                "card {}: received a dict instance {}".format(self.cardno, control)
            )
            for key in control:
                if hasattr(self.laser_control.bits, key):
                    if control[key] == 0 or control[key] is False:
                        self.logger.debug(
                            "card {}: '{}' set to 0".format(self.cardno, key)
                        )
                        setattr(self.laser_control.bits, key, 0)
                    else:
                        self.logger.debug(
                            "card {}: '{}' set to 1".format(self.cardno, key)
                        )
                        setattr(self.laser_control.bits, key, 1)
                else:
                    self.logger.info(
                        "card {}: key '{}' not recognized".format(self.cardno, key)
                    )
        elif isinstance(control, int):
            self.logger.debug(
                "card {}: received a integer: {}".format(self.cardno, control)
            )
            self.laser_control.ulData = control
        elif control is None:
            pass  # use the current self.laser_control
        else:
            self.logger.info("card {} no useful control received".format(self.cardno))

        self._n_set_laser_control(self.cardno, self.laser_control.ulData)
        self.logger.debug(
            "card {}: laser control set to {}".format(
                self.cardno, self.laser_control.ulData
            )
        )

    def select_cor_table(self, head_a=None, head_b=None):
        super().select_cor_table(head_a=None, head_b=None)
        self.get_head_paras()
        self.logger.debug(
            "card {}: head a: {}, head b: {}".format(self.cardno, head_a, head_b)
        )


# noinspection PyProtectedMember,PyUnresolvedReferences
class Rtc5List(rtc.RtcList):
    def mark_text(self, text=None, listno=0):
        """mark text at current position

        If more then 12 characters a given, it will split the text into marks of 12 chars.

        :param text: a string to be marked
        :param listno: character list (font)
        :return:
        """
        rtc = self._card
        rtc._n_select_char_set(self.cardno, listno)
        self.logger.debug(
            "card {}: list {}: selected character set {}".format(
                self.cardno, self.listno, listno
            )
        )

        if text is not None:
            rtc._n_mark_text(self.cardno, text[:12].encode("UTF-8"))
            self.logger.debug(
                "card {}: list {}: mark text {}".format(
                    self.cardno, self.listno, text[:12].encode("UTF-8")
                )
            )
            if len(text) > 12:
                self.mark_text(text[12:])

    def execute_list(self, position=None):
        """start executing a list with previous loaded commands
        :param position: the position at which to start executing the list (0 if None)
        :return: the number of times the 10 us clock is over run
        """
        rtc = self._card
        if position is None:
            position = 0
        rtc._n_execute_list_pos(self.cardno, self.listno, position)
        self.logger.debug(
            "card {}: list {}: executing list".format(self.cardno, self.listno)
        )

    def load_list(self, position=None):
        """Load this list
        :param position: Start at this position if None postion 0 is used.
        :return: True is succesfull
        """
        rtc = self._card
        if position is None:
            position = 0
        ret = rtc._n_load_list(self.cardno, self.listno, position)
        self.logger.debug(
            "card {}: list {}: load list from position {}".format(
                self.cardno, self.listno, position
            )
        )
        self.logger.debug(
            "card {}: list {}: resulted in: {}".format(self.cardno, self.listno, ret)
        )
        if ret == self.listno:
            ret = True
        return ret
