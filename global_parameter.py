# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 14:17:32 2016

@author: rpo
"""

#import numpy as np

# STAGE POSITIONS
gbl_stage_x_pos = 0.0
gbl_stage_y_pos = 0.0
gbl_stage_z_pos = 0.0
gbl_laser_power = 0.0

# SOFTWARE LIMITS
gbl_stage_x_lim_down = 0.05
gbl_stage_x_lim_up = 150
gbl_stage_y_lim_down = 0.03  # was 0.05
gbl_stage_y_lim_up = 150
gbl_stage_z_lim_down = 0.03
gbl_stage_z_lim_up = 99.7  # was 90 !!
gbl_laser_power_lim_down = 0.05
gbl_laser_power_lim_up = 100

# STOP BUTTON
gbl_super_pause = False
gbl_super_stop = True

# MPT SCAN PARAMETERS
gbl_length_x = 0.1  # mm
gbl_length_y = 0.1  # mm
gbl_vel = 10  # mm/s
gbl_resolution = 500  # pixels
gbl_clk_fq = 125  # MS/s
gbl_volt_range = 0.08  # V

# SYNC VARIABLES
gbl_done_scan = True
gbl_daq_capture = False

# PROCESSING
gbl_threshold = 10000
gbl_slim_up = 0.0
gbl_slim_down = 100000.0

# SAVE IMAGE
gbl_save_img = False

# LOGGING
gbl_avg_power = 0   # W
gbl_rep_rate = 0    # MHz
gbl_pulse_e = 0     # uJ
gbl_pmt_gain = 0

# DICTIONARY
gbl_dict = {
    "gbl_stage_x_pos": gbl_stage_x_pos,
    "gbl_stage_y_pos": gbl_stage_y_pos,
    "gbl_stage_z_pos": gbl_stage_z_pos,
    "gbl_laser_power": gbl_laser_power,
    "gbl_stage_x_lim_down": gbl_stage_x_lim_down,
    "gbl_stage_x_lim_up": gbl_stage_x_lim_up,
    "gbl_stage_y_lim_down": gbl_stage_y_lim_down,
    "gbl_stage_y_lim_up": gbl_stage_y_lim_up,
    "gbl_stage_z_lim_down": gbl_stage_z_lim_down,
    "gbl_stage_z_lim_up": gbl_stage_z_lim_up,
    "gbl_laser_power_lim_down": gbl_laser_power_lim_down,
    "gbl_laser_power_lim_up": gbl_laser_power_lim_up,
    "gbl_super_pause": gbl_super_pause,
    "gbl_super_stop": gbl_super_stop,
    "gbl_length_x": gbl_length_x,
    "gbl_length_y": gbl_length_y,
    "gbl_vel": gbl_vel,
    "gbl_resolution": gbl_resolution,
    "gbl_clk_fq": gbl_clk_fq,
    "gbl_volt_range": gbl_volt_range,
    "gbl_done_scan": gbl_done_scan,
    "gbl_threshold": gbl_threshold,
    "gbl_slim_up": gbl_slim_up,
    "gbl_slim_down": gbl_slim_down,
    "gbl_daq_capture": gbl_daq_capture,
    "gbl_save_img": gbl_save_img,
    "gbl_avg_power": gbl_avg_power,
    "gbl_rep_rate": gbl_rep_rate,
    "gbl_pulse_e": gbl_pulse_e,
    "gbl_pmt_gain": gbl_pmt_gain,
}
