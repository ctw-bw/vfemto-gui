import time
import numpy as np
from PIL import Image
from numba import jit
import png
import multiprocessing as mp
import numexpr as ne

##-- Parameters
pixels = 200
speed = 100000  # um/s
length = 500  # um
stime = length / speed
samplesPerSec = 50 * 10 ** 6
samplesPerLine = int(samplesPerSec * stime)
samplesPerPixel = int(samplesPerLine / pixels)
#print("Samples per pixel: ", samplesPerPixel)


##-- Read decimal bits from file
def readFile():
    volt = np.load('daq-output.npy')
    print("Type of volt data: ", type(volt))
    return volt


def makeImage(data):
    data = np.reshape(data, (pixels, pixels))
    with open("foo.png", 'wb') as f:
        writer = png.Writer(width=data.shape[1], height=data.shape[0], bitdepth=16, greyscale=True)
        data_list = data.tolist()
        writer.write(f, data_list)


####################################################################################
####################################################################################

def main(volt):

    def arrSum(dummy):
        return 0 - np.sum(dummy)

    def toGray(dummy):
        cutoff1 = 0 * (-100000)
        cutoff2 = 5000 * (-100000)
        temp1 = np.array([cutoff1 if x > cutoff1 else x for x in dummy], dtype=np.int64)
        temp2 = np.array([cutoff2 if x < cutoff2 else x for x in temp1], dtype=np.int64)
        arrMin = np.min(temp2)
        arrMax = np.max(temp2)
        #print((arrMin + arrMax) / -(2 * 100000))
        return [65535 * (x - arrMin) / (arrMax - arrMin) for x in temp2]

    '''
    cpu_count = 10
    pool = mp.Pool(cpu_count)
    pool_range = int(pixels**2/cpu_count)

    sumVolt = np.empty(pixels ** 2, dtype=np.dtype('i8'))

    #print(pool_range)
    #print((k + samplesPerPixel) * 10)
    #for i in range(10):
        #for k in range(0, (0 + samplesPerPixel) * pool_range, samplesPerPixel):
            #print(i,k)

    for i in range(pool_range):
        #print(i)  # [i*cpu_count:(i+1)*cpu_count]
        sumVolt = [pool.apply_async(arrSum, args=(volt[k:k + samplesPerPixel])) for k in range(0, int(samplesPerPixel*cpu_count), samplesPerPixel)]

    pool.close()
    print("Ready")

    grayscale = np.array(toGray(sumVolt), dtype=np.uint16)
    
    '''
    k = 0
    sumVolt = np.empty(pixels ** 2, dtype=np.dtype('i8'))
    for i in range(pixels ** 2):
        sumVolt[i] = arrSum(volt[k:k+samplesPerPixel])
        k += samplesPerPixel
    grayscale = np.array(toGray(sumVolt), dtype=np.uint16)

    return grayscale


if __name__ == "__main__":
    volt = readFile()

    start = time.time()
    gray = main(volt)
    print("Processing time:", round(time.time() - start, 6), "seconds")

    makeImage(gray)


