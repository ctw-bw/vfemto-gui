# vFEMTO GUI

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/ctw-bw/vfemto-gui/badge)](https://www.codefactor.io/repository/bitbucket/ctw-bw/vfemto-gui)

Python GUI for the femtosecond laser for Luigi's research.

## Hardware

The PC interacting with the laser has two PCI cards:

 - ScanLab RTC4, for controlling the galvano scanner
 - ATS9146 by Alazar Tech, for capturing signal data

The target PC with the PCI cards needs the appropriate drivers.

We will also need the SDK for the Alazar card.

### RTC4

 1. Download drivers from Scanlab: https://www.scanlab.de/en/products/control-electronics/rtc-software/download (for the RTC4 card for your platform)
 2. There is no executable, so extract the archive somewhere on your computer.
 3. Now open Device Manager and search for the PCI card (probably under 'Unknown Devices')
 4. Right-click on the RTC4 card, click 'Update Drivers' and select 'From computer' when prompted
 5. Navigate to the extracted archive and select the `Disk\Windows\Drivers` directory. Keep 'Include sub-directories' checked.
 6. Windows should identify the right files and install the drivers.

### ATS

Driver:

 1. Download drivers from: https://www.alazartech.com/en/product/ats9146/17
 2. Run the installer and verify in the Device Manager the Alazar card is identified normally.

SDK:

 1. Now download the software development kit from: https://www.alazartech.com/en/product/ats-sdk/27/
    - You will need a key to unlock the downloaded archive. This key can be found on our Confluence.  
 2. Install the SDK with default options.
 3. Make sure to install the latest version of Visual Redistributable: https://docs.microsoft.com/en-US/cpp/windows/latest-supported-vc-redist

## Install

This GUI is based on PyQ4, which is no longer supported. The easiest way to get started is to use Python 3.6 and download the appropriate wheel from a site like https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyqt4.

 1. Create a virtual environment with `python -m venv venv` and activate it with `venv\Scripts\activate`.
 2. Install the wheel for PyQt4.
 3. Install the `atsapi` module from the local SDK install by running `pip install C:\AlazarTech\ATS-SDK\7.5.0\Samples_Python\Library`
 4. Install the other dependencies with `pip install -r requirements.txt`.
    - This will fail for PyQt4 or atsapi if those were not installed manually before.
 5. Run the app like `python .\main_app.py`.

**NOTE:** Getting the necessary libraries is still unclear.
