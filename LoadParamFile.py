__author__ = "Hedzer Durksz"
__copyright__ = "Copyright 2012, 3D Printer"
__credits__ = ["", ""]
__license__ = ""
__version__ = "1.0.0"
__maintainer__ = "Hedzer Durksz"
__email__ = "hedzerdurksz@gmail.com"
__status__ = "Development"  # "Prototype", "Development", or "Production"

""" in this pyfile opening op files and parameters is managed.
Als the saving of file data and parameterdata is managed """

# try:
#    import wx
#    from wx.lib.pubsub import setuparg1
#    from wx.lib.pubsub import pub as Publisher
# except ImportError:
#    raise ImportError,"The wxPython module is required to run this program"

import subprocess
import os, shutil, glob, time
import logging

# import cPickle
# from threading import Thread
from inspect import getsourcefile

# import ErrorMessages
# ErrMsg = ErrorMessages.ErrMsg()

# import DisplayConfig#
# DisplayConfig = DisplayConfig.DisplayConfig()

# import Serial_Arduino
# RWSerial = Serial_Arduino.RWSerial()


class OpenFile:
    def __init__(self):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def file_load(self, loadfilename, seperator=" "):
        """load values from file, and create a list to be read in programm"""
        file = open(loadfilename, "r")
        load_message = []

        for line in file:
            # read each line of the file and convert it to a list that can be read by the program
            line = line.rstrip()
            line = line.split(seperator)
            clean_line = []
            for string in line:
                # print string
                string = string.strip()
                clean_line.append(string)
            # print clean_line
            load_message.append(clean_line)
        file.close()
        return load_message

    # ----------------------------------------------------------------------
    def retrieve_data_from_file(self, message, data_name=""):
        """retrieve data from a message and loof if in the datasource the data name exists
        and what the data name has for value, return the value"""
        # print ("data name = : ", data_name)
        # print ("message =: ", message)
        for data in message:
            # print(data)
            if data[0] == data_name:
                # print("data name found")
                data_name_value = data[1]
                self.logger.debug(
                    "data loaded, data_name = {} : value = {}".format(
                        data_name, data_name_value
                    )
                )
                return data_name_value
        self.logger.debug("no data present: ".format(data_name))
        return "None"


class LoadSettings:
    def __init__(self):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)
        self.OpenFile = OpenFile()

    # ----------------------------------------------------------------------

    def retrieve_settings_file(self, loadfile_path=None, seperator=":"):

        try:
            load_message = self.OpenFile.file_load(loadfile_path, seperator)
        except:
            self.logger.debug("no settings_file present".format(loadfile_path))
            print("hier gaat iets mis")
            return

        # print("load message", load_message)
        return loadfile_path, load_message

    def load_settings_file(self, loadfile_path=None, seperator=":"):
        loadfile_path = self.retrieve_file_path(loadfile_path)
        load_message = self.retrieve_settings_file(loadfile_path)

        # self.LoadPrinterSettings(load_message)
        return load_message

    def retrieve_file_path(self, loadfile_path=None):
        # print("load path %s" %loadfile_path)
        if loadfile_path == None:
            loadfile_path = "init_parameters.txt"
            # get location of the OpenSaveFile
            folder_dir, filename = os.path.split(
                os.path.abspath(getsourcefile(lambda: 0))
            )
            # loadfile_folder = os.path.join(folder_dir, "settings")
            loadfile_path = os.path.join(folder_dir, loadfile_path)
        else:
            loadfile_path = loadfile_path

        # print("loadfile path %s" %loadfile_path)
        self.logger.debug("loading path: {}".format(loadfile_path))
        return loadfile_path


class SaveFile:
    def __init__(self):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def file_save(self, filename, save_message):
        file = open(filename, "w")

        TimeStamp = time.localtime()
        file.write(
            "Date[yyyy-mm-dd]     : %04d-%02d-%02d \n"
            % (TimeStamp[0], TimeStamp[1], TimeStamp[2])
        )
        file.write(
            "Time[hh.mm.ss]       : %02d.%02d.%02d \n"
            % (TimeStamp[3], TimeStamp[4], TimeStamp[5])
        )

        try:
            for message in save_message:
                self.logger.debug(message)
                file.write("%s : %s \n" % (message[0], message[1]))

        except:
            self.logger.debug("File could not be saved")
            file.close()
            return "Error"

        file.close()
        return "done"

    # ----------------------------------------------------------------------
    def create_folder(self, folder_dir, folder_name):
        """ create a folder, if it already exists replace the existing folder"""
        new_dir_path = os.path.join(folder_dir, folder_name)
        self.logger.debug("new dir path: %s" % new_dir_path)
        if os.path.exists(new_dir_path) == True:
            self.logger.debug("folder already exist, will be removed")
            self.RemoveFolder(new_dir_path)

        os.mkdir(new_dir_path)
        return new_dir_path

    def remove_folder(self, folder_dir):
        try:
            shutil.rmtree(folder_dir)
            message = "folder removed"
        except:
            message = "removing folder failed"

        return message

    # ----------------------------------------------------------------------
    def copy_file_to_folder(self, source_file, destination_folder):
        """copy's files to a different folder"""
        self.logger.debug("start copying files")
        # try:
        #    shutil.copy2(source_file, destination_folder)
        # except (IOError, exception):
        #    self.logger.debug("ERROR in OpenSaveFile CopyFileToFolder: " .format(exception))
        return destination_folder

    # ----------------------------------------------------------------------
    def copy_folder_to_folder(self, source_folder, destination_folder):
        """copy's files to a different folder"""
        self.logger.debug("start copying files")
        # try:
        shutil.copytree(source_folder, destination_folder)
        # except IOError, exception:
        #    self.logger.debug("ERROR in OpenSaveFile CopyFolderToFolder: " .format(exception))
        return destination_folder

    # ----------------------------------------------------------------------
    def remove_data_from_file(self, message, data_name=""):
        """retrieve data from a message and loof if in the datasource the data name exists
        and what the data name has for value, return the value"""
        self.logger.debug("remove data from file")
        for data in message:
            if data[0] == data_name:
                # print"data name found"
                data_name_value = data[1]
                data_tuple = data_name, data_name_value
                message.remove(data_tuple)
                return message


# ----------------------------------------------------------------------


class ChangeDataFromFile:
    def change_data_from_file_TUPLES(
        self, message, data_name="", new_data_name_value=""
    ):
        """retrieve data from a message and proof if in the datasource the data name exists
        and what the data name has for value, return the value"""
        self.logger.debug("change data from file")
        for data in message:
            if data[0] == data_name:
                data_name_value = data[1]

                # create old and new tuple
                data_tuple = data_name, data_name_value
                new_data_tuple = data_name, new_data_name_value
                self.logger.debug("new_data_tuple: %s".format(new_data_tuple))

                message[message.index(data_tuple)] = new_data_tuple
        return message

    def change_data_from_file_LISTS(
        self, message, data_name="", new_data_name_value=""
    ):
        """retrieve data from a message and proof if in the datasource the data name exists
        and what the data name has for value, return the value"""
        # print "change data from file"
        data_found = False
        for data in message:
            if data[0] == data_name:
                data_name_value = data[1]

                # create old and new tuple
                data_tuple = [data_name, data_name_value]
                new_data_tuple = [data_name, new_data_name_value]
                # print "new_data_tuple: %s" %(new_data_tuple,)

                message[message.index(data_tuple)] = new_data_tuple
                data_found = True

        if data_found == False:
            new_data_tuple = [data_name, new_data_name_value]
            message.append(new_data_tuple)

        return message

    def remove_data_from_file_LISTS(self, message, data_name=""):
        """retrieve data from a message and proof if in the datasource the data name exists
        and what the data name has for value, return the value"""
        # print "change data from file"

        for data in message:
            if data[0] == data_name:
                data_name_value = data[1]

                # remove data list
                data_tuple = [data_name, data_name_value]
                message.remove(data_tuple)

        return message

    # ------------------------------------------------------------------------------
    # ==============================================================================
    # ------------------------------------------------------------------------------


class SavePrinterSettings:
    def __init__(self):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)
        self.SaveFile = SaveFile()
        self.OpenFile = OpenFile()
        self.ChangeDataFromFile = ChangeDataFromFile()

    def save_selected_settings(self, save_data, savefile_path=None, seperator=":"):
        # savefile_path, savefilename = self.SavePrinterLocation()
        savefile_path = LoadSettings().retrieve_file_path(savefile_path)

        try:
            load_message = self.OpenFile.file_load(savefile_path, seperator)
            # remove time and date stamp
            load_message = self.ChangeDataFromFile.remove_data_from_file_LISTS(
                load_message, "Date[yyyy-mm-dd]"
            )
            load_message = self.ChangeDataFromFile.remove_data_from_file_LISTS(
                load_message, "Time[hh.mm.ss]"
            )

            for data in save_data:
                # print (data [0], data [1])
                load_message = self.ChangeDataFromFile.change_data_from_file_LISTS(
                    load_message, data[0], data[1]
                )

            self.logger.debug(load_message)
            self.SaveFile.file_save(savefile_path, load_message)
        except:
            self.logger.debug(
                "no parameter file present, create one! see documentation how to do that"
            )
            return
        # print load_message
