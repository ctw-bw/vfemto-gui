""" These are all the custom exceptions for the rtc package
"""

__author__ = "Gerald Ebberink"
__license__ = "GPL v2"
__version__ = "0.0.1"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@utwente.nl"
__status__ = "Prototype"


# errors and exceptions
class Error(Exception):
    """Base class for exceptions in this module."""

    pass


class InputError(Error):
    """Exception raised for errors in the input

    Attributes:
        expr -- input expression in which the error occurred
        msg -- explanetion of the error
    """

    def __init__(self, expr, msg):
        self.expr = expr
        self.msg = msg


class CardError(Error):
    """Exception raised for errors with the RTC card

    Attributes:
        expr -- the expression in which the error occured
        msg -- explanation of the error
    """

    def __init__(self, expr, msg):
        self.expr = expr
        self.msg = msg
