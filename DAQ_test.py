from __future__ import division

import ctypes
import time
import atsapi as ats
import numpy as np
from PIL import Image
from numba import jit

pixels = 256
sLength = 368  # um
sVel = 736000  # um/s
sTime = sLength / sVel  # s

print("Time to scan line:", sTime, "s")

samplesPerSec = 125 * 10**6
samplesPerLine = int(samplesPerSec * sTime)
print("Samples per line:", samplesPerLine)
samplesPerPixel = int(samplesPerLine / pixels)
print("Samples per pixel:", samplesPerPixel)


# Configures a board for acquisition
def ConfigureBoard(board):

    board.setCaptureClock(
        ats.INTERNAL_CLOCK, ats.SAMPLE_RATE_50MSPS, ats.CLOCK_EDGE_RISING, 0
    )

    # TODO: Select channel A input parameters as required.
    board.inputControlEx(
        ats.CHANNEL_A, ats.DC_COUPLING, ats.INPUT_RANGE_PM_2_V, ats.IMPEDANCE_50_OHM
    )

    # TODO: Select channel A bandwidth limit as required.
    board.setBWLimit(ats.CHANNEL_A, 0)

    # TODO: Select trigger inputs and levels as required.
    board.setTriggerOperation(
        ats.TRIG_ENGINE_OP_J,
        ats.TRIG_ENGINE_J,
        ats.TRIG_EXTERNAL,
        ats.TRIGGER_SLOPE_POSITIVE,
        220,
        ats.TRIG_ENGINE_K,
        ats.TRIG_DISABLE,
        ats.TRIGGER_SLOPE_POSITIVE,
        128,
    )

    # TODO: Select external trigger parameters as required.
    board.setExternalTrigger(ats.DC_COUPLING, ats.ETR_2V5_50OHM)

    # TODO: Set trigger delay as required.
    triggerDelay_sec = 0
    triggerDelay_samples = int(triggerDelay_sec * samplesPerSec + 8)  # 0.5
    board.setTriggerDelay(triggerDelay_samples)

    # TODO: Set trigger timeout as required.
    #
    # NOTE: The board will wait for a for this amount of time for a
    # trigger event.  If a trigger event does not arrive, then the
    # board will automatically trigger. Set the trigger timeout value
    # to 0 to force the board to wait forever for a trigger event.
    #
    # IMPORTANT: The trigger timeout value should be set to zero after
    # appropriate trigger parameters have been determined, otherwise
    # the board may trigger if the timeout interval expires before a
    # hardware trigger event arrives.
    board.setTriggerTimeOut(0)

    print("Board Configured")


def AcquireDataNPT(board):

    volt = np.array([], dtype=np.dtype('i8'))

    # No pre-trigger samples in NPT mode
    preTriggerSamples = 0

    # TODO: Select the number of samples per record.
    postTriggerSamples = int(samplesPerLine)

    # TODO: Select the number of records per DMA buffer.
    recordsPerBuffer = int(pixels/12)

    # TODO: Select the number of buffers per acquisition.
    buffersPerAcquisition = 12

    # TODO: Select the active channels.
    channels = ats.CHANNEL_A
    channelCount = 0
    for c in ats.channels:
        channelCount += c & channels == c

    # Compute the number of bytes per record and per buffer
    memorySize_samples, bitsPerSample = board.getChannelInfo()
    bytesPerSample = (bitsPerSample.value + 7) // 8
    samplesPerRecord = preTriggerSamples + postTriggerSamples
    bytesPerRecord = bytesPerSample * samplesPerRecord
    bytesPerBuffer = bytesPerRecord * recordsPerBuffer * channelCount

    # Timeout calculation
    samplesPerBuffer = samplesPerRecord * recordsPerBuffer
    timeout = int(100 * (1000 * samplesPerBuffer / samplesPerSec))

    # TODO: Select number of DMA buffers to allocate
    bufferCount = 4

    # Allocate DMA buffers
    sample_type = ctypes.c_uint16

    buffers = []
    for i in range(bufferCount):
        buffers.append(ats.DMABuffer(board.handle, sample_type, bytesPerBuffer))

    board.setRecordSize(preTriggerSamples, postTriggerSamples)
    # Set the record size

    recordsPerAcquisition = recordsPerBuffer * buffersPerAcquisition

    # Configure the board to make an NPT AutoDMA acquisition
    board.beforeAsyncRead(
        channels,
        -preTriggerSamples,
        samplesPerRecord,
        recordsPerBuffer,
        recordsPerAcquisition,
        ats.ADMA_EXTERNAL_STARTCAPTURE | ats.ADMA_NPT,
    )

    # Post DMA buffers to board
    for buffer in buffers:
        board.postAsyncBuffer(buffer.addr, buffer.size_bytes)

    start = time.time()  # Keep track of when acquisition started
    try:
        board.startCapture()  # Start the acquisition
        #        print("Capturing %d buffers. Press <enter> to abort" %
        #              buffersPerAcquisition)
        buffersCompleted = 0
        bytesTransferred = 0
        while buffersCompleted < buffersPerAcquisition:

            # Wait for the buffer at the head of the list of available
            # buffers to be filled by the board.
            buffer = buffers[buffersCompleted % len(buffers)]
            board.waitAsyncBufferComplete(buffer.addr, timeout_ms=timeout)
            buffersCompleted += 1
            bytesTransferred += buffer.size_bytes

            # TODO: Process sample data in this buffer. Data is available
            # as a NumPy array at buffer.buffer

            # NOTE:
            #
            # While you are processing this buffer, the board is already
            # filling the next available buffer(s).
            #
            # You MUST finish processing this buffer and post it back to the
            # board before the board fills all of its available DMA buffers
            # and on-board memory.
            #
            # Samples are arranged in the buffer as follows:
            # S0A, S0B, ..., S1A, S1B, ...
            # with SXY the sample number X of channel Y.
            #
            # A 14-bit sample code is stored in the most significant bits of
            # each 16-bit sample value.
            #
            # Sample codes are unsigned by default. As a result:
            # - 0x0000 represents a negative full scale input signal.
            # - 0x8000 represents a ~0V signal.
            # - 0xFFFF represents a positive full scale input signal.
            # Optionaly save data to file

            volt = np.append(volt, np.array(buffer.buffer))

            # Add the buffer to the end of the list of available buffers.
            board.postAsyncBuffer(buffer.addr, buffer.size_bytes)
    finally:
        board.abortAsyncRead()

    # Compute the total transfer time, and display performance information.
    transferTime_sec = time.time() - start
    print("Capture completed in %f sec" % transferTime_sec)

    if transferTime_sec > 0:
        buffersPerSec = buffersCompleted / transferTime_sec
        bytesPerSec = bytesTransferred / transferTime_sec
        recordsPerSec = recordsPerBuffer * buffersCompleted / transferTime_sec
        #print("Captured %d buffers (%f buffers per sec)" % (buffersCompleted, buffersPerSec))
        #print("Captured %d records (%f records per sec)" % (recordsPerBuffer * buffersCompleted, recordsPerSec))
        print("Transferred %d bytes (%f bytes per sec)" % (bytesTransferred, bytesPerSec))

    return volt


def AcquireDataTS(board):

    volt = np.array([], dtype=np.dtype('i8'))

    acquisitionLength_sec = sTime*pixels*2
    print(acquisitionLength_sec)
    samplesPerBuffer = 20480000

    channels = ats.CHANNEL_A
    channelCount = 0
    for c in ats.channels:
        channelCount += (c & channels == c)

    # Compute the number of bytes per record and per buffer
    memorySize_samples, bitsPerSample = board.getChannelInfo()
    bytesPerSample = (bitsPerSample.value + 7) // 8
    bytesPerBuffer = bytesPerSample * samplesPerBuffer * channelCount
    # Calculate the number of buffers in the acquisition
    samplesPerAcquisition = int(samplesPerSec * acquisitionLength_sec + 0.5)
    buffersPerAcquisition = ((samplesPerAcquisition + samplesPerBuffer - 1) //
                             samplesPerBuffer)

    bufferCount = 4
    sample_type = ctypes.c_uint16

    buffers = []
    for i in range(bufferCount):
        buffers.append(ats.DMABuffer(board.handle, sample_type, bytesPerBuffer))

    board.beforeAsyncRead(channels,
                          0,  # Must be 0
                          samplesPerBuffer,
                          1,  # Must be 1
                          0x7FFFFFFF,  # Ignored
                          ats.ADMA_EXTERNAL_STARTCAPTURE | ats.ADMA_TRIGGERED_STREAMING)

    # Post DMA buffers to board
    for buffer in buffers:
        board.postAsyncBuffer(buffer.addr, buffer.size_bytes)

    start = time.time()  # Keep track of when acquisition started
    try:
        board.startCapture()  # Start the acquisition
        #print("Capturing %d buffers. Press <enter> to abort" %
              #buffersPerAcquisition)
        buffersCompleted = 0
        bytesTransferred = 0
        while buffersCompleted < buffersPerAcquisition:
            # Wait for the buffer at the head of the list of available
            # buffers to be filled by the board.
            buffer = buffers[buffersCompleted % len(buffers)]
            board.waitAsyncBufferComplete(buffer.addr, timeout_ms=10000)
            buffersCompleted += 1
            bytesTransferred += buffer.size_bytes

            # Process sample data in this buffer.
            volt = np.append(volt, np.array(buffer.buffer))
            #volt = buffer.buffer

            # Add the buffer to the end of the list of available buffers.
            board.postAsyncBuffer(buffer.addr, buffer.size_bytes)
    finally:
        board.abortAsyncRead()

    # Compute the total transfer time, and display performance information.
    transferTime_sec = time.time() - start
    print("Capture completed in %f sec" % transferTime_sec)
    '''
    buffersPerSec = 0
    bytesPerSec = 0
    if transferTime_sec > 0:
        buffersPerSec = buffersCompleted / transferTime_sec
        bytesPerSec = bytesTransferred / transferTime_sec
    print("Captured %d buffers (%f buffers per sec)" %
          (buffersCompleted, buffersPerSec))
    '''
    bytesPerSec = bytesTransferred / transferTime_sec
    print("Transferred %d bytes (%f bytes per sec)" %
          (bytesTransferred, bytesPerSec))
    return volt


# -- Post-processing
def PostProcess(volt):

    #@jit(nopython=True)  # numba.jit decorator
    def arrSum(dummy):
        return 0 - np.sum(dummy)

    #@jit(nopython=True)
    def toGray(dummy, scale_up, scale_down):
        cutoff1 = scale_up * (-100000)
        cutoff2 = scale_down * (-100000)
        temp1 = np.array([cutoff1 if x > cutoff1 else x for x in dummy], dtype=np.int64)
        temp2 = np.array([cutoff2 if x < cutoff2 else x for x in temp1], dtype=np.int64)
        arrMin = np.min(temp2)
        arrMax = np.max(temp2)
        #print((arrMin + arrMax) / -(2 * 100000))  # just for visualizing the average
        return [65535 * (x - arrMin) / (arrMax - arrMin) for x in temp2]

    # User input for post-processing
    scaleLimitUp = 0
    scaleLimitDown = 10000

    # Summation and greyscale conversion
    sumVolt = np.empty(pixels ** 2, dtype=np.dtype('i8'))
    k = 0
    for i in range(pixels ** 2):
        sumVolt[i] = arrSum(volt[k:k+samplesPerPixel])
        k += samplesPerPixel
    greyscale = np.array(toGray(sumVolt, scaleLimitUp, scaleLimitDown), dtype=np.uint16)

    return greyscale


if __name__ == "__main__":

    #-- Configure board
    board = ats.Board(systemId=1, boardId=1)
    ConfigureBoard(board)

    #-- Acquisition NPT
    #timeStart = time.time()
    volt = AcquireDataNPT(board)
    #print("\nTotal acquisition time:", round(time.time() - timeStart, 5), "seconds")

    # -- Acquisition TS
    #timeStart = time.time()
    #volt = AcquireDataTS(board)
    #print("\nTotal acquisition time:", round(time.time() - timeStart, 5), "seconds")
    print(volt.size)


    #-- Processing
    start = time.time()
    greyscale = PostProcess(volt)
    print("Processing time:", round(time.time() - start, 5), "seconds")

    #-- Make image
    #im = Image.new("L", (pixels, pixels))
    #im.putdata(greyscale)
    #im.save("img.png")
    #im.show()

