import socket

# HOST = socket.gethostname()
HOST = "192.168.1.3"
PORT = 4002
FORMAT = "utf-8"
SIZE = 1024


def connect():
    """
    Establishes the connection with WinXP PC.
    Returns: the connection object
    """
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((HOST, PORT))
        return s
    except:
        print("ERROR: connection failed")


def init_stages():
    s = connect()  # Creates a socket obj
    msg = ["init_stages"]  # Make a list with first element as function name
    msg = str(msg)  # Convert list into string
    s.sendall(msg.encode(FORMAT))  # Encode and sent to server
    data = s.recv(SIZE)  # Receive the response from server
    data = data.decode(FORMAT)  # Decode into string
    s.close()  # Close the connection
    print(data)
    return data  # Return message that stages are init ???


def home_axis(ax):
    s = connect()
    msg = ["home_axis", ax]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()


def move_abs(ax, x):
    s = connect()
    msg = ["move_abs", ax, x]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()


def move_rel(ax, dx):
    s = connect()
    msg = ["move_rel", ax, dx]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()


def get_pos(ax):
    s = connect()
    msg = ["get_pos", ax]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    data = s.recv(SIZE)
    data = data.decode(FORMAT)
    s.close()
    return data


def halt_axis(ax):
    s = connect()
    msg = ["halt_axis", ax]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()


def set_vel(ax, vel):
    s = connect()
    msg = ["set_vel", ax, vel]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()


def enable_axis(ax):
    s = connect()
    msg = ["enable_axis", ax]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()


def close_axis(ax):
    s = connect()
    msg = ["close_axis", ax]
    msg = str(msg)
    s.sendall(msg.encode(FORMAT))
    s.close()
