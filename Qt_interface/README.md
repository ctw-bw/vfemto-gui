# QT Interface

The main GUI is made using QT Designer, see the `*.ui` file. After modifying the ui file suing QT Designer, a new related `.py` has to be generated. Do that by running:

```cmd
pyuic4 ./LIFT_GUI.ui -o ./lift_gui.py
```

Or, in case `pyuic4` is not in your path, run it from Python:

```cmd
python -m PyQt4.uic.pyuic .\LIFT_GUI.ui -o .\lift_gui.py
```
