"""
provides interfaces to the Scanlab RTC4 card

currently only one head per card is used.


"""

import ctypes
import os.path
import logging

# noinspection PyUnresolvedReferences
import exceptions
import time
import math
import shapely
import shapely.geometry
import shapely.affinity
import LoadParamFile

OpenFile = LoadParamFile.OpenFile()

__author__ = "Gerald Ebberink"
__license__ = "GPL v2"
__version__ = "0.0.1"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@utwente.nl"
__status__ = "Prototype"


# noinspection PyUnresolvedReferences
class RtcDll:
    """
    This class provides interfaces to the Scanlab RTC4 and RTC5 card

    It has 'private' memebers for the DLL fucntions and accessable functions for use in other programs and classes

    Attributes:
        dll_path = the path to the RTCDLL.dll file
        dll_initialized = True when the DLL is initialized otherwise False
    """

    def __init__(self):
        """
        Initalizes the class and set all python variables to their defaults
        :return:
        """
        self.rtc_path = "C:\\Rtc"
        self.programfile_path = self.rtc_path
        self.dll_path = self.programfile_path + "\\RTCDLLx64.dll"
        self.headerfile = "\\RTCImport\\RTCexpl.h"
        self.dll_initialized = False
        self.default_rtc = 1
        self.option_3d = False
        self.rtc_dll = None
        self.cards = []

        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    # The __enter__ and __exit__ functions can be used in by the with clause
    def __enter__(self):
        """function used for the contextmanager (the with statement)
        :return:
        """
        self.logger.debug("entering rtc context")
        self.init()
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, type, value, traceback):
        """function used for the contextmanager (the with statement)
        :return:
        """
        self.logger.debug("exiting rtc context")
        self._terminate_dll()

    def _load_function(
        self, name, argtypes=None, restype=None, doc=None, private=True, other=None
    ):
        """loads function with "name" from the dll and set the argtypes, restype and doc

        :param name: Name of the function
        :param argtypes: the arguments of the function
        :param restype: the return type of the function
        :param doc: the docstring for the function
        :param private: if set to True (default) an _ is added to the beginning of the function name in python
        :param other: is the Card instance into which the function should be loaded.
        :return:
        """
        if not isinstance(other, RtcCard):
            other = self

        func = getattr(self.rtc_dll, name)
        if argtypes is not None:
            func.argtypes = argtypes
        if restype is not None:
            func.restype = restype
        if doc is not None:
            func.__doc__ = doc
        if private:
            name = "_" + name
        setattr(other, name, func)
        self.logger.debug(
            "function {} loaded into {}: argtypes = {}, restype = {}, private = {}".format(
                name, other, argtypes, restype, private
            )
        )

    def _load_functions_from_headerfile(self, headerfile=None, other=None):
        """loads the functiondefinitions from the headerfile

        This function assumes that the dll-only functions are in the top 26 typedefs
        The drawback of this function is the lack of documentation for the functions so no __doc__ is set.
        :param headerfile: the filepath to the headerfile.
        If None then it is derived from the programfile_path variable
        :param other: is other is none or self only the first 26 typedefs are loaded.
        Otherwise all the functions are loaded.
        :return:
        """
        self.logger.debug("starting to parse c headerfile")

        # TODO: make the system only load the needed ones for the DLL by name instead of just the first 26
        if headerfile is None:
            headerfile = self.programfile_path + self.headerfile
        if other is None:
            other = self
        if other is self:
            endline = 100000
        else:
            # very large number
            endline = 100000
        self.logger.debug("headerfile: {}".format(headerfile))
        self.logger.debug("target: {}".format(other))
        self.logger.debug("endline: {}".format(endline))
        with open(headerfile, "rb") as fp:
            currline = 0
            for line in fp:
                line = line.decode("utf-8")
                if line.startswith("typedef"):
                    currline += 1
                    typedef = line[7:-1]
                    items = typedef.split("(")
                    for item in items:
                        # noinspection PyUnusedLocal
                        item = item.strip("_() ;")

                    restype = items[0].lower().strip().split()
                    if restype[0].strip() == "void":
                        restype = None
                    elif restype[0].strip() == "unsigned":
                        # some how somebody put an unsigned char in the .h file
                        # chars are unsigned by default but we have to make do
                        if restype[1].strip() == "char":
                            restype = "c_" + restype[1].strip()
                            restype = getattr(ctypes, restype)
                        else:
                            restype = "c_u" + restype[1].strip()
                            restype = getattr(ctypes, restype)
                    else:
                        restype = "c_" + restype[0].strip()
                        restype = getattr(ctypes, restype)
                    command = items[1][9:].lower().strip(" *()")
                    if command.endswith("_fp"):
                        command = command[:-3]
                    argtypes = items[2].split(",")
                    arglist = []
                    for argtype in argtypes:
                        args = argtype.strip("();\r ").split()

                        if len(args) == 3:
                            var_name = args[2]
                            if args[0].strip().lower() == "unsigned":
                                # some how somebody put an unsigned char in the .h file
                                # chars are unsigned by default but we have to make do
                                if args[1].strip().lower() == "char":
                                    args = args[1]
                                else:
                                    args = "u" + args[1]
                            else:
                                args = args[1]
                        else:
                            if not args[0] == "void":
                                var_name = args[1]
                            args = args[0]
                        if args == "void":
                            arglist = None
                        elif args.endswith("*"):
                            args = "c_" + args.lower()[:-1]
                            args = getattr(ctypes, args)
                            args = getattr(ctypes, "POINTER")(args)
                            arglist.append(args)
                        elif var_name.startswith("*"):
                            args = "c_" + args.lower()
                            args = getattr(ctypes, args)
                            args = getattr(ctypes, "POINTER")(args)
                            arglist.append(args)
                        elif args.endswith("_PTR"):
                            args = "c_" + args.lower()[:-4]
                            args = getattr(ctypes, args)
                            args = getattr(ctypes, "POINTER")(args)
                            arglist.append(args)
                        else:
                            args = "c_" + args.lower()
                            args = getattr(ctypes, args)
                            arglist.append(args)

                    self._load_function(command, arglist, restype, other=other)
                    if currline is endline:
                        break
        self.logger.debug("last line number: {}".format(currline))
        self.logger.debug("finished to parse c headerfile")

    def _init_dll(self, dll=None):
        """
        Initializes the dll and checks for errors
        :param dll: path to the dll. If None is given (as default) the value of self.dll_path is used
        :return: self.dll_initialized is set to True and True is returned
        """
        self.logger.debug("entering the _init_dll function")

        print(dll)

        if dll is None:
            dll = self.dll_path

        print(dll)

        if os.path.isfile(dll):
            self.rtc_dll = ctypes.WinDLL(dll)
            self.logger.debug("DLL found: {}".format(dll))

            self._load_functions_from_headerfile()
            self.dll_initialized = True

    def _init_cards(self):
        """
        Initialize the cards.

        :return:
        """
        self.logger.debug("initialising cards")
        self.card_count = self._rtc_count_cards()
        self.cards = []
        for card in range(self.card_count):
            self.logger.debug("card no. {} added".format(card + 1))
            self.cards.append(self.rtc_card_class(self, card + 1))
            self.cards[card].initialize_card()
        self.logger.debug("all cards added")

    def _terminate_dll(self):
        """
        Stops all the running processes
        terminates the connection to the RTC5 card.
        :return:
        """
        self.logger.debug("terminating...")
        for card in self.cards:
            self.logger.debug("stop execution of card {}".format(card.cardno))
            card.stop_execution()

        if self.dll_initialized:
            self.logger.debug("terminating dll")
            self._free_rtc_dll()
            # self._rtc5close()
        else:
            self.logger.debug("DLL not initialized")
        self.logger.info("BYE BYE")

    def init(self):
        self.logger.debug("init")
        self._init_dll()
        self._init_cards()


# noinspection PyProtectedMember,PyUnresolvedReferences
class RtcCard:
    """ " To communicate with RTC cards

    Most of the functions can be run concurrent for several cards.
    Only the initialize function is depended on the select_rtc and has to be run by itself
    """

    def __init__(self, dll=None, cardno=None):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)
        self.logger.debug("initializing RTC Card instance")
        self.lists = []
        if not isinstance(dll, RtcDll):
            self.logger.critical("No Dll passed to init")
            raise (ValueError, "No dll given")
        else:
            self._dll = dll

        if cardno is None:
            cardno = 1
        self.cardno = cardno
        self.correction_file = dll.rtc_path + "\\Cor_1to1.ct5"
        self.initialized = False
        self.program_file_path = None
        self.out_version = None
        self.rbf_version = None
        self.option_3d = False
        self.serial_number = None
        self.delays = {
            "laseron": 5,
            "laseroff": 5,
            "jump": 50,
            "mark": 30,
            "polygon": 30,
            "varpoly": 0,
            "direct3d": 0,
            "edgelevel": 0xFFFFFFFF,
            "minjumpdelay": 1,
            "jumplength": 0,
        }
        self.head_parameters = {"factor_K": 1}
        self.logger.debug("Instance is initialized for cardno: {}".format(self.cardno))

    def initialize_card(self):
        """This initializes the card

         Internally it uses _select_rtc so it cannot be run concurrent or multi-threaded

        :return:
        """

        card = self.cardno
        dll = self._dll
        self.logger.debug("card {}: initializing".format(card))

        # TODO: only load the functions used for the card (so not the dll or list)
        self.logger.debug("card {}: loading headerfile functions".format(card))
        dll._load_functions_from_headerfile(other=self)

        self._select_rtc(card)

        # Now load the correct program file
        if self.program_file_path is None:
            self.program_file_path = dll.programfile_path + "\\RTC4D2.hex"

        self.logger.debug(
            "card {}: loading programfile {}".format(card, self.program_file_path)
        )
        errorcode = self._load_program_file(self.program_file_path.encode("utf-8"))
        self.logger.debug("card {}: resulted in errorcode: {}".format(card, errorcode))

        self.out_version = dll._get_hex_version()
        self.logger.debug("card {}: hex version: {}".format(card, self.out_version))
        if self.out_version > 3000:
            self.out_version -= 1000
            self.option_3d = True
        self.out_version -= 2000

        rtc_version = RtcVersion()
        rtc_version.ulData = dll._get_rtc_version()
        self.rbf_version = rtc_version.bits.firmware_version + 500
        self.logger.debug("card {}: rbf version: {}".format(card, self.rbf_version))
        self.serial_number = self._n_get_serial_number(self.cardno)
        self.logger.debug("card {}: serial number: {}".format(card, self.serial_number))

        self.load_correction_file()
        self.load_parameters()

        # set the basic laser settings
        self.set_laser_mode()

        self.lists = []
        for listno in range(2):
            listno += 1
            self.logger.debug("card {}: init of list {}".format(card, listno))
            self.lists.append(self.rtc_list_class(self, listno))
        return errorcode

    def set_laser_source(self, source):
        """The different laser setups require different parameters, therefore the laser source is selected. this parameter defines what parameter is used at which laser source
        options are:
        1:  "pico"      --->    this is the default setting, used for the pico setup.
        2:  "femto" --->    the femto setup requires the laser start and end to be regulated by the sh_on() and sh_off() commands
                            Also the laser power is set using an power modulator (Altechna Watt Pilot)
        """
        self.laser_source = source

    def stop_execution(self):
        """If any list is executing it is stopped
        :return:
        """
        self._n_stop_execution(self.cardno)
        self.logger.debug("card {}: execution stopped".format(self.cardno))

    def get_list_status(self):
        """Get the list status bits

        :return: a RtcListStatus instance filled with the correct bits
        """
        status = RtcListStatus()
        status.ulData = self._read_status()
        self.logger.debug(
            "card {}: get list status {}".format(self.cardno, status.ulData)
        )

        return status

    def set_delay_mode(self, delays=None):
        """set the delay mode
        see RTC manual
        If any of the values is none, the value is taken from self.delays
        :param delays:  a dict with the delays needing to be changed or
                        if None self.delays is used
        :return:
        """
        if isinstance(delays, dict):
            new_delay = self.delays
            new_delay.update(delays)
            self.logger.debug(
                "card {}: updated delays with {}".format(self.cardno, delays)
            )
        elif delays is None:
            new_delay = self.delays
            self.logger.debug(
                "card {}: delays used from self.delays {}".format(
                    self.cardno, self.delays
                )
            )
        else:
            self.logger.error(
                "card {}: No valid delays given using self.delays {}".format(
                    self.cardno, self.delays
                )
            )
            new_delay = self.delays

        self.logger.debug("card {}: delays set:".format(self.cardno))
        self.logger.debug(
            "card {}: varpoly is {}".format(self.cardno, new_delay["varpoly"])
        )
        self.logger.debug(
            "card {}: direct3d is {}".format(self.cardno, new_delay["direct3d"])
        )
        self.logger.debug(
            "card {}: edgelevel is {}".format(self.cardno, new_delay["edgelevel"])
        )
        self.logger.debug(
            "card {}: minjumpdelay is {}".format(self.cardno, new_delay["minjumpdelay"])
        )
        self.logger.debug(
            "card {}: jumplength is {}".format(self.cardno, new_delay["jumplength"])
        )

        self._n_set_delay_mode(
            self.cardno,
            new_delay["varpoly"],
            new_delay["direct3d"],
            new_delay["edgelevel"],
            new_delay["minjumpdelay"],
            new_delay["jumplength"],
        )
        self.delays = new_delay

    def load_varpolydelay(self, filename=None, number=None):
        """load the variable polygon delays from a file
        if None is given for the filename a 1-cos phi is used
        :param filename: the filename as a string
        :param number: the number of the table that should be loaded.
        :return:
        """
        if filename is None:
            filename = 0
        if number is None:
            number = 0
        self.logger.debug(
            "card {}: loading {}, number {}".format(self.cardno, filename, number)
        )
        res = self._n_load_varpolydelay(self.cardno, filename, number)

        if res == -1024 and filename == 0:
            self.logger.info("card {}: using 1 - cos(phi)")
        elif res < 0:
            res *= -1
        else:
            self.logger.error(
                "card {}: during load_varplydelay error {}".format(self.cardno, res)
            )
            raise (
                Error,
                "during load_varplydelay error {} on cardno: {}".format(
                    res, self.cardno
                ),
            )
        return res

    def load_correction_file(self, filename=None, table=None):
        """load correction file from file and store it in table
        :param filename: the filename if None, the self.correction_file will be used
        :param table: the table if None 1 will be used
        :return:
        """

        self.logger.debug("card {}: loading correctionfile".format(self.cardno))
        if filename is None:
            filename = self.correction_file.encode("UTF-8")
        else:
            self.correction_file = filename
        if table is None:
            table = 1
        error = self._n_load_correction_file(
            self.cardno, filename, table, 1, 1, 0, 0, 0
        )
        self.correction_file = filename
        self.logger.debug(
            "card {}: loading {}, table {}".format(self.cardno, filename, table)
        )
        self.logger.debug("card {}: resulted in error {}".format(self.cardno, error))
        if error == 0:
            self.select_cor_table(table, 0)
        return error

    def load_parameter_file(self):
        """this code loads the entire file containing the parameters for the setup. this file is located in the settings-directory
        :located inside the settings directory located at the same folder-level as the current directory (settings\init_parameters.txt)
        :from the file the parameters are loaded into a list, called the parameter_list
        :from this list the required parameters can be retrieved using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        :this is done in the def load_parameters, found below)
        :return:
        """
        path, self.parameter_list = LoadParamFile.LoadSettings().load_settings_file()
        self.logger.debug(self.parameter_list)
        return self.parameter_list

    def load_parameters(self):
        """from the parameter_list the required parameters are loaded.
        :this can be done using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        the desired setting name can be chosen with any character except :, this is the seperator that seperates the setting name and setting value.
        the parameter_file is constructed in the following manner:
        setting name 1 : value
        setting name 2 : value
        setting name 3 : value
        """
        parameter_list = self.load_parameter_file()

    def select_cor_table(self, head_a=None, head_b=None):
        """select the correction table for each head
        :param head_a: an integer from 0 to 3
        :param head_b: an integer from 0 to 3
        :return:
        """
        if head_a is None:
            head_a = 0
        if head_b is None:
            head_b = 0
        self._n_select_cor_table(self.cardno, head_a, head_b)

    def set_laser_mode(self, mode=None):
        """set the laser mode, the input can either be a string, an integer or None
        :param mode: either an integer as per the manual or a value from the list
                    ["CO2", "YAG1", "YAG2", "YAG3", "mode4", "YAG5", "mode6"] if None CO2 is used
        :return:
        """
        laser_modes = ["CO2", "YAG1", "YAG2", "YAG3", "mode4", "YAG5", "mode6"]
        if isinstance(mode, str):
            mode = laser_modes.index(mode)
        elif mode is None:
            mode = 0
        self.logger.debug("card {}: setting laser mode to {}".format(self.cardno, mode))
        self._n_set_laser_mode(self.cardno, mode)

    def write_da(self, da=None, value=None, bits=False):
        """Write a value to the given d/a converter

        :param da: 1 and 2 are valid if none then 1 is used
        :param value: between 0 and 10 if bits is False otherwise a value between 0x000 and 0xFFF
        :param bits: when True value is given in bits
        :return:
        """
        if da is None:
            da = 1
        if value is None:
            value = 0

        if not bits:
            value = (value / 10) * 0x3FF  # (2 ** 12) -1
            value = int(value)

        if value > 0x3FF:
            self.logger.info(
                "card {}: DA channel {} value to high ({})clipped to 0x3FF".format(
                    self.cardno, da, value
                )
            )
            value = 0x3FF
        if value < 0:
            self.logger.info(
                "card {}: DA channel {} value to low ({})clipped to 0x0".format(
                    self.cardno, da, value
                )
            )
            value = 0x0

        self.logger.debug(
            "card {}: DA channel {} set to {} Volt or {} in bits".format(
                self.cardno, da, (value / 0x3FF) * 10, value
            )
        )
        self._n_write_da_x(self.cardno, da, value)

    def sh_on(self):
        """ open shutter to let the laser light pass through"""
        # port = 1
        # self.write_io_port(int(str(10000000000),base=2))
        # self.write_io_port_all(1) #RTC4

    def sh_off(self):
        """ close shutter to block the laser light"""
        # port = 1
        # self.write_io_port(int(str(0000000000000000),base=2))
        # self.write_io_port_all(0) #RTC4

    def write_io_port(self, port=None, value=None):
        """Write a valyue to one of the IO ports available at the RCT card. thes can be found in the Extension socked
        on the board.
        :param port = select which port is set to the given value (on RTC5 card 16 digital ports are available (pin 0 t/m 15 on extension 1 socket)
        :param value = give value of I/O port;  1 = high (3.3v or 5v depends on position of Jumper 1 on RTC card),
                                                0 = low (0v)
        :the available ports on the 25pin sub-d connector are DigitalOUT 0 - 9 and DigitalIN 0-9
        :return:
        """
        if port is None:
            self.logger.info("card {}: IO port is not set".format(self.cardno))

        if value is None:
            self.logger.info(
                "card {}: IO channel {} value is not set".format(self.cardno, port)
            )
        self.logger.debug(
            "card {}: IO port {} set to value {}".format(self.cardno, port, value)
        )

        self._n_write_io_port_mask(self.cardno, value, port)

    def write_io_port_all(self, value=None):
        """set all IO ports on the RTC card to a certain value (low or high).
        :return:
        """
        if value is None:
            self.logger.info(
                "card {}: IO channel {} value is not set".format(self.cardno, port)
            )
        self.logger.debug(
            "card {}: All IO port set to value {}".format(self.cardno, value)
        )

        self._n_write_io_port(self.cardno, value)

    def position(self, bits=False):
        """returns the position as a (x,y) tuple
        :param bits: If set to True returns the bit value
        :return: tuple with (x,y) position"""
        xpos = ctypes.c_short()
        ypos = ctypes.c_short()
        self._n_get_xy_pos(self.cardno, xpos, ypos)
        xpos = xpos.value
        ypos = ypos.value
        if not bits:
            xpos /= self.head_parameters["factor_K"]
            ypos /= self.head_parameters["factor_K"]
        return (xpos, ypos)

    def get_time(self):
        raise (NotImplementedError)

    def reset_time(self):
        raise (NotImplementedError)


# noinspection PyProtectedMember,PyUnresolvedReferences
class RtcList:
    def __init__(self, card=None, listno=None, laser_source="femto"):
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)
        self.set_laser_source(laser_source)
        if not isinstance(card, RtcCard):
            self.logger.error("No RTC given.")
            raise (ValueError, "No RtcCard given")
        if listno is None:
            listno = 1
        self._card = card
        self.cardno = self._card.cardno
        self.listno = listno
        self.delays = card.delays
        self.wait_finish = True
        self.logger.debug(
            "card {}: list {}: instance initialized".format(self.cardno, self.listno)
        )
        self.load_parameters()

    def set_laser_source(self, source):
        """The different laser setups require different parameters, therefore the laser source is selected. this parameter defines what parameter is used at which laser source
        options are:
        1:  "pico"      --->    this is the default setting, used for the pico setup.
        2:  "femto" --->    the femto setup requires the laser start and end to be regulated by the sh_on() and sh_off() commands
                            Also the laser power is set using an power modulator (Altechna Watt Pilot)
        """
        self.laser_source = source

    def load_parameter_file(self):
        """this code loads the entire file containing the parameters for the setup. this file is located in the settings-directory
        :located inside the settings directory located at the same folder-level as the current directory (settings\init_parameters.txt)
        :from the file the parameters are loaded into a list, called the parameter_list
        :from this list the required parameters can be retrieved using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        :this is done in the def load_parameters, found below)
        :return:
        """

        path, self.parameter_list = LoadParamFile.LoadSettings().load_settings_file()
        self.logger.debug(self.parameter_list)
        return self.parameter_list

    def load_parameters(self):
        """from the parameter_list the required parameters are loaded.
        :this can be done using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        The parameter that is being loaded is in text format so to convert it to a float or int use float() or int()
        the desired setting name can be chosen with any character except :, this is the seperator that seperates the setting name and setting value.
        the parameter_file is constructed in the following manner:
        setting name 1 : value
        setting name 2 : value
        setting name 3 : value
        """
        parameter_list = self.load_parameter_file()
        OpenFile.retrieve_data_from_file(parameter_list, "PowerModulator MIN Steps")
        self.shutter_open_wait_time = float(
            OpenFile.retrieve_data_from_file(parameter_list, "Shutter open wait time")
        )
        self.shutter_close_wait_time = float(
            OpenFile.retrieve_data_from_file(parameter_list, "Shutter close wait time")
        )

    def __enter__(self):
        self.load_list()
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, type, value, traceback):
        self.set_end_of_list()
        self.execute_list()
        if self.wait_finish:
            while self.check_busy():
                time.sleep(0.005)

    def mark(self, x, y, relative=True, bits=False):
        """Marks a vector
        :param x: the x coordinate in mm of the position in absolute mode or the dx in relative mode
        :param y: the y coordinate in mm of the position in absolute mode or the dy in relative mode
        :param relative: if False absolute mode is used
        :return:
        """
        rtc = self._card
        if not bits:
            x *= rtc.head_parameters["factor_K"]
            y *= rtc.head_parameters["factor_K"]
        x = int(x)
        y = int(y)
        # if self.laser_source == "femto":# if the femto-setup is used, the laser is enabled by openening the shutter
        #    self.sh_on()
        if relative:
            rtc._n_mark_rel(self.cardno, x, y)
        else:
            rtc._n_mark_abs(self.cardno, x, y)
        # if self.laser_source == "femto":# if the femto-setup is used, the laser is enabled by openening the shutter
        #    #print("closing shutter")
        #    self.sh_off()
        self.logger.debug(
            "card {}: list {}: mark vector to {},{} bits with relative = {}".format(
                self.cardno, self.listno, x, y, relative
            )
        )

    def jump(self, x, y, relative=True, bits=False):
        """jumps to position
        :param x: the x coordinate in mm of the position in absolute mode or the dx in relative mode
        :param y: the y coordinate in mm of the position in absolute mode or the dy in relative mode
        :param relative: if False absolute mode is used
        :return:
        """
        rtc = self._card
        if not bits:
            x *= rtc.head_parameters["factor_K"]
            y *= rtc.head_parameters["factor_K"]
        x = int(x)
        y = int(y)
        if relative:
            rtc._n_jump_rel(self.cardno, x, y)
        else:
            rtc._n_jump_abs(self.cardno, x, y)
        self.logger.debug(
            "card {}: list {}: jump to {},{} bits with relative = {}".format(
                self.cardno, self.listno, x, y, relative
            )
        )

    def arc(self, x, y, angle, relative=True, bits=False):
        """draw an circular arc from the current position
        The center of this circle is located at the x, y position.
        :param x: the x coordinate of the center in absolute mode or the dx in relative mode
        :param y: the y coordinate of the center in absolute mode or the dy in relative mode
        :param angle: the angle to be marked
        :param relative: if False absolute mode is used
        :return:
        """
        rtc = self._card
        if not bits:
            x *= int(rtc.head_parameters["factor_K"])
            y *= int(rtc.head_parameters["factor_K"])

        # if self.laser_source == "femto":# if the femto-setup is used, the laser is enabled by openening the shutter
        #    self.sh_on()
        if relative:
            rtc._n_arc_rel(self.cardno, x, y, angle)
        else:
            rtc._n_arc_abs(self.cardno, x, y, angle)
        # if self.laser_source == "femto":# if the femto-setup is used, the laser is enabled by openening the shutter
        #    self.sh_off()
        self.logger.debug(
            "card {}: list {}: arc cente {},{} with angle {} and relative = {}".format(
                self.cardno, self.listno, x, y, angle, relative
            )
        )

    def ellipse(self, a, b, phi0, phi, x, y, alpha, relative=True, bits=False):
        """Draw an elliptical arc
        The center of this is located at the x, y position.
        :param a: length of long half-axis
        :param b: length of the short half-axis
        :param phi0: beginning phase angle (distance from the end point of a
        :param phi: angle to be marked (defines the length of the mark)
        :param x: center of the ellipse in either absolute x or relative dx
        :param y: center of the ellipse in either absolute y or relative dy
        :param alpha: angle between the X-axis and the a axis of the ellipse
        :param relative: if False absolute mode is used
        :return:
        """
        rtc = self._card
        if not bits:
            x *= int(rtc.head_parameters["factor_K"])
            y *= int(rtc.head_parameters["factor_K"])
            a *= int(rtc.head_parameters["factor_K"])
            b *= int(rtc.head_parameters["factor_K"])
        rtc._n_set_ellipse(self.cardno, a, b, phi0, phi)
        self.logger.debug(
            "card {}: list {}: set ellipse length a,b: {},{} phase start, move: {},{}".format(
                self.cardno, self.listno, a, b, phi0, phi, relative
            )
        )
        # if self.laser_source == "femto":# if the femto-setup is used, the laser is enabled by openening the shutter
        #    self.sh_on()
        if relative:
            rtc._n_mark_ellipse_rel(self.cardno, x, y, alpha)
        else:
            rtc._n_mark_ellipse_abs(self.cardno, x, y, alpha)
        # if self.laser_source == "femto": # if the femto-setup is used, the laser is enabled by openening the shutter
        #    self.sh_off()
        self.logger.debug(
            "card {}: list {}: ellipse center {},{} with angle {} and relative = {}".format(
                self.cardno, self.listno, x, y, alpha, relative
            )
        )

    def execute_list(self, position=None):
        """start executing a list with previous loaded commands
        :param position: the position at which to start executing the list (0 if None)
        :return: the number of times the 10 us clock is over run
        """
        rtc = self._card
        if position is None:
            position = 0
        if position != 0:
            self.logger.info("The RTC4 card has no position indicator for a list")
        rtc._n_execute_list(self.cardno, self.listno)
        self.logger.debug(
            "card {}: list {}: executing list".format(self.cardno, self.listno)
        )

    def load_list(self, position=None):
        """Load this list
        :param position: Start at this position if None postion 0 is used.
        :return: True is succesfull
        """
        rtc = self._card
        if position is None:
            position = 0
        if position != 0:
            self.logger.info("The RTC4 card has no position indicator for a list")
        ret = rtc._n_set_start_list(self.cardno, self.listno)
        self.logger.debug(
            "card {}: list {}: load list ".format(self.cardno, self.listno)
        )
        self.logger.debug(
            "card {}: list {}: resulted in: {}".format(self.cardno, self.listno, ret)
        )
        if ret == self.listno:
            ret = True
        return ret

    def set_start_list(self, a):
        """sets the end of the list os processing will stop here
        :return:
        """
        rtc = self._card
        rtc._n_set_start_list(self.cardno, a)
        # self.logger.debug('card {}: list {}: end of list set'.format(self.cardno, self.listno))

    def set_end_of_list(self):
        """sets the end of the list os processing will stop here
        :return:
        """
        rtc = self._card
        rtc._n_set_end_of_list(self.cardno)
        self.logger.debug(
            "card {}: list {}: end of list set".format(self.cardno, self.listno)
        )

    def set_laser_delays(self, laseron=None, laseroff=None):
        """Sets the laser on and laser off delays.
        Time in microseconds
        if None is given the appropriate value from self.delays is used.
        :param laseron: LaserOnDelay in us
        :param laseroff: LaserOffDelay in us
        :return:
        """
        rtc = self._card
        if laseron is None:
            laseron = self.delays["laseron"]
        if laseroff is None:
            laseroff = self.delays["laseroff"]
        laseron = int(laseron)
        laseroff = int(laseroff)
        rtc._n_set_laser_delays(self.cardno, laseron, laseroff)

        self.logger.debug(
            "card {}: list{}: set laser delays to: {} and {}".format(
                self.cardno, self.listno, laseron, laseroff
            )
        )

    def set_laser_timing(self, half_period, pulse_width1, pulse_width2, time_base):
        rtc = self._card
        rtc._n_set_laser_timing(
            self.cardno, half_period, pulse_width1, pulse_width2, time_base
        )

    def set_scanner_delays(self, jump=None, mark=None, polygon=None):
        """Sets the jump, mark and polygon delays
        Time in microseconds
        if None is given the appropriate value from self.delays is used.
        :param jump: jump delay in us
        :param mark: mark delay in us
        :param polygon: polygon delay in us
        :return:
        """
        rtc = self._card
        if jump is None:
            jump = self.delays["jump"]
        if mark is None:
            mark = self.delays["mark"]
        if polygon is None:
            polygon = self.delays["polygon"]
        jump = int(jump / 10)
        mark = int(mark / 10)
        polygon = int(polygon / 10)
        self.logger.debug(
            "card {}: list {}: scanner delays".format(self.cardno, self.listno)
        )
        self.logger.debug(
            "card {}: list {}: jump: {}".format(self.cardno, self.listno, jump)
        )
        self.logger.debug(
            "card {}: list {}: mark: {}".format(self.cardno, self.listno, mark)
        )
        self.logger.debug(
            "card {}: list {}: polygon: {}".format(self.cardno, self.listno, polygon)
        )
        rtc._n_set_scanner_delays(self.cardno, jump, mark, polygon)

    def set_delay_mode(self, delays=None):
        """set the delay mode
        see RTC5 manual
        If any of the values is none, the value is taken from self.delays
        :param delays:  a dict with the delays needing to be changed or
                        if None self.delays is used
        :return:
        """
        rtc = self._card
        if isinstance(delays, dict):
            new_delay = self.delays
            new_delay.update(delays)
            self.logger.debug(
                "card {}: updated delays with {}".format(self.cardno, delays)
            )
        elif delays is None:
            new_delay = self.delays
            self.logger.debug(
                "card {}: delays used from self.delays {}".format(
                    self.cardno, self.delays
                )
            )
        else:
            self.logger.error(
                "card {}: No valid delays given using self.delays {}".format(
                    self.cardno, self.delays
                )
            )
            new_delay = self.delays

        self.logger.debug(
            "card {}: list {}: delay mode".format(self.cardno, self.listno)
        )
        self.logger.debug(
            "card {}: list {}: varpoly {}".format(
                self.cardno, self.listno, new_delay["varpoly"]
            )
        )
        self.logger.debug(
            "card {}: list {}: direct3d {}".format(
                self.cardno, self.listno, new_delay["direct3d"]
            )
        )
        self.logger.debug(
            "card {}: list {}: edgelevel {}".format(
                self.cardno, self.listno, new_delay["edgelevel"]
            )
        )
        self.logger.debug(
            "card {}: list {}: minjumpdelay {}".format(
                self.cardno, self.listno, new_delay["minjumpdelay"]
            )
        )
        self.logger.debug(
            "card {}: list {}: jumplength {}".format(
                self.cardno, self.listno, new_delay["jumplength"]
            )
        )

        rtc._n_set_delay_mode_list(
            self.cardno,
            new_delay["varpoly"],
            new_delay["direct3d"],
            new_delay["edgelevel"],
            new_delay["minjumpdelay"],
            new_delay["jumplength"],
        )

    def wait(self, time=None, bits=False):
        """wait for amount of time
        :param time: the time to wait, this will be cut to multiples of 10 us
        :param bits: if True time becomes the number of 10 us which should be waited
        :the maximal waiting time is 0.655 seconds.
        :return:
        """
        rtc = self._card
        self.logger.debug("card {}: list {}: wait".format(self.cardno, self.listno))
        if time is None:
            self.logger.debug(
                "card {}: list {}: No time given using 0.003 seconds".format(
                    self.cardno, self.listno
                )
            )
            time = 0.003
        if not bits:
            time *= 1e5
            time = int(time)
        if time < 0:
            time = 0
        elif time > 65500:
            time = 65500
        self.logger.debug(
            "card {}: list {}: wait for {} seconds".format(
                self.cardno, self.listno, time / 1e5
            )
        )
        rtc._long_delay(time)

    def set_mark_speed(self, speed=None, bits=False):
        rtc = self._card
        if speed is None:
            speed = 2.0  # m/s
        if not bits:
            speed *= rtc.head_parameters["factor_K"]
        rtc._set_mark_speed(speed)

    def set_jump_speed(self, speed=None, bits=False):
        rtc = self._card
        if speed is None:
            speed = 2.0  # m/s
        if not bits:
            speed *= rtc.head_parameters["factor_K"]
        rtc._set_jump_speed(speed)

    def write_da(self, da=None, value=None, bits=False):
        """Write a value to the given d/a converter

        :param da: 1 and 2 are valid if none then 1 is used
        :param value: between 0 and 10 if bits is False otherwise a value between 0x000 and 0xFFF
        :param bits: when True value is given in bits
        :return:
        """
        rtc = self._card
        if da is None:
            da = 1
        if value is None:
            value = 0

        if not bits:
            value = (value / 10) * 0x3FF  # (2 ** 12) -1
            value = int(value)

        if value > 0x3FF:
            self.logger.info(
                "card {}: list {}: DA channel {} value to high ({}) clipped to 0x3FF".format(
                    self.cardno, self.listno, da, hex(value)
                )
            )
            value = 0x3FF
        if value < 0:
            self.logger.info(
                "card {}: list {}: DA channel {} value to low ({}) clipped to 0x0".format(
                    self.cardno, self.listno, da, hex(value)
                )
            )
            value = 0x0
        self.logger.debug(
            "card {}: list {}: DA channel {} set to {} Volt or {} in bits".format(
                self.cardno, self.listno, da, (value / 0x3FF) * 10, hex(value)
            )
        )
        rtc._n_write_da_x_list(self.cardno, da, value)

    def sh_on(self):
        """open shutter to allow the laser light to pass through"""
        # port = 1
        self.write_io_port(1)
        # self.write_io_port_all(1) #RTC4
        self.wait(
            self.shutter_open_wait_time
        )  # wait for the shutter to open, time in seconds

    def sh_off(self):
        """ close shutter to block the laser light"""
        # port = 1
        self.write_io_port(0)
        # self.write_io_port_all(0) #RTC4
        self.wait(
            self.shutter_open_wait_time
        )  # wait for the shutter to close, time in seconds

    def write_io_port(self, value=None):  # port = None
        """Write a value to one of the IO ports available at the RCT card. this can be found in the Extension socked
        on the board.
        :param port = select which port is set to the given value (on RTC5 card 16 digital ports are available (pin 0 t/m 15 on extension 1 socket)
        :param value = give value of I/O port;  1 = high (3.3v or 5v depends on position of Jumper 1 on RTC card),
                                                0 = low (0v)
        :return:
        """
        rtc = self._card
        # if port is None:
        # self.logger.info("card {}: IO port is not set".format(self.cardno))

        # if value is None:
        # self.logger.info("card {}: IO channel {} value is not set".format(self.cardno, port))
        # self.logger.debug("card {}: IO port {} set to value {}".format(self.cardno, port, value))

        # rtc._n_write_io_port_mask_list(self.cardno,value,port)
        rtc._write_io_port_list(value)

    def write_io_port_all(self, value=None):
        """set all IO ports on the RTC card to a certain value (low or high).
        :return:
        """
        rtc = self._card
        if value is None:
            self.logger.info(
                "card {}: IO channel {} value is not set".format(self.cardno, port)
            )
        self.logger.debug(
            "card {}: All IO port set to value {}".format(self.cardno, value)
        )

        rtc._n_write_io_port_list(self.cardno, value)

    def laser_on(self, period=None, bits=False):
        """turn the laser on for 'period' amount of seconds (for bits is False)
        if bits is True then the amount is given in multiples of 10 us
        :param period: the amount of seconds the laser needs to be on (if None zero is used)
        :param bits: if False period is in seconds otherwise in 10 us
        """
        rtc = self._card
        if period is None:
            period = 0
        if period < 0:
            period = 0
        if not bits:
            period *= 1e5
            period = int(period)
        if period > 65500:
            self.logger.info(
                "card {}: list {}: period ({}) larger then 0x7FFFFFFF clipped to 0x7FFFFFFF".format(
                    self.cardno, self.listno, period
                )
            )
            period = 65500  # the max for a RTC4

        self.logger.debug(
            "card {}: list {}: laser on for {} s or {} in bits".format(
                self.cardno, self.listno, period / 1e5, hex(period)
            )
        )
        #        if self.laser_source == "femto":# if the femto-setup is used, the laser is enabled by openening the shutter
        #            self.sh_on()
        #            self.wait(period)
        #            self.sh_off
        #        if self.laser_source == "pico":
        rtc._n_laser_on_list(self.cardno, period)

    def check_busy(self):
        """check if the scanner is busy
        :return: True if the scanner is busy
        """
        rtc = self._card
        ret = False
        if self.listno == 1:
            if rtc.get_list_status().bits.busy1 != 0:
                ret = True

        elif self.listno == 2:
            if rtc.get_list_status().bits.busy2 != 0:
                ret = True
        else:
            ret = None
        self.logger.debug(
            "card {}: list {}: check busy returns {}".format(
                self.cardno, self.listno, ret
            )
        )
        return ret

    def position(self, bits=False):
        rtc = self._card
        return rtc.position()

    def mark_linestring(self, line=None, relative=True, bits=False):
        card = self._card

        if not issubclass(type(line), shapely.geometry.LineString):
            raise (TypeError, "No subclass of or shapely.geometry.LineString given")
        else:
            initial = True
            prev_point = shapely.geometry.Point((0, 0))
            for point in line.coords:
                point = shapely.geometry.Point(point)
                if initial:
                    if not relative:
                        self.jump(x=point.x, y=point.y, relative=False, bits=bits)
                    initial = False
                else:
                    track = shapely.geometry.Point(
                        point.x - prev_point.x, point.y - prev_point.y
                    )
                    self.mark(track.x, track.y, relative=True, bits=bits)
                prev_point = shapely.geometry.Point(point)

    def mark_multilinestring(self, multiline=None, relative=True, bits=False):
        if not issubclass(type(multiline), shapely.geometry.MultiLineString):
            raise (TypeError, "No subclass of shapely.geometry.MultiLineString")
        firstline = list(multiline)[0]
        if not relative:
            self.jump(
                firstline.coords[0][0],
                firstline.coords[0][1],
                relative=False,
                bits=bits,
            )
        last_point = shapely.geometry.Point((0, 0))
        for line in list(multiline):
            next_point = shapely.geometry.Point(line.coords[0])
            trace = shapely.geometry.Point(
                next_point.x - last_point.x, next_point.y - last_point.y
            )
            self.jump(trace.x, trace.y, relative=True, bits=bits)
            self.mark_linestring(line=line, relative=True, bits=bits)
            last_point = shapely.geometry.Point(line.coords[-1])

    def mark_shape(self, shape=None, relative=True, bits=False):
        if issubclass(type(shape), shapely.geometry.MultiLineString):
            self.mark_multilinestring(multiline=shape, relative=relative, bits=bits)
        elif issubclass(type(shape), shapely.geometry.LineString):
            self.mark_linestring(line=shape, relative=relative, bits=bits)
        else:
            raise (TypeError, "Not a recognised shape")

    def center(self):
        self.load_list()
        self.jump(0, 0, relative=False)
        self.set_end_of_list()
        self.execute_list()
        while self.check_busy():
            time.sleep(0.005)

    def laser_signal_on(self):
        """turn the laser on"""
        rtc = self._card
        self.logger.debug("card {}: list {}: laser on".format(self.cardno, self.listno))
        rtc._n_laser_signal_on_list(self.cardno)

    def laser_signal_off(self):
        """turn the laser off"""
        rtc = self._card
        self.logger.debug(
            "card {}: list {}: laser off".format(self.cardno, self.listno)
        )
        rtc._n_laser_signal_off_list(self.cardno)

    def set_angle(self, angle=0, radians=False):
        """Change the angle of the scanner X and Y axes
        :param angle: The angle by which the X and Y should be rotated
        :param radians: If True angle is given in radians if False (Default) in degrees
        :return:
        """
        if not radians:
            angle = math.radians(angle)
        self.logger.debug(
            "card {}: angle to be used {}".format(self.cardno, math.degrees(angle))
        )
        matrix = [
            [math.cos(angle), -1.0 * math.sin(angle)],
            [math.sin(angle), math.cos(angle)],
        ]
        self.set_matrix(matrix=matrix)

    def stop_execution(self):
        rtc = self._card
        rtc._n_stop_execution(self.cardno)
        # print('It should stop')


# Bits and bytes
class RtcVersionBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("firmware_version", ctypes.c_uint8, 8),
        ("on_the_fly", ctypes.c_uint8, 1),
        ("second_control", ctypes.c_uint8, 1),
        ("3d_enabled", ctypes.c_uint8, 1),
        ("reserved", ctypes.c_uint8, 5),
        ("DSP_version", ctypes.c_uint8, 8),
        ("firmware_subversion", ctypes.c_uint8, 8),
    ]


class RtcVersion(ctypes.Union):
    _fields_ = [("bits", RtcVersionBits), ("ulData", ctypes.c_uint32)]


class RtcListStatusBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("load1", ctypes.c_uint8, 1),
        ("load2", ctypes.c_uint8, 1),
        ("ready1", ctypes.c_uint8, 1),
        ("ready2", ctypes.c_uint8, 1),
        ("busy1", ctypes.c_uint8, 1),
        ("busy2", ctypes.c_uint8, 1),
        ("used1", ctypes.c_uint8, 1),
        ("used2", ctypes.c_uint8, 1),
    ]


class RtcListStatus(ctypes.Union):
    _fields_ = [("bits", RtcListStatusBits), ("ulData", ctypes.c_uint32)]


class RtcLaserControlBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("pulse_switch", ctypes.c_uint8, 1),
        ("phase_shift", ctypes.c_uint8, 1),
        ("enable", ctypes.c_uint8, 1),
        ("laser_on_signal_level", ctypes.c_uint8, 1),
        ("laser_signal_level", ctypes.c_uint8, 1),
        ("ext_flank", ctypes.c_uint8, 1),
        ("synchronize", ctypes.c_uint8, 1),
        ("constant_pulse", ctypes.c_uint8, 1),
        ("reserved", ctypes.c_uint8, 8),
        ("AX_power", ctypes.c_uint8, 1),
        ("AX_temp", ctypes.c_uint8, 1),
        ("AX_pos", ctypes.c_uint8, 1),
        ("AY_power", ctypes.c_uint8, 1),
        ("AY_temp", ctypes.c_uint8, 1),
        ("AY_pos", ctypes.c_uint8, 1),
        ("BX_power", ctypes.c_uint8, 1),
        ("BX_temp", ctypes.c_uint8, 1),
        ("BX_pos", ctypes.c_uint8, 1),
        ("BY_power", ctypes.c_uint8, 1),
        ("BY_temp", ctypes.c_uint8, 1),
        ("BY_pos", ctypes.c_uint8, 1),
        ("auto_stop", ctypes.c_uint8, 1),
    ]


class RtcLaserCtrl(ctypes.Union):
    _fields_ = [("bits", RtcLaserControlBits), ("ulData", ctypes.c_uint32)]


# classes for complex shapes
class RtcLine:
    def __init__(self, node1=None, node2=None):
        self.node1 = node1
        self.node2 = node2

    def invert(self):
        temp = self.node1
        self.node1 = self.node2
        self.node2 = temp

    def flip_x(self):
        temp = self.node2[0]
        self.node2[0] = self.node1[0]
        self.node1[0] = temp

    def distance(self):
        distance = math.sqrt(
            (self.node1[0] - self.node2[0]) ** 2 + (self.node1[1] - self.node2[1]) ** 2
        )
        if self.node2[1] < self.node1[1]:
            distance *= -1
        return distance


class RtcOutLine:
    def __init__(self, points, contours):
        self.points = points
        self.contours = contours
