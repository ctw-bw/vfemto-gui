"""
provides interfaces to a virtual RTC card,
thus plotting to a figure
"""

import logging
import math

# noinspection PyUnresolvedReferences
import exceptions

# noinspection PyUnresolvedReferences
import virtual

import LoadParamFile

OpenFile = LoadParamFile.OpenFile()

__author__ = "Gerald Ebberink"
__license__ = "GPL v2"
__version__ = "0.2.0"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@utwente.nl"
__status__ = "Prototype"


def setup_loggers(file_level="INFO", console_level="INFO", filename=None):
    # set up logging to a file
    # from https://docs.python.org/2/howto/logging-cookbook.html#logging-to-multiple-destinations
    if filename is None:
        filename = "logfile.log"
    file_level = getattr(logging, file_level.upper(), logging.DEBUG)
    console_level = getattr(logging, console_level.upper(), logging.INFO)

    logging.basicConfig(
        level=file_level,
        format="%(asctime)s %(levelname)-8s %(name)-20s  %(message)s",
        filename=filename,
        filemode="w",
    )
    # define a Handler which writes INFO messages or higher to sys.stderr
    console = logging.StreamHandler()
    console.setLevel(console_level)
    formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
    console.setFormatter(formatter)
    logging.getLogger(__name__).addHandler(console)


# noinspection PyUnresolvedReferences
class vRtc4(virtual.vRtcDll):
    """
    This class provides interfaces to a virtual Scanlab RTC4 card
    """

    def __init__(self):
        """
        Initalizes the class and set all python variables to their defaults
        :return:
        """
        super().__init__()
        self.rtc_path = "C:\\RTC4"
        self.programfile_path = self.rtc_path
        self.dll_path = self.programfile_path + "\\RTC4DLL.dll"
        self.headerfile = "\\RTC4Import\\RTC4expl.h"
        self.rtc_card_class = vRtc4Card
        self.font = {}

        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def _rtc_count_cards(self):
        """Since there are no cards this always returns one
        :return: 1
        """
        return 1


# noinspection PyProtectedMember,PyUnresolvedReferences
class vRtc4Card(virtual.vRtcCard):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rtc_list_class = vRtc4List

    def set_matrix(self, matrix=None):
        """set the transformation matrix
        :param matrix: if None the unity [[1, 0], [0,1]] is used
        """
        self.logger.debug("card {}: set matrix".format(self.cardno))
        if matrix is None:
            matrix = [[1, 0], [0, 1]]
        self.logger.debug("card {}: matrix to be used {}".format(self.cardno, matrix))
        self._matrix = matrix

    def set_angle(self, angle=0, radians=False):
        """Change the angle of the scanner X and Y axes
        :param angle: The angle by which the X and Y should be rotated
        :return:
        """
        if not radians:
            angle = math.radians(angle)
        self.logger.debug(
            "card {}: angle to be used {}".format(self.cardno, math.degrees(angle))
        )
        matrix = [
            [math.cos(angle), -1.0 * math.sin(angle)],
            [math.sin(angle), math.cos(angle)],
        ]
        self.set_matrix(matrix=matrix)

    def set_laser_source(self, source):
        """The different laser setups require different parameters, therefore the laser source is selected. this parameter defines what parameter is used at which laser source
        options are:
        1:  "pico"      --->    this is the default setting, used for the pico setup.
        2:  "femto" --->    the femto setup requires the laser start and end to be regulated by the sh_on() and sh_off() commands
                            Also the laser power is set using an power modulator (Altechna Watt Pilot)
        """
        self.laser_source = source

    def load_parameter_file(self):
        """this code loads the entire file containing the parameters for the setup. this file is located in the settings-directory
        :located inside the settings directory located at the same folder-level as the current directory (settings\init_parameters.txt)
        :from the file the parameters are loaded into a list, called the parameter_list
        :from this list the required parameters can be retrieved using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        :this is done in the def load_parameters, found below)
        :return:
        """
        path, self.parameter_list = LoadParamFile.LoadSettings().load_settings_file()
        self.logger.debug(self.parameter_list)
        return self.parameter_list


# noinspection PyProtectedMember,PyUnresolvedReferences
class vRtc4List(virtual.vRtcList):
    def set_laser_source(self, source):
        """The different laser setups require different parameters, therefore the laser source is selected. this parameter defines what parameter is used at which laser source
        options are:
        1:  "pico"      --->    this is the default setting, used for the pico setup.
        2:  "femto" --->    the femto setup requires the laser start and end to be regulated by the sh_on() and sh_off() commands
                            Also the laser power is set using an power modulator (Altechna Watt Pilot)
        """
        self.laser_source = source

    def load_parameter_file(self):
        """this code loads the entire file containing the parameters for the setup. this file is located in the settings-directory
        :located inside the settings directory located at the same folder-level as the current directory (settings\init_parameters.txt)
        :from the file the parameters are loaded into a list, called the parameter_list
        :from this list the required parameters can be retrieved using the OpenFile.RetrieveDataFromFile(parameter_list, "desired setting name") command.
        :this is done in the def load_parameters, found below)
        :return:
        """

        path, self.parameter_list = LoadParamFile.LoadSettings().load_settings_file()
        self.logger.debug(self.parameter_list)
        return self.parameter_list

    pass
