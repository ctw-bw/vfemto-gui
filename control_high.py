# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 16:22:53 2021

@author: Ihar & Luigi
"""

import time
import threading
import queue
import global_parameter as gb
import ctrl_laser
import client


class main:

    def __init__(self, q1, q2, q3):
        """
        The function which is called when the main-app initializes the script.
        """

        # Create queues (worker,sender...) and bind them to queue objects (q1,q2...)
        self.worker_q = queue.Queue()
        self.worker_q = q1
        self.sender_q = queue.Queue()
        self.sender_q = q2
        self.laser_q = queue.Queue()
        self.laser_q = q3
        self.status_q = queue.Queue()

        # Variables
        self.active = False
        self.ready = False

        # Initialize stages
        self.stages = client
        self.stages.init_stages()

        # Initialize RTC card
        self.ctrl_laser = ctrl_laser.main(self.laser_q)
        self.ctrl_laser.run()

        # Galvo scale factor
        self.K_scale = 5.618  # was 5

        # Create 2 threads that will run worker and sender loops
        self.worker_t = threading.Thread(target=self.worker_loop)
        self.sender_t = threading.Thread(target=self.sender_loop)

        # Dictionary for external function calls
        self.worker_options = {
            "move_abs_x": self.move_abs_x,
            "move_abs_y": self.move_abs_y,
            "move_abs_z": self.move_abs_z,
            "move_rel_x": self.move_rel_x,
            "move_rel_y": self.move_rel_y,
            "move_rel_z": self.move_rel_z,
            "halt_x": self.halt_x,
            "halt_y": self.halt_y,
            "halt_z": self.halt_z,
            "home_x": self.home_x,
            "home_y": self.home_y,
            "home_z": self.home_z,
            "get_pos_x": self.get_pos_x,
            "get_pos_y": self.get_pos_y,
            "get_pos_z": self.get_pos_z,
            "laser_on": self.laser_on,
            "laser_off": self.laser_off,
            "update_laser_power": self.update_laser_power,
            "single_pulse": self.single_pulse,
            "multi_pulse": self.multi_pulse,
            "wait_rtc": self.wait_rtc,
            "set_jump_speed": self.set_jump_speed,
            "set_mark_speed": self.set_mark_speed,
            "galvo_mark": self.galvo_mark,
            "galvo_jump": self.galvo_jump,
            "galvo_home": self.galvo_home,
            "cont_scan": self.cont_scan,
        }

    def worker_loop(self):
        """
        Infinite loop which waits to execute the command in the queue.
        """
        while self.active is True:
            time.sleep(0.01)
            if not self.worker_q.empty():
                item = self.worker_q.get()
                self.worker_options[item[0]](item[1])
                self.worker_q.task_done()

    def sender_loop(self):
        """
        Infinite loop which sends updates to the main_app
        """
        while self.active is True:
            time.sleep(1)
            self.sender_q.put(["update_stage_x", self.get_pos_x()], False)
            self.sender_q.put(["update_stage_y", self.get_pos_y()], False)
            self.sender_q.put(["update_stage_z", self.get_pos_z()], False)
            self.sender_q.put(["update_laser", self.get_laser_power()], False)
            self.sender_q.put(["alive_ctrl", 0], False)
            self.sender_q.put(["alive_laser", 1], False)

    def run(self):
        """
        Starts the threads
        """
        self.active = True
        self.worker_t.start()
        self.sender_t.start()

    def stop(self):
        """
        Closes the threads
        """
        self.active = False
        self.worker_t.join()
        self.sender_t.join()
        self.stages.close_axis(1)  # not tested
        self.stages.close_axis(2)  # not tested

    # ----- CONTINUOUS MPT SCAN --------------------------------------------- #

    def cont_scan(self, dummy):

        # Correcting parameters
        galvoSpeed = dummy[0] * self.K_scale    # [mm/s]
        galvoX = dummy[1] * self.K_scale        # [mm]
        galvoY = dummy[2] * self.K_scale        # [mm]

        pixels = gb.gbl_dict["gbl_resolution"]
        dummy = [galvoSpeed, galvoX, galvoY, 1, pixels]
        gb.gbl_dict["gbl_super_stop"] = False
        time.sleep(0.1)

        # Software sync of scanning
        while gb.gbl_dict["gbl_super_stop"] is False:
            time.sleep(0.01)
            if (gb.gbl_dict["gbl_done_scan"] is True) and (gb.gbl_dict["gbl_daq_capture"] is True):
                gb.gbl_dict["gbl_done_scan"] = False
                self.laser_q.put(["galvo_scan", dummy], False)  # start scan command

        # If stop pushed clear the queue and roll back the variables
        self.worker_q.queue.clear()
        self.laser_q.put(["galvo_stop", 0], False)
        gb.gbl_dict["gbl_done_scan"] = True
        print("[control_high] Stop scan")
        time.sleep(1)

    # ----- COMMANDS FOR GALVO --------------------------------------------- #

    def set_jump_speed(self, dummy):
        self.laser_q.put(["set_jump_speed", dummy], False)

    def set_mark_speed(self, dummy):
        self.laser_q.put(["set_mark_speed", dummy], False)

    def galvo_mark(self, dummy):
        self.laser_q.put(["galvo_mark", dummy], False)

    def galvo_jump(self, dummy):
        self.laser_q.put(["galvo_jump", dummy], False)

    def galvo_home(self, dummy):
        self.laser_q.put(["galvo_home", dummy], False)

    # ----- COMMANDS FOR STAGES --------------------------------------------- #

    def home_x(self, dummy):
        print("Homing stage X")
        self.stages.home_axis(1)

    def home_y(self, dummy):
        print("Homing stage Y")
        self.stages.home_axis(2)

    def home_z(self, dummy):
        print("Homing stage Z")
        self.stages.home_axis(3)

    def move_abs_x(self, pos):

        if gb.gbl_dict["gbl_stage_x_lim_up"] >= pos >= gb.gbl_dict["gbl_stage_x_lim_down"]:
            print("Abs move stage X to " + str(pos))
            self.stages.move_abs(1, pos)
        else:
            print("Stage X OUT OF LIMITS")

    def move_abs_y(self, pos):

        if gb.gbl_dict["gbl_stage_y_lim_up"] >= pos >= gb.gbl_dict["gbl_stage_y_lim_down"]:
            print("Abs move stage y to " + str(pos))
            self.stages.move_abs(2, pos)
        else:
            print("Stage Y OUT OF LIMITS")

    def move_abs_z(self, pos):

        if gb.gbl_dict["gbl_stage_z_lim_up"] >= pos >= gb.gbl_dict["gbl_stage_z_lim_down"]:
            print("Abs move stage Z to " + str(pos))
            self.stages.move_abs(3, pos)
        else:
            print("Stage X OUT OF LIMITS")

    def move_rel_x(self, delta):

        if gb.gbl_dict["gbl_stage_x_lim_up"] >= gb.gbl_dict["gbl_stage_x_pos"] + delta >= \
                gb.gbl_dict["gbl_stage_x_lim_down"]:
            print("Rel move stage X of " + str(delta))
            self.stages.move_rel(1, delta)
        else:
            print("Stage X OUT OF LIMITS")

    def move_rel_y(self, delta):

        if gb.gbl_dict["gbl_stage_y_lim_up"] >= gb.gbl_dict["gbl_stage_y_pos"] + delta >= \
                gb.gbl_dict["gbl_stage_y_lim_down"]:
            print("Rel move stage Y of " + str(delta))
            self.stages.move_rel(2, delta)
        else:
            print("Stage Y OUT OF LIMITS")

    def move_rel_z(self, delta):

        if gb.gbl_dict["gbl_stage_z_pos"] + delta <= gb.gbl_dict["gbl_stage_z_lim_up"] and \
                gb.gbl_dict["gbl_stage_x_pos"] + delta >= gb.gbl_dict["gbl_stage_x_lim_down"]:
            print("Rel move stage Z of " + str(delta))
            self.stages.move_rel(3, delta)
        else:
            print("Stage Z OUT OF LIMITS")

    def halt_x(self, direc):  # not implemented
        # print ("X axis stopped")
        self.stages.halt_axis(1)

    def halt_y(self, direc):  # not implemented
        # print ("Y axis stopped")
        self.stages.halt_axis(2)

    def halt_z(self, direc):  # not implemented
        # print ("Z axis stopped")
        self.stages.halt_axis(3)

    def get_pos_x(self):
        return self.stages.get_pos(1)

    def get_pos_y(self):
        return self.stages.get_pos(2)

    def get_pos_z(self):
        return self.stages.get_pos(3)

    # ----- COMMANDS FOR LASER --------------------------------------------- #

    def laser_on(self, dummy):
        self.laser_q.put(["laser_on", 0], False)

    def laser_off(self, dummy):
        self.laser_q.put(["laser_off", 0], False)

    def update_laser_power(self, dummy):
        self.laser_q.put(["update_laser_power", dummy], False)

    def single_pulse(self, dummy):  # not implemented
        self.laser_q.put(["single_pulse", dummy], False)

    def multi_pulse(self, dummy):  # not implemented
        self.laser_q.put(["multi_pulse", dummy], False)

    def get_laser_power(self):
        return gb.gbl_dict["gbl_laser_power"]

    # ----- COMMANDS FOR RTC4 --------------------------------------------- #

    def wait_rtc(self, deltat):  # not implemented
        self.laser_q.put(['wait_rtc', deltat], False)
