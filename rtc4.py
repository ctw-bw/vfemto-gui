"""
provides interfaces to a RTC 4 card thus creating the shapes on the scanner and (hopefully) in your sample

currently only one head per card is used.
"""

import logging
import math

# noinspection PyUnresolvedReferences
import exceptions

# noinspection PyUnresolvedReferences
import rtc

__author__ = "Gerald Ebberink"
__license__ = "GPL v2"
__version__ = "0.0.1"
__maintainer__ = "Gerald Ebberink"
__email__ = "g.h.p.ebberink@utwente.nl"
__status__ = "Prototype"


def setup_loggers(file_level="DEBUG", console_level="INFO", filename=None):
    # set up logging to a file
    # from https://docs.python.org/2/howto/logging-cookbook.html#logging-to-multiple-destinations
    if filename is None:
        filename = "logfile.log"
    file_level = getattr(logging, file_level.upper(), logging.DEBUG)
    console_level = getattr(logging, console_level.upper(), logging.INFO)

    logging.basicConfig(
        level=file_level,
        format="%(asctime)s %(levelname)-8s %(name)-20s  %(message)s",
        filename=filename,
        filemode="w",
    )
    # define a Handler which writes INFO messages or higher to sys.stderr
    console = logging.StreamHandler()
    console.setLevel(console_level)
    formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
    console.setFormatter(formatter)
    logging.getLogger(__name__).addHandler(console)


# noinspection PyUnresolvedReferences
class Rtc4(rtc.RtcDll):
    """
    This class provides interfaces to the Scanlab RTC4 card

    It has 'private' members for the DLL fucntions and accessable functions for use in other programs and classes

    Attributes:
        dll_path = the path to the RTC4DLL.dll file
        dll_initialized = True when the DLL is initialized otherwise False
    """

    def __init__(self):
        """
        Initalizes the class and set all python variables to their defaults
        :return:
        """
        super().__init__()
        self.rtc_path = "C:\\RTC4"
        #         self.programfile_path = self.rtc_path #Original
        self.programfile_path = self.rtc_path
        self.dll_path = self.programfile_path + "\\RTC4DLLx64.dll"
        #         self.headerfile = '\\RTC4Import\\RTC4expl.h' # Original
        self.headerfile = "\\RTC4expl.h"
        self.rtc_card_class = Rtc4Card
        self.font = {}

        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def _rtc_count_cards(self):
        return self._rtc4_count_cards()


# noinspection PyProtectedMember,PyUnresolvedReferences
class Rtc4Card(rtc.RtcCard):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rtc_list_class = Rtc4List

    def set_matrix(self, matrix=None):
        """set the transformation matrix
        :param matrix: if None the unity [[1, 0], [0,1]] is used
        """
        self.logger.debug("card {}: set matrix".format(self.cardno))
        if matrix is None:
            matrix = [[1, 0], [0, 1]]
        self.logger.debug("card {}: matrix to be used {}".format(self.cardno, matrix))
        self._n_set_matrix(
            self.cardno, matrix[0][0], matrix[0][1], matrix[1][0], matrix[1][1]
        )

    def set_angle(self, angle=0, radians=False):
        """Change the angle of the scanner X and Y axes
        :param angle: The angle by which the X and Y should be rotated
        :param radians: If True angle is given in radians if False (Default) in degrees
        :return:
        """
        if not radians:
            angle = math.radians(angle)
        self.logger.debug(
            "card {}: angle to be used {}".format(self.cardno, math.degrees(angle))
        )
        matrix = [
            [math.cos(angle), -1.0 * math.sin(angle)],
            [math.sin(angle), math.cos(angle)],
        ]
        self.set_matrix(matrix=matrix)


# noinspection PyProtectedMember,PyUnresolvedReferences
class Rtc4List(rtc.RtcList):
    pass
