# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 19:32:15 2016

@author: Luigi 20181015

"""

# import cv2
import sys
import os
import openpyxl
from datetime import date
import time
import queue
import threading
import lift_gui
import control_high
import ctrl_daq
import global_parameter as gb
import png
import numpy as np

from PyQt5 import QtGui
from PyQt5.QtGui import QImage, QPixmap, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow


class MainApp(QMainWindow, lift_gui.Ui_MainWindow):

    def __init__(self):

        """
        The function which is called when the program starts.
        Main job is to initialize the GUI.
        """

        super(self.__class__, self).__init__()
        self.setupUi(self)

        # Define queues for communication between scripts
        self.active = False
        self.status_q = queue.Queue()
        self.laser_q = queue.Queue()
        self.daq_q = queue.Queue()
        self.control_q = queue.Queue()

        # Create a thread for local queue execution
        self.status_t = threading.Thread(target=self.status_loop)

        # Parameters used to check if hardware is initialized
        self.status_laser = 999
        self.status_ctrl = 999
        self.status_zstage = 999
        self.timeout_value = 400

        # Pre-allocation
        self.control_high = None
        self.ctrl_daq = None
        self.image = None

        # Dictionary to accept calls from other scripts
        self.update_options = {
            "update_stage_x": self.update_stage_x,
            "update_stage_y": self.update_stage_y,
            "update_stage_z": self.update_stage_z,
            "update_laser": self.update_laser,
            "alive_ctrl": self.ctrl_alive,
            "alive_laser": self.laser_alive,
            "update_image": self.update_image,
            "save_image": self.save_image,
        }

        # ----- GUI: connecting buttons to functions and defining text fields ----- #

        # -- Movement Stage X
        self.pushButton_donor_x_move_abs.clicked.connect(self.stage_x_move_abs)
        self.lineEdit_donor_x_move_abs.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_donor_x_move_abs.setText("0.000")
        self.pushButton_donor_x_move_rel_pos.clicked.connect(self.stage_x_move_rel)
        self.pushButton_donor_x_move_rel_neg.clicked.connect(self.stage_x_move_rel_neg)
        self.lineEdit_donor_x_move_rel.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_donor_x_move_rel.setText("0.01")
        self.pushButton_donor_x_home.clicked.connect(self.stage_x_home)

        # -- Movement Stage Y
        self.pushButton_donor_y_move_abs.clicked.connect(self.stage_y_move_abs)
        self.lineEdit_donor_y_move_abs.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_donor_y_move_abs.setText("0.000")
        self.pushButton_donor_y_move_rel_pos.clicked.connect(self.stage_y_move_rel)
        self.pushButton_donor_y_move_rel_neg.clicked.connect(self.stage_y_move_rel_neg)
        self.lineEdit_donor_y_move_rel.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_donor_y_move_rel.setText("0.01")
        self.pushButton_donor_y_home.clicked.connect(self.stage_y_home)

        # -- Movement Stage Z
        self.pushButton_zstage_move_abs.clicked.connect(self.stage_z_move_abs)
        self.lineEdit_zstage_move_abs.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_zstage_move_abs.setText("0.000")
        self.pushButton_zstage_move_rel_pos.clicked.connect(self.stage_z_move_rel)
        self.pushButton_zstage_move_rel_neg.clicked.connect(self.stage_z_move_rel_neg)
        self.lineEdit_zstage_move_rel.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_zstage_move_rel.setText("0.005")  # mf1.1c
        self.pushButton_zstage_home.clicked.connect(self.stage_z_home)

        # -- Laser settings
        self.pushButton_laser_power.clicked.connect(self.laser_setPower)
        self.lineEdit_laser_power.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_laser_power.setText("0.000")
        self.pushButton_laser_singlePulse.clicked.connect(self.laser_singlePulse)
        self.lineEdit_laser_numberPulses.setValidator(QtGui.QIntValidator())
        self.lineEdit_laser_numberPulses.setText("0")
        self.pushButton_laser_multiPulse.clicked.connect(self.laser_multiPulse)
        self.pushButton_laser_on.clicked.connect(self.laser_on)
        self.pushButton_laser_on_2.clicked.connect(self.laser_on)
        self.pushButton_laser_off.clicked.connect(self.laser_off)
        self.pushButton_laser_off_2.clicked.connect(self.laser_off)

        # -- Initialization
        self.pushButton_donor_ctrl_start.clicked.connect(self.ctrl_start)
        self.pushButton_donor_ctrl_stop.clicked.connect(self.ctrl_stop)
        self.lineEdit_donor_ctrl_status.setText("Off")

        # -- Stop and Pause Buttons
        self.pushButton_super_pause.clicked.connect(self.super_pause)  # not used
        self.pushButton_super_stop.clicked.connect(self.super_stop)

        # -- Global parameters
        self.comboBox_global.currentIndexChanged.connect(self.global_show)
        self.pushButton_global.clicked.connect(self.global_set)

        # -- Galvo jump to abs zero
        self.pushButton_galvo_home.clicked.connect(self.galvo_home)

        # -- Multiphoton tomography [lc 09-jun-2021]
        self.lineEdit_galvo_speed.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_galvo_speed.setText("500")
        self.lineEdit_x_galvo_scan.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_x_galvo_scan.setText("500")
        self.lineEdit_y_galvo_scan.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_y_galvo_scan.setText("500")
        self.pushButton_galvoscan.clicked.connect(self.galvo_scan)

        self.lineEdit_resolution.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_resolution.setText("500")

        # -- Configure MPT
        self.pushButton_mpt_set.clicked.connect(self.setMPT)

        # -- Post-processing parameters
        self.lineEdit_slim_up.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_slim_up.setText("0")
        self.lineEdit_slim_down.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_slim_down.setText("100")

        # -- Save Image
        self.pushButton_save_img.clicked.connect(self.saveImageButton)

        # -- Logging
        self.lineEdit_avg_power.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_avg_power.setText("0.22")  # Watt
        self.lineEdit_rep_rate.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_rep_rate.setText("15")  # MHz
        self.lineEdit_pulse_e.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_pulse_e.setText("0")  # uJ (set automatically)
        self.lineEdit_pmt_gain.setValidator(QtGui.QDoubleValidator())
        self.lineEdit_pmt_gain.setText("4")

    def status_loop(self):
        """
        Infinite loop which waits to execute the command in the queue.
        """
        while self.active is True:
            self.ctrl_status_update()
            time.sleep(0.002)

            if not self.status_q.empty():
                item = self.status_q.get()
                self.update_options[item[0]](item[1])
                self.status_q.task_done()

    def run(self):
        """
        Starts the thread
        """
        self.active = True
        self.status_t.start()

    def stop(self):
        """
        Closes the threads (incl. of control_high if exists)
        """
        self.active = False
        self.status_t.join()
        if self.control_high is not None:
            self.control_high.stop()

    def ctrl_status_update(self):
        """
        Checks the hardware status.
        Probably can be implemented in a more straightforward way.
        """
        self.status_ctrl += 1

        if self.status_ctrl > self.timeout_value:
            self.lineEdit_donor_ctrl_status.setText("Off")
            self.tab_donor.setDisabled(True)
            self.tab_multiphoton.setDisabled(True)
            self.groupBox_laser.setDisabled(True)
            self.groupBox_gb.setDisabled(True)
            self.groupBox_info.setDisabled(True)
        else:
            self.lineEdit_donor_ctrl_status.setText("Active")
            self.tab_donor.setDisabled(False)
            self.tab_multiphoton.setDisabled(False)
            self.groupBox_laser.setDisabled(False)
            self.groupBox_gb.setDisabled(False)
            self.groupBox_info.setDisabled(False)

    def ctrl_start(self):
        """
        Initializes (opens threads of) two scrips down the hierarchy
        """
        if self.status_ctrl > self.timeout_value:
            try:
                self.control_high = control_high.main(self.control_q, self.status_q, self.laser_q)
                self.control_high.run()
                self.ctrl_daq = ctrl_daq.main(self.daq_q, self.status_q)
                self.ctrl_daq.run()
                self.pushButton_galvoscan.setDisabled(True)
            except:
                print("[main_app]", sys.exc_info()[0], "occurred at line", sys.exc_info()[2].tb_lineno)
                print("ERROR during initialization")

    def ctrl_stop(self):
        """
        Stops (closes threads of) two scrips down the hierarchy
        """
        if self.status_ctrl <= self.timeout_value:
            self.control_high.stop()
            self.ctrl_daq.stop()

    def super_pause(self):
        """
        Pause button which is not used at the moment.
        """
        gb.gbl_dict["gbl_super_pause"] = not gb.gbl_dict["gbl_super_pause"]
        if gb.gbl_dict["gbl_super_pause"]:
            self.pushButton_super_pause.setText("RESUME")
        else:
            self.pushButton_super_pause.setText("PAUSE")

    def super_stop(self):
        """
        Stop button which discontinues the MPT scan
        """
        gb.gbl_dict["gbl_super_stop"] = True

        # Color the laser box
        self.lineEdit_laser.setAutoFillBackground(True)
        self.lineEdit_laser_2.setAutoFillBackground(True)
        p = self.lineEdit_laser.palette()
        p.setColor(self.lineEdit_laser.backgroundRole(), QColor("white"))
        self.lineEdit_laser.setPalette(p)
        p2 = self.lineEdit_laser_2.palette()
        p2.setColor(self.lineEdit_laser_2.backgroundRole(), QColor("white"))
        self.lineEdit_laser_2.setPalette(p2)

    # ----- COMMANDS FOR LASER CONTROL --------------------------------------------- #

    def laser_on(self):
        self.control_q.put(["laser_on", 0], False)
        self.lineEdit_laser.setAutoFillBackground(True)
        self.lineEdit_laser_2.setAutoFillBackground(True)
        p = self.lineEdit_laser.palette()
        p.setColor(self.lineEdit_laser.backgroundRole(), QColor("red"))
        self.lineEdit_laser.setPalette(p)
        p2 = self.lineEdit_laser_2.palette()
        p2.setColor(self.lineEdit_laser_2.backgroundRole(), QColor("red"))
        self.lineEdit_laser_2.setPalette(p2)

    def laser_off(self):
        self.control_q.put(["laser_off", 0], False)
        self.lineEdit_laser.setAutoFillBackground(True)
        self.lineEdit_laser_2.setAutoFillBackground(True)
        p = self.lineEdit_laser.palette()
        p.setColor(self.lineEdit_laser.backgroundRole(), QColor("white"))
        self.lineEdit_laser.setPalette(p)
        p2 = self.lineEdit_laser_2.palette()
        p2.setColor(self.lineEdit_laser_2.backgroundRole(), QColor("white"))
        self.lineEdit_laser_2.setPalette(p2)

    def laser_setPower(self):
        self.control_q.put(
            ["update_laser_power", float(self.lineEdit_laser_power.text())], False)

    def laser_singlePulse(self):
        arg = 10
        self.control_q.put(["single_pulse", arg], False)

    def laser_multiPulse(self):
        arg0 = int(self.lineEdit_laser_numberPulses.text())
        arg1 = 100
        arg = [arg0, arg1]
        self.control_q.put(["multi_pulse", arg], False)

    def laser_alive(self, dummy):
        self.status_laser = dummy  # ???

    def update_laser(self, pos):
        self.lineEdit_laser.setText(str(pos))
        self.lineEdit_laser_2.setText(str(pos))
        # pos = gb.gbl_dict["gbl_laser_power"]  # ???

    # ----- COMMANDS FOR STAGES CONTROL --------------------------------------------- #

    def stage_x_move_abs(self):
        self.control_q.put(
            ["move_abs_x", float(self.lineEdit_donor_x_move_abs.text())], False)

    def stage_x_move_rel(self):
        self.control_q.put(
            ["move_rel_x", float(self.lineEdit_donor_x_move_rel.text())], False)

    def stage_x_move_rel_neg(self):
        self.control_q.put(
            ["move_rel_x", -1 * float(self.lineEdit_donor_x_move_rel.text())], False)

    def stage_x_home(self):
        self.control_q.put(["home_x", 0], False)

    def stage_y_move_abs(self):
        self.control_q.put(
            ["move_abs_y", float(self.lineEdit_donor_y_move_abs.text())], False)

    def stage_y_move_rel(self):
        self.control_q.put(
            ["move_rel_y", float(self.lineEdit_donor_y_move_rel.text())], False)

    def stage_y_move_rel_neg(self):
        self.control_q.put(
            ["move_rel_y", -1 * float(self.lineEdit_donor_y_move_rel.text())], False)

    def stage_y_home(self):
        self.control_q.put(["home_y", 0], False)

    def stage_z_move_abs(self):
        self.control_q.put(
            ["move_abs_z", float(self.lineEdit_zstage_move_abs.text())], False)

    def stage_z_move_rel(self):
        self.control_q.put(
            ["move_rel_z", float(self.lineEdit_zstage_move_rel.text())], False)

    def stage_z_move_rel_neg(self):  # mf1.1c
        self.control_q.put(
            ["move_rel_z", -1 * float(self.lineEdit_zstage_move_rel.text())], False)

    def stage_z_home(self):
        self.control_q.put(["home_z", 0], False)

    def update_stage_x(self, pos):
        pos = float(pos)
        pos = round(pos, 3)  # mf1.1c for keeping No of digits constant to '5'
        self.lineEdit_donor_x.setText(str(pos))  # mf1.1c

        gb.gbl_dict["gbl_stage_x_pos"] = pos

    def update_stage_y(self, pos):
        pos = float(pos)
        pos = round(pos, 3)  # mf1.1c for keeping No of digits constant to '5'
        self.lineEdit_donor_y.setText(str(pos))  # mf1.1c

        gb.gbl_dict["gbl_stage_y_pos"] = pos

    def update_stage_z(self, pos):
        pos = float(pos)
        pos = round(pos, 3)  # mf1.1c for keeping No of digits constant to '5'
        self.lineEdit_zstage.setText(str(pos))  # mf1.1c change LCD to lineEdit

        gb.gbl_dict["gbl_stage_z_pos"] = pos

    def ctrl_alive(self, dummy):
        self.status_ctrl = dummy

    # ----- THE GLOBAL PARAM THING --------------------------------------------- #

    def global_show(self):
        self.lineEdit_global.setText(
            str(gb.gbl_dict[str(self.comboBox_global.currentText())])
        )

    def global_set(self):
        if type(gb.gbl_dict[str(self.comboBox_global.currentText())]) is bool:
            gb.gbl_dict[str(self.comboBox_global.currentText())] = bool(
                float(self.lineEdit_global.text())
            )
        else:
            gb.gbl_dict[str(self.comboBox_global.currentText())] = float(
                self.lineEdit_global.text()
            )

    # ----- CONFIGURE MPT SCAN --------------------------------------------- #

    def setMPT(self):
        try:
            self.setGalvo()  # Galvo parameters
            time.sleep(0.3)
            self.setResolution()  # Image resolution
            time.sleep(0.3)
            self.setDAQ()  # DAQ parameters
            time.sleep(0.3)
            self.setProcessing()  # Post-processing parameters
            time.sleep(0.3)
            self.setLogging()
            self.pushButton_galvoscan.setDisabled(False)
        except:
            print("[main_app]", sys.exc_info()[0], "occurred at line", sys.exc_info()[2].tb_lineno)
            print("Configuration failed")

    def setGalvo(self):
        gb.gbl_dict["gbl_vel"] = float(self.lineEdit_galvo_speed.text())  # [mm/s]
        gb.gbl_dict["gbl_length_x"] = (
                float(self.lineEdit_x_galvo_scan.text()) / 1000)  # [mm]
        gb.gbl_dict["gbl_length_y"] = (
                float(self.lineEdit_y_galvo_scan.text()) / 1000)  # [mm]

        print("\n########## SCAN SETUP ############")
        print("Scan velocity is set to:", gb.gbl_dict["gbl_vel"], "[mm/s]")
        print("Scan area is set to:", gb.gbl_dict["gbl_length_x"], "[mm]")

    def setResolution(self):
        gb.gbl_dict["gbl_resolution"] = int(self.lineEdit_resolution.text())
        print("Resolution is set to:", gb.gbl_dict["gbl_resolution"], "p")

    def setDAQ(self):

        # Setting the clock speed
        if str(self.comboBox_clk_rate.currentText()) == "125 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 125
        elif str(self.comboBox_clk_rate.currentText()) == "100 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 100
        elif str(self.comboBox_clk_rate.currentText()) == "50 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 50
        elif str(self.comboBox_clk_rate.currentText()) == "20 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 20
        elif str(self.comboBox_clk_rate.currentText()) == "10 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 10
        elif str(self.comboBox_clk_rate.currentText()) == "5 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 5
        elif str(self.comboBox_clk_rate.currentText()) == "2 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 2
        elif str(self.comboBox_clk_rate.currentText()) == "1 MS/s":
            gb.gbl_dict["gbl_clk_fq"] = 1

        # Setting the input voltage range
        if str(self.comboBox_in_range.currentText()) == "80 mV":
            gb.gbl_dict["gbl_volt_range"] = 0.08
        elif str(self.comboBox_in_range.currentText()) == "100 mV":
            gb.gbl_dict["gbl_volt_range"] = 0.10
        elif str(self.comboBox_in_range.currentText()) == "200 mV":
            gb.gbl_dict["gbl_volt_range"] = 0.20
        elif str(self.comboBox_in_range.currentText()) == "400 mV":
            gb.gbl_dict["gbl_volt_range"] = 0.40
        elif str(self.comboBox_in_range.currentText()) == "500 mV":
            gb.gbl_dict["gbl_volt_range"] = 0.50
        elif str(self.comboBox_in_range.currentText()) == "800 mV":
            gb.gbl_dict["gbl_volt_range"] = 0.80
        elif str(self.comboBox_in_range.currentText()) == "1 V":
            gb.gbl_dict["gbl_volt_range"] = 1.00
        elif str(self.comboBox_in_range.currentText()) == "2 V":
            gb.gbl_dict["gbl_volt_range"] = 2.00
        elif str(self.comboBox_in_range.currentText()) == "4 V":
            gb.gbl_dict["gbl_volt_range"] = 4.00

        settings = [gb.gbl_dict["gbl_clk_fq"], gb.gbl_dict["gbl_volt_range"]]
        self.daq_q.put(["configure", settings], False)

    def setProcessing(self):
        gb.gbl_dict["gbl_slim_up"] = float(self.lineEdit_slim_up.text())
        gb.gbl_dict["gbl_slim_down"] = float(self.lineEdit_slim_down.text())

    def setLogging(self):
        gb.gbl_dict["gbl_avg_power"] = float(self.lineEdit_avg_power.text())
        gb.gbl_dict["gbl_rep_rate"] = int(self.lineEdit_rep_rate.text())
        # gb.gbl_dict["gbl_pulse_e"] = float(self.lineEdit_pulse_e.text())
        gb.gbl_dict["gbl_pulse_e"] = gb.gbl_dict["gbl_avg_power"] / gb.gbl_dict["gbl_rep_rate"]
        gb.gbl_dict["gbl_pmt_gain"] = int(self.lineEdit_pmt_gain.text())

    # ----- PERFORM MPT SCAN --------------------------------------------- #

    def galvo_scan(self):

        # Send command
        arg = [gb.gbl_dict["gbl_vel"], gb.gbl_dict["gbl_length_x"], gb.gbl_dict["gbl_length_y"]]
        self.control_q.put(["cont_scan", arg], False)

        # Color the laser box
        self.lineEdit_laser.setAutoFillBackground(True)
        self.lineEdit_laser_2.setAutoFillBackground(True)
        p = self.lineEdit_laser.palette()
        p.setColor(self.lineEdit_laser.backgroundRole(), QColor("red"))
        self.lineEdit_laser.setPalette(p)
        p2 = self.lineEdit_laser_2.palette()
        p2.setColor(self.lineEdit_laser_2.backgroundRole(), QColor("red"))
        self.lineEdit_laser_2.setPalette(p2)

    def galvo_home(self):
        self.control_q.put(["galvo_home", 0], False)

    # ----- UPDATE THE IMAGE --------------------------------------------- #

    def update_image(self, graysc):
        pixels = gb.gbl_dict["gbl_resolution"]
        self.image = QImage(pixels, pixels, QImage.Format_RGBX64)

        k = 0
        for j in range(pixels):

            # For the snake scan
            """ 
            if (j % 2) == 0:
                for i in range(pixels):
                    value = qRgb(graysc[k],graysc[k],graysc[k])
                    self.image.setPixel(i,j,value)
                    k += 1
            else:
                for i in range(pixels):
                    value = qRgb(graysc[k],graysc[k],graysc[k])
                    self.image.setPixel(pixels-i,j,value)
                    k += 1
            """

            # For the raster scan
            for i in range(pixels):
                value = QColor.fromRgba64(int(graysc[k]), int(graysc[k]), int(graysc[k]), 65535)
                self.image.setPixelColor(i, j, value)
                k += 1

        # update image
        if pixels > 400:
            self.mpt_screen.setPixmap(QPixmap.fromImage(self.image))
        else:
            pix = QPixmap.fromImage(self.image)
            self.mpt_screen.setPixmap(pix.scaled(500, 500))

    # ----- SAVE THE IMAGE --------------------------------------------- #

    def saveImageButton(self):
        gb.gbl_dict["gbl_save_img"] = True

    def save_image(self, dummy):

        # Assign data arrays
        volt = dummy[0]
        gray = dummy[1]

        # Add directory with date-month name to path
        directory = date.today().strftime("Experiment %d-%m")
        parent_directory = "/Users/Labuser/Documents/FEMTO/Experiments"
        path = os.path.join(parent_directory, directory)

        # If directory not exists - create
        if os.path.exists(path) is False:
            os.mkdir(path)

        # Add sub-directory to path
        count = 1
        sub_directory = 'Image-' + str(count)
        sub_path = os.path.join(path, sub_directory)

        # If sub-directory not exists - create
        while os.path.exists(sub_path) is True:
            count += 1
            sub_directory = 'Image-' + str(count)
            sub_path = os.path.join(path, sub_directory)
        os.mkdir(sub_path)

        # Save DAQ output
        with open(sub_path + '/daq-output.npy', 'wb') as f:
            np.save(f, volt)

        # Save image
        pixels = gb.gbl_dict["gbl_resolution"]
        gray = np.reshape(gray, (pixels, pixels))
        with open(sub_path + '/image.png', 'wb') as f:
            writer = png.Writer(width=gray.shape[1], height=gray.shape[0], bitdepth=16, greyscale=True)
            gray_list = gray.tolist()
            writer.write(f, gray_list)

        # Add excel file name to path
        xl_name = 'experiment_param' + '.xlsx'
        xl_path = os.path.join(path, xl_name)

        # If excel not exists - create and fill
        if os.path.exists(xl_path) is False:
            wb = openpyxl.Workbook()
            ws = wb.active
            ws['B2'] = 'Avg. Power [W]'
            ws['C2'] = 'Rep. Rate [MHz]'
            ws['D2'] = 'Pulse Energy [uJ]'
            ws['E2'] = 'Power Mod [%]'
            ws['F2'] = 'Stage Z [mm]'
            ws['G2'] = 'Scan range [mm]'
            ws['H2'] = 'Scan speed [mm/s]'
            ws['I2'] = 'Resolution'
            ws['J2'] = 'PMT Gain'
            ws['K2'] = 'DAQ range [V]'
            ws['L2'] = 'DAQ clock [MS/s]'
            ws['M2'] = 'Upper Scale Limit'
            ws['N2'] = 'Bottom Scale Limit'

        # Otherwise load excel file
        else:
            wb = openpyxl.load_workbook(xl_path)
            ws = wb.active

        # Write data to excel
        cellNo = str(count + 2)
        ws['A' + cellNo] = sub_directory
        ws['B' + cellNo] = str(gb.gbl_dict["gbl_avg_power"])
        ws['C' + cellNo] = str(gb.gbl_dict["gbl_rep_rate"])
        ws['D' + cellNo] = str(gb.gbl_dict["gbl_pulse_e"])
        ws['E' + cellNo] = str(gb.gbl_dict["gbl_laser_power"])
        ws['F' + cellNo] = str(gb.gbl_dict["gbl_stage_z_pos"])
        ws['G' + cellNo] = str(gb.gbl_dict["gbl_length_x"])
        ws['H' + cellNo] = str(gb.gbl_dict["gbl_vel"])
        ws['I' + cellNo] = str(gb.gbl_dict["gbl_resolution"])
        ws['J' + cellNo] = self.lineEdit_pmt_gain.text()
        ws['K' + cellNo] = str(gb.gbl_dict["gbl_volt_range"])
        ws['L' + cellNo] = str(gb.gbl_dict["gbl_clk_fq"])
        ws['M' + cellNo] = str(gb.gbl_dict["gbl_slim_up"])
        ws['N' + cellNo] = str(gb.gbl_dict["gbl_slim_down"])

        # Save excel
        wb.save(xl_path)


# To write standard output to console and to file, see stackoverflow 14906764
# mf2018012
class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("Logging/logfile.log", "a")
        # self.file = open(self.path, 'a')
        self.log.write("=== SESSION STARTED AT: " + str(time.asctime()) + " ===\n")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
        self.log.flush()

    def flush(self):
        pass


sys.stdout = Logger()


def main():
    app = QApplication(sys.argv)
    form = MainApp()
    form.run()
    form.show()
    app.exec_()
    form.stop()

    time.sleep(1)


if __name__ == "__main__":
    main()
