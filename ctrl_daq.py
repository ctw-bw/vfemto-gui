# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 12:33:35 2021

@author: Luigi
"""

from __future__ import division

import ctypes
import queue
import sys
import threading
import time
import atsapi as ats
import numpy as np
from numba import jit
import global_parameter as gb
import numexpr as ne


class main:

    def __init__(self, q1, q2):

        """
        The function which is called when the main-app initializes the acquisition board.
        """

        self.board = ats.Board(systemId=1, boardId=1)

        # Pre-allocation
        self.samplesPerLine = 0
        self.samplesPerPixel = 0
        self.pixels = 0
        self.samplesPerSec = 0

        # Variables
        self.active = False

        # Create queues
        self.worker_q = queue.Queue()
        self.worker_q = q1
        self.sender_q = queue.Queue()
        self.sender_q = q2

        # Create thread
        self.worker_t = threading.Thread(target=self.worker_loop)

        # Dictionary for external calls
        self.worker_options = {
            "configure": self.ConfigureBoard,
        }

    def worker_loop(self):
        """
        Infinite loop which either acquires data, executes command in the queue, or
        just clear the parameters.
        """

        volt = None
        greyscale = None

        while self.active is True:
            time.sleep(0.02)

            # If STOP is NOT pushed do...
            if gb.gbl_dict["gbl_super_stop"] is False:
                try:
                    volt = self.AcquireData(self.board)  # Acquire
                    start = time.time()
                    greyscale = self.PostProcess(volt)  # Process
                    print("[ctrl_daq] Processing time:", time.time()-start)

                except ValueError:
                    print('[ctrl_daq] Not enough data points')

                except ZeroDivisionError:
                    print('[ctrl_daq] Division by zero occurred')

                except:
                    if gb.gbl_dict["gbl_super_stop"] is True:
                        print("[ctrl_daq] Stop scan")
                    else:
                        print("[ctrl_daq]", sys.exc_info()[0], "occurred at line", sys.exc_info()[2].tb_lineno)

                finally:
                    gb.gbl_dict["gbl_done_scan"] = True

            # If the queue has stuff - execute
            elif not self.worker_q.empty():
                item = self.worker_q.get()
                self.worker_options[item[0]](item[1])
                self.worker_q.task_done()

            # If save image button pushed - save the image
            elif gb.gbl_dict["gbl_save_img"] is True:
                gb.gbl_dict["gbl_save_img"] = False
                self.sender_q.put(["save_image", [volt, greyscale]], False)
                print("[ctrl_daq] Image saved")

    def run(self):
        """
        Starts the thread
        """
        self.active = True
        self.worker_t.start()

    def stop(self):
        """
        Closes the thread
        """
        self.active = False
        self.worker_t.join()

    # -- Configures a board for acquisition
    def ConfigureBoard(self, settings):
        """
        Function which configures the DAQ board.
        Args:
            settings: [sampleRate, inputRange]
        """
        board = self.board

        sampleRateDict = {
            125: ats.SAMPLE_RATE_125MSPS,
            100: ats.SAMPLE_RATE_100MSPS,
            50: ats.SAMPLE_RATE_50MSPS,
            20: ats.SAMPLE_RATE_20MSPS,
            10: ats.SAMPLE_RATE_10MSPS,
            5: ats.SAMPLE_RATE_5MSPS,
            2: ats.SAMPLE_RATE_2MSPS,
            1: ats.SAMPLE_RATE_1MSPS,
        }

        inputRangeDict = {
            0.08: ats.INPUT_RANGE_PM_80_MV,
            0.10: ats.INPUT_RANGE_PM_100_MV,
            0.20: ats.INPUT_RANGE_PM_200_MV,
            0.40: ats.INPUT_RANGE_PM_400_MV,
            0.50: ats.INPUT_RANGE_PM_500_MV,
            0.80: ats.INPUT_RANGE_PM_800_MV,
            1.00: ats.INPUT_RANGE_PM_1_V,
            2.00: ats.INPUT_RANGE_PM_2_V,
            4.00: ats.INPUT_RANGE_PM_4_V,
        }

        # Work with some parameters
        self.pixels = gb.gbl_dict["gbl_resolution"]
        sLength = gb.gbl_dict["gbl_length_x"]
        sVel = gb.gbl_dict["gbl_vel"]
        sTime = sLength / sVel
        print("Time for RTC to scan one line =", sTime, "seconds")

        self.samplesPerSec = settings[0] * 10 ** 6

        self.samplesPerLine = int(self.samplesPerSec * sTime)
        #self.samplesPerLine = int((samplesPerSec * sTime) - ((samplesPerSec * sTime) % 32))
        print("Samples per line =", self.samplesPerLine)

        self.samplesPerPixel = int(self.samplesPerLine / self.pixels)
        print("Samples per pixel =", self.samplesPerPixel)

        # TODO: Select clock parameters
        board.setCaptureClock(
            ats.INTERNAL_CLOCK, sampleRateDict[settings[0]], ats.CLOCK_EDGE_RISING, 0
        )

        # TODO: Select channel A input parameters as required.
        board.inputControlEx(
            ats.CHANNEL_A,
            ats.DC_COUPLING,
            inputRangeDict[settings[1]],
            ats.IMPEDANCE_50_OHM,
        )

        # TODO: Select channel A bandwidth limit as required.
        # board.setBWLimit(ats.CHANNEL_A, 0)

        # TODO: Select trigger inputs and levels as required.
        board.setTriggerOperation(
            ats.TRIG_ENGINE_OP_J,
            ats.TRIG_ENGINE_J,
            ats.TRIG_EXTERNAL,
            ats.TRIGGER_SLOPE_POSITIVE,
            230,
            ats.TRIG_ENGINE_K,
            ats.TRIG_DISABLE,
            ats.TRIGGER_SLOPE_NEGATIVE,
            128,
        )

        # TODO: Select external trigger parameters as required.
        board.setExternalTrigger(ats.DC_COUPLING, ats.ETR_2V5_50OHM)

        # TODO: Set trigger delay as required.
        triggerDelay_sec = 0
        triggerDelay_samples = int(triggerDelay_sec * self.samplesPerSec + 0.5) # was 8
        board.setTriggerDelay(triggerDelay_samples)

        # TODO: Set trigger timeout as required.
        board.setTriggerTimeOut(0)

        #gb.gbl_dict["gbl_daq_config"] = True
        print("------- DAQ Configured ------- \n###################################\n")

    def AcquireData(self, board):
        """
        Function which acquires the data.
        Args:
            board: board handle
        Returns:
            volt: voltage 16-bit representation in numpy array
        """

        # Pre allocation
        volt = np.array([], dtype=np.dtype('i8'))

        # No pre-trigger samples in NPT mode
        preTriggerSamples = 0

        # TODO: Select the number of samples per record.
        postTriggerSamples = self.samplesPerLine

        # TODO: Select the number of records per DMA buffer.
        recordsPerBuffer = int(self.pixels / 10)

        # TODO: Select the number of buffers per acquisition.
        buffersPerAcquisition = 10

        # TODO: Select the active channels.
        channels = ats.CHANNEL_A
        channelCount = 0
        for c in ats.channels:
            channelCount += c & channels == c

        # Compute the number of bytes per record and per buffer
        memorySize_samples, bitsPerSample = board.getChannelInfo()
        bytesPerSample = (bitsPerSample.value + 7) // 8
        samplesPerRecord = preTriggerSamples + postTriggerSamples
        bytesPerRecord = bytesPerSample * samplesPerRecord
        bytesPerBuffer = bytesPerRecord * recordsPerBuffer * channelCount

        # Timeout calculation
        samplesPerBuffer = samplesPerRecord * recordsPerBuffer
        timeout = int(5*(1000*samplesPerBuffer/self.samplesPerSec))

        # TODO: Select number of DMA buffers to allocate
        bufferCount = 4

        # Allocate DMA buffers
        sample_type = ctypes.c_uint16
        buffers = []
        for i in range(bufferCount):
            buffers.append(ats.DMABuffer(board.handle, sample_type, bytesPerBuffer))

        # Set the record size
        board.setRecordSize(preTriggerSamples, postTriggerSamples)

        recordsPerAcquisition = recordsPerBuffer * buffersPerAcquisition

        # Configure the board to make an NPT AutoDMA acquisition
        board.beforeAsyncRead(
            channels,
            -preTriggerSamples,
            samplesPerRecord,
            recordsPerBuffer,
            recordsPerAcquisition,
            ats.ADMA_EXTERNAL_STARTCAPTURE | ats.ADMA_NPT,
        )

        # Post DMA buffers to board
        for buffer in buffers:
            board.postAsyncBuffer(buffer.addr, buffer.size_bytes)

        start = time.time()  # Keep track of when acquisition started
        try:
            board.startCapture()  # Start the acquisition
            gb.gbl_dict["gbl_daq_capture"] = True

            buffersCompleted = 0
            bytesTransferred = 0
            while buffersCompleted < buffersPerAcquisition:
                # Wait for the buffer at the head of the list of available
                # buffers to be filled by the board.
                buffer = buffers[buffersCompleted % len(buffers)]
                board.waitAsyncBufferComplete(buffer.addr, timeout_ms=timeout)
                buffersCompleted += 1
                bytesTransferred += buffer.size_bytes

                # TODO: Process sample data in this buffer. Data is available
                # as a NumPy array at buffer.buffer

                # NOTE:
                #
                # While you are processing this buffer, the board is already
                # filling the next available buffer(s).
                #
                # You MUST finish processing this buffer and post it back to the
                # board before the board fills all of its available DMA buffers
                # and on-board memory.
                #
                # Samples are arranged in the buffer as follows:
                # S0A, S0B, ..., S1A, S1B, ...
                # with SXY the sample number X of channel Y.
                #
                # A 14-bit sample code is stored in the most significant bits of
                # each 16-bit sample value.
                #
                # Sample codes are unsigned by default. As a result:
                # - 0x0000 represents a negative full scale input signal.
                # - 0x8000 represents a ~0V signal.
                # - 0xFFFF represents a positive full scale input signal.

                volt = np.append(volt, np.array(buffer.buffer))
                #print("Buffer complete")

                # Add the buffer to the end of the list of available buffers.
                board.postAsyncBuffer(buffer.addr, buffer.size_bytes)

        finally:
            gb.gbl_dict["gbl_daq_capture"] = False
            board.abortAsyncRead()

        transferTime_sec = time.time() - start
        print("[ctrl_daq] Capture completed in %f sec" % transferTime_sec)
        #print("[ctrl_daq] Bytes per sec: %f" % (bytesTransferred/transferTime_sec))

        return volt

    def PostProcess(self, volt):
        """
        Post-processing of bit signal into grayscale array.
        Args:
            volt: 16-bit representation of voltage in numpy array
        Returns:
            grayscale: 8-bit values in numpy array
        """

        @jit(nopython=True)  # numba.jit decorator
        def arrSum(dummy):
            return 0 - np.sum(dummy)

        @jit(nopython=True)
        def toGray(dummy, scale_up, scale_down):
            cutoff1 = scale_up * (-100000)
            # cutoff2 = scale_down * (-100000)
            temp1 = np.array([cutoff1 if x > cutoff1 else x for x in dummy], dtype=np.int64)
            # temp2 = np.array([cutoff2 if x < cutoff2 else x for x in temp1], dtype=np.int64)
            # arrMin = np.min(temp2)
            # arrMax = np.max(temp2)
            # print((arrMin+arrMax) / -(2*100000))  # just for visualizing the average
            # return [65535 * (x - arrMin) / (arrMax - arrMin) for x in temp2]
            arrMin = np.min(temp1)
            arrMax = np.max(temp1)
            return [(65535 * (x - arrMin) / (arrMax - arrMin)) + scale_down for x in dummy]


        # User input for post-processing
        scaleLimitUp = gb.gbl_dict["gbl_slim_up"]
        scaleLimitDown = gb.gbl_dict["gbl_slim_down"]

        # Summation and greyscale conversion
        sumVolt = np.empty(self.pixels ** 2, dtype=np.dtype('i8'))
        k = 0
        for i in range(self.pixels ** 2):
            sumVolt[i] = arrSum(volt[k:k+self.samplesPerPixel])
            k += self.samplesPerPixel
        greyscale = np.array(toGray(sumVolt, scaleLimitUp, scaleLimitDown), dtype=np.uint16)

        # Check if enough data points
        if greyscale.size < (self.pixels ** 2):
            raise ValueError
        self.sender_q.put(["update_image", greyscale], False)

        return greyscale
