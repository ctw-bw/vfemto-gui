import time
import threading
import queue
from control import Workstation
import global_parameter as gb


class main:

    def __init__(self, q1):
        """
        The function which is called when the control_high initializes the script.
        """

        # Initialize RTC4 card
        self.InitCard()

        # Pre allocation
        self.graysc = []

        # Create a queue
        self.worker_q = queue.Queue()
        self.worker_q = q1

        # Variables
        self.active = False

        # Create a thread
        self.worker_t = threading.Thread(target=self.worker_loop)

        # Dictionary for external calls
        self.worker_options = {
            "laser_on": self.laser_on,
            "laser_off": self.laser_off,
            "update_laser_power": self.update_laser_power,
            "single_pulse": self.single_pulse,
            "multi_pulse": self.multi_pulse,
            'wait_rtc': self.wait_rtc,
            "get_laser_power": self.get_laser_power,
            "set_jump_speed": self.set_jump_speed,
            "set_mark_speed": self.set_mark_speed,
            "galvo_mark": self.galvo_mark,
            "galvo_jump": self.galvo_jump,
            "galvo_scan": self.galvo_scan,
            "galvo_stop": self.galvo_stop,
            "galvo_home": self.galvo_home,
        }

    def worker_loop(self):
        """
        Infinite loop which waits to execute the command in the queue.
        """
        while self.active is True:
            time.sleep(0.01)

            if not self.worker_q.empty():
                item = self.worker_q.get()
                self.worker_options[item[0]](item[1])
                self.worker_q.task_done()

    def run(self):
        """
        Starts the thread
        """
        self.active = True
        self.worker_t.start()

    def stop(self):
        """
        Stops the thread
        """
        self.active = False
        self.worker_t.join()

    def InitCard(self):
        """
        Initializes the RTC4 card
        """
        self.ws = Workstation("rtc4")
        self.ws.init_scanner()
        self.galvo_jump([0, 0, 0])
        self.ws.init_powermodulator()
        self.UpdateLaserPower(1)
        with self.ws.list_1:
            self.ws.list_1.set_laser_delays(laseron=100, laseroff=0)
            self.ws.list_1.set_laser_timing(100, 50, 50, 0)

    # ----- COMMANDS FOR THE LASER ------------------------------------- #

    def laser_on(self, dummy):
        print("Laser switched ON")
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.sh_on()
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def laser_off(self, dummy):
        print("Laser switched OFF")
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.sh_off()
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def update_laser_power(self, power):
        # print("Laser power: " + str(power) + " %")

        if gb.gbl_dict["gbl_laser_power_lim_up"] >= power >= gb.gbl_dict["gbl_laser_power_lim_down"]:
            print("Laser power " + str(power) + " %")
            self.ws.set_power(power)
            gb.gbl_dict["gbl_laser_power"] = power
        else:
            print("LASER OUT OF LIMITS")

    def single_pulse(self, dummy):  # not implemented
        PulseTimeWindow = dummy
        print("Release single pulse")
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.laser_on(PulseTimeWindow, True)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def multi_pulse(self, dummy):  # not implemented
        numberOfPulses = dummy[0]
        TimeWindow = dummy[1]

        bits = int(numberOfPulses * TimeWindow)  # *1e5)
        deltat_temp = bits

        print("Printing %d pulses" % numberOfPulses)
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.sh_on()

        if bits < 65500:
            self.ws.list_1.wait(bits, True)

        if bits > 65500:
            while deltat_temp > 65500:
                self.ws.list_1.wait(65500)
                deltat_temp = deltat_temp - 65500
            self.ws.list_1.wait(deltat_temp, True)

        self.ws.list_1.sh_off()
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def get_laser_power(self):
        return gb.gbl_dict["gbl_laser_power"]

    def UpdateLaserPower(self, power_percentage):  # second update laser command :|
        self.ws.set_power(power_percentage)

    # ----- COMMANDS FOR THE GALVO ------------------------------------- #

    def set_jump_speed(self, dummy):
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.set_jump_speed(dummy, True)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def set_mark_speed(self, dummy):
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.set_mark_speed(dummy, True)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def galvo_mark(self, dummy):

        galvo_x = dummy[0]
        galvo_y = dummy[1]
        galvo_rel = dummy[2]  # if zero means absolute, if 1 means relative

        self.ws.list_1.set_start_list(1)
        self.ws.list_1.mark(galvo_x, galvo_y, galvo_rel, False)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def galvo_jump(self, dummy):

        galvo_x = dummy[0]
        galvo_y = dummy[1]
        galvo_rel = dummy[2]  # if zero means absolute, if 1 means relative

        self.ws.list_1.set_start_list(1)
        self.ws.list_1.jump(galvo_x, galvo_y, galvo_rel, False)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def galvo_home(self, dummy):
        self.ws.list_2.set_start_list(1)
        self.ws.list_1.jump(0, 0, 0, False)
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)
        print("[ctrl_laser] Galvo Homed")

    # ----- COMMANDS FOR RTC CARD ------------------------------------- #

    def wait_rtc(self, deltat):  # not implemented
        deltat_temp = deltat

        if deltat < 0.5:
            with self.ws.list_1:
                self.ws.list_1.wait(deltat)
        if deltat > 0.5:
            while deltat_temp > 0.5:
                with self.ws.list_1:
                    self.ws.list_1.wait(0.5)
                deltat_temp = deltat_temp - 0.5
            with self.ws.list_1:
                self.ws.list_1.wait(deltat_temp)

    # ----- COMMANDS FOR MPT SCAN ------------------------------------- #

    def galvo_scan(self, dummy):

        galvo_speed = dummy[0]
        galvo_x = dummy[1]
        galvo_y = dummy[2]
        galvo_rel = dummy[3]
        pixels = dummy[4]

        self.ws.list_1.set_start_list(1)

        self.ws.list_1.set_jump_speed(galvo_speed, True)
        self.ws.list_1.jump(-galvo_x / 2, -galvo_y / 2, 0, False)

        for i in range(int(pixels)):

            self.ws.list_1.write_io_port(3)  # output trigger ON
            self.ws.list_1.jump(galvo_x, 0, galvo_rel, False)
            self.ws.list_1.write_io_port(1)  # output trigger OFF

            self.ws.list_1.jump(-galvo_x, galvo_y / pixels, galvo_rel, False)

        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)

    def galvo_stop(self, dummy):
        """
        Function that sends interrupt signal to RTC4 which clears its buffer
        """
        self.worker_q.queue.clear()
        self.ws.list_1.set_start_list(1)
        self.ws.list_1.stop_execution()
        self.ws.list_1.sh_off()
        self.ws.list_1.set_end_of_list()
        self.ws.list_1.execute_list(1)
        print("[ctrl_laser] Stop Scan")
