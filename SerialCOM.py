__author__ = "Hedzer Durksz"
__copyright__ = "Copyright 2012, 3D Printer"
__credits__ = ["", ""]
__license__ = ""
__version__ = "1.0.0"
__maintainer__ = "Hedzer Durksz"
__email__ = "hedzerdurksz@gmail.com"
__status__ = "Development"  # "Prototype", "Development", or "Production"

""" 
In this script a connection with an Altechna Watt pilot power modulator or other device that is 
connected to the com-port. 

See for suitable commands the Altechna manual 
(http://www.altechna.com/download/Watt_Pilot/Watt_Pilot_User_Manual_A4_Full_07.pdf)
"""

import queue
import time
import logging
import serial

q = queue.Queue()


"""
Manier om met mac computers de arduino te vinden.
http://stackoverflow.com/questions/1659283/macpython-programmatically-finding-all-serial-ports

import glob
def scan():
    return glob.glob('/dev/tty*') + glob.glob('/dev/cu*')

for port in scan():
do something to check this port is open.
"""


# class read_serial_thread(threading.Thread):
#     def __init__ (self, port=""):
#
#         threading.Thread.__init__(self)
#         name = '.'.join([__name__, self.__class__.__name__])
#         self.logger = logging.getLogger(name)
#         self.logger.debug("start thread")
#         self.port = port
#         self.stop_event = threading.Event()
#         self.start()
#
#     def stop(self):
#         self.logger.debug("Read serial stop executed")
#         self.stop_event.set()
#         return
#
#     def run(self):
#         while not self.stop_event.isSet():
#             try:
#                 data = self.port.readline()
#                 if not data == "":
#                     print("received data from serial : ", data)
#                     self.logger.debug(data)
#                     q.put(data)
#
#             except:
#                 self.logger.debug("error in reading data from adruino")
#         self.logger.debug("end of read serial routine")
#         return

# ------------------------------------------------------------------------------
# ==============================================================================
# ------------------------------------------------------------------------------


class RWSerial:
    def __init__(self):
        self.serial_port = 0
        self.sum_comports = 0
        name = ".".join([__name__, self.__class__.__name__])
        self.logger = logging.getLogger(name)

    def search_serial(self):
        # dialog = ErrMsg.SubclassDialog()
        # self.search_serial = threading.Thread(target = self.SearchForConnectedArduinos)
        # Thread.setDeamon(True)
        # self.search_serial.start()
        # return serial_search
        self.search_for_connected_serial_port()

    def scan_serial(self):
        """scan for available ports. return a list of tuples (num, name)"""
        self.logger.debug("scan serial")
        available = []
        for i in range(256):
            port = "COM" + str(i)
            try:
                s = serial.Serial(port)
                available.append(s.portstr)
                s.close()  # explicit close 'cause of delayed GC in java
            except serial.SerialException:
                pass
        # print (available)
        return available

    # ----------------------------------------------------------------------

    def search_for_connected_serial_port(self):
        """ check if the connected device is the right arduino with the correct software"""
        self.logger.debug("Scanning COM ports for available Arduino's")

        available_COM_ports = self.scan_serial()
        self.logger.debug("available com ports: ".format(available_COM_ports))
        self.serial_port = ""
        self.sum_comports = 0
        nr_of_comports = len(available_COM_ports)
        self.logger.debug(nr_of_comports)
        for PortName in available_COM_ports[::]:
            self.logger.debug(PortName)
            # print("portname : ",PortName)
            x = serial.Serial(
                port=PortName, baudrate=38400, timeout=0.2, writeTimeout=0.1
            )
            try:
                for y in range(3):  # DEFAULT 3
                    command_value = "p"
                    # print("y : ", y)
                    self.write_to_serial(command_value, x)
                    # time.sleep(0.2)
                    reply = x.readline()
                    self.logger.debug("serial reply: ".format(reply))
                    # print("serial reply: ", reply)
                    reply = reply.decode("utf-8")
                    useful_reply_part = reply[0:4]
                    # print("serial reply: ", useful_reply_part)

                    if useful_reply_part == "pUSB":
                        # print("right reply found")
                        self.serial_port = x
                        # self.serial_port = serial.Serial(port=PortName, baudrate=38400, timeout=.1)
                        # print("set serialport : ", self.serial_port)
                        # self.stop_thread_event = 0
                        # self.readserial = read_serial_thread(self.serial_port)
                        self.logger.debug(
                            "Altechna Watt Pilot connected : info: {}".format(
                                self.serial_port
                            )
                        )
                        # print("serial connection established : ", self.serial_port)
                        print("serial connection established")
                        return self.serial_port

                x.close()
                self.logger.debug("Altechna Watt Pilot NOT CONNECTED")
                self.sum_comports = self.sum_comports + 1

            except serial.SerialException:
                self.logger.debug("no Watt Pilot was found, serial Exception")
                self.logger.info(
                    "no Altechna Watt Pilot was found, try to turn of and on the Watt Pilot"
                )
                return "ERROR"

            self.logger.debug("done")

        if self.sum_comports >= len(available_COM_ports):
            self.logger.debug(
                "no arduino has been connected sum ports less than available ports"
            )
            return "ERROR"

        if available_COM_ports is []:
            self.logger.debug("no COM ports found")
            self.serial_port = 0
            return "ERROR"

        self.logger.debug("end search for serial communication")
        return "ERROR"

    #
    #     def stop_read_from_serial_thread(self):
    #         """stop thread, this command hast to be called when the program is shut down,
    #         this to prevent the thread from staying active after the program has been closed"""
    #         self.logger.debug("STOP CALLED")
    #         try:
    #             self.readserial.stop()
    #             self.logger.debug(self.readserial)
    #         except:
    #             self.logger.debug("read serial thread not running, so it can not be stopped")
    #             pass
    #         return
    #
    # ------------------------------------------------------------------------------
    # ==============================================================================
    # ------------------------------------------------------------------------------

    def write_to_serial(self, command="", port=None):
        """
        this def sends the command defined in other defs to the selected serial port.
        The command given is ended by an \r"""
        # print(command)
        command = command + "\r"
        # print(command)
        command = command.encode("utf-8")
        if port is None:
            port = self.serial_port
        # print("port : ",port," command : ", command)
        if hasattr(port, "flushInput"):
            port.flushInput()
        port.write(command)  # write the command to the comport
        return "DONE"

    # ------------------------------------------------------------------------------
    # ==============================================================================
    # ------------------------------------------------------------------------------

    def wait_for_response(self, wait_time=5):
        """wait for response of the arduino for max ...sec,
        if this response correct return the response value
        else return "NO RESP" """
        time_out = time.clock() + wait_time
        # print time.clock()
        # print time_out - time.clock()

        while time_out - time.clock() >= -1:
            # if (self.message != []):
            reply = self.serial_port.readline()
            self.logger.debug("serial reply: ".format(reply))

            reply = reply.decode("utf-8")
            # print("serial reply: ", reply)

            if reply != "":
                return reply
            # useful_reply_part = reply[0:4]
            # if not q.empty():
            #    message = q.get()
            #    return message

            if time_out - time.clock() <= 0:
                self.logger.debug("no message received from selected serial port")
                return "NO RESP"
                # break

            # if self.stoprequest.isSet():
            #    #wx.CallAfter(ErrMsg.GeneralError,"Print routine aborted, while waiting for printer to response")
            #    logger.debug("routine aborted during waiting on response of serial port")
            #    return "ABORT RESP WAIT"
            #    break
            # print time_out - time.clock()
